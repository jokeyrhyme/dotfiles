#! /usr/bin/env sh

set -eu

if [ "$(uname)" != "Linux" ]; then
  # no SSH services enabled outside of Linux, so nothing to do
  exit 0
fi

# shellcheck source=../lib/helpers.sh
. "$(dirname $0)/../lib/helpers.sh"

FORMFACTOR=$("${HOME}/.dotfiles/bin/detect-formfactor.sh")
echo "formfactor = ${FORMFACTOR}"

if [ "${FORMFACTOR}" = "server" ]; then
  rm -fv ~/.ssh/authorized_keys
  for PUB in ~/.dotfiles/public-keys/*.pub; do
    printf "\n# %s\n" "${PUB}" >>~/.ssh/authorized_keys
    cat "${PUB}" >>~/.ssh/authorized_keys
  done
fi

if [ -f /etc/ssh/sshd_config ]; then
  CONFIG_TEMP=$(mktemp)
  sed \
    -e 's/^#PermitRootLogin[[:space:]]/PermitRootLogin /g' \
    -e 's/^PermitRootLogin[[:space:]]\+[[:alpha:]-]\+$/PermitRootLogin no/g' \
    >"${CONFIG_TEMP}" </etc/ssh/sshd_config
  __dotfiles_mv_if_diff "${CONFIG_TEMP}" /etc/ssh/sshd_config
fi
