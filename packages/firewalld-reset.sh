#! /usr/bin/env sh

set -eu

if [ "$(uname)" != "Linux" ]; then
  # so far, only needed on Linux systems
  exit 0
fi
if ! command -v firewall-cmd >/dev/null 2>&1; then
  exit 0
fi

sudo rm -f /etc/firewalld/ipsets/local.xml
sudo rm -f /etc/firewalld/ipsets/local6.xml
sudo rm -f /etc/firewalld/ipsets/*dotfiles*
sudo rm -rf /etc/firewalld/policies/*
sudo rm -rf /etc/firewalld/zones/*

sudo systemctl restart firewalld.service
sudo firewall-cmd --reset-to-default
