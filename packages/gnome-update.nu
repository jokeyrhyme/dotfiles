#! /usr/bin/env nu

if not ((sys host).name | str contains 'Linux') {
    # so far, only needed on Linux systems
    exit 0
}

