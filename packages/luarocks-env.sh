#! /usr/bin/env sh

if [ -z "${LUA_ROCKS-}" ] && command -v luarocks >/dev/null; then
  eval "$(luarocks path)"
fi
