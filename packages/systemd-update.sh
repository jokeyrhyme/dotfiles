#! /usr/bin/env sh

set -eu

if ! command -v systemctl >/dev/null 2>&1; then
  exit 0
fi

# shellcheck source=../lib/systemd.sh
. "$(dirname $0)/../lib/systemd.sh"

LOGIND_CONF_D=/etc/systemd/logind.conf.d
SLEEP_CONF_D=/etc/systemd/sleep.conf.d

sudo mkdir -p "${LOGIND_CONF_D}"
sudo mkdir -p "${SLEEP_CONF_D}"

# nevermind, suspend is still broken
# HOSTNAME=$("${HOME}/.dotfiles/bin/detect-hostname.sh")
# if [ "${HOSTNAME}" = "valios" ]; then
#   # we have working hibernate and suspend on this system
#   printf "[Login]\nHandleLidSwitch=hybrid-sleep\nHandleLidSwitchExternalPower=ignore\nHandleLidSwitchDocked=igore\n" | sudo tee "${LOGIND_CONF_D}/dotfiles.conf"
#   printf "[Sleep]\nAllowSuspend=yes\nAllowHibernation=yes\nAllowSuspendThenHibernate=yes\nAllowHybridSleep=yes\n" | sudo tee "${SLEEP_CONF_D}/dotfiles.conf"
# fi

__dotfiles_systemctl_enable dotfiles-lock-sessions.service
