#! /usr/bin/env sh

set -eu

if [ "$(uname)" != "Linux" ]; then
  # so far, only compatible with Linux systems
  exit 0
fi

sudo mkdir -p /efi/loader/entries
printf "title memtest86+\nefi /memtest86+/memtest.efi" | sudo tee /efi/loader/entries/memtest.conf
