#! /usr/bin/env nu

use ../lib/git.nu [ git_update ]

if not ((sys host).name | str contains 'Linux') {
    # so far, only compatible with Linux systems
    exit 0
}

let FORMFACTOR = ((~/.dotfiles/bin/detect-formfactor.sh | complete).stdout | str trim)
print $'formfactor = ($FORMFACTOR)'
if ($FORMFACTOR != 'desktop') {
    # we only want this running on interactive/desktop machines for now
    exit 0
}

exit 0

^sudo rm -rfv /var/lib/extensions/cosmic-sysext

let REMOTE_REPO = 'https://github.com/pop-os/cosmic-greeter.git'
let CLONE_DIR = $'($env.HOME)/GitHub/pop-os/cosmic-greeter'
git_update $REMOTE_REPO $CLONE_DIR

^sudo mkdir -p "/etc/systemd/system"
^sudo cp -v $'($CLONE_DIR)/debian/cosmic-greeter.service' '/etc/systemd/system/'

# uncomment the `[Install]` section in the unit file
let CONFIG_TEMP = (mktemp)
open /etc/systemd/system/cosmic-greeter.service
| str replace --all --multiline --regex '^#\[Install\]' '[Install]'
| str replace --all --multiline --regex '^#Alias=display-manager.service' 'Alias=display-manager.service'
| save --force $CONFIG_TEMP
^sudo mv -v $CONFIG_TEMP /etc/systemd/system/cosmic-greeter.service

