#! /usr/bin/env bash

# this script cannot make many assumptions about its execution environment

set -eu

DRY_RUN="false"
while test $# -gt 0; do
  case "$1" in --dry-run) DRY_RUN="true" ;;
  *) ;;
  esac
  shift
done

echo "dry-run = ${DRY_RUN}"
echo "current user = $(whoami)"

# setup
HOSTNAME=$("${HOME}/.dotfiles/bin/detect-hostname.sh")
echo "hostname = ${HOSTNAME}"

if [ -r "${HOME}/.cargo/env" ]; then
  . "${HOME}/.cargo/env"
fi
if command -v rustup; then
  echo "found rustup!"
else
  # no `rustup`, let's install it, but first we need `curl`
  if command -v curl; then
    echo "found curl!"
  else
    # no `curl`, yet
    if [ "$(whoami)" = "root" ] && command -v pacman; then
      pacman -S --refresh
      pacman -S --needed --noconfirm curl
    elif [ "$(whoami)" = "root" ] && command -v apk; then
      apk add curl
    elif [ "$(whoami)" = "root" ] && command -v apt-get; then
      apt-get update
      apt-get install --yes curl
    elif command -v apt-get && command -v sudo; then
      sudo apt-get update
      sudo apt-get install --yes curl
    fi
  fi
  # okay, in theory, we have `curl` now
  curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh -s -- -y --no-modify-path
fi
if [ -r "${HOME}/.cargo/env" ]; then
  . "${HOME}/.cargo/env"
fi

# fix "ld: cannot find crti.o: No such file or directory" on alpine
if [ "$(whoami)" = "root" ] && command -v apk; then
  apk add musl-dev
fi

# okay, we should have `rustup` now, but C/Cpp crates need more stuff
if command -v fakeroot && command -v gcc && command -v pkgconf; then
  echo "found fakeroot, gcc, and pkgconf!"
else
  if [ "$(whoami)" = "root" ] && command -v pacman; then
    pacman -S --refresh
    pacman -S --needed --noconfirm fakeroot gcc pkgconf
  elif [ "$(whoami)" = "root" ] && command -v apk; then
    apk add fakeroot gcc pkgconf
  elif [ "$(whoami)" = "root" ] && command -v apt-get; then
    apt-get update
    apt-get install --yes fakeroot gcc pkgconf
  elif command -v apt-get && command -v sudo; then
    sudo apt-get update
    sudo apt-get install --yes fakeroot gcc pkgconf
  fi
fi
if ! pkgconf --libs openssl; then
  if [ "$(whoami)" = "root" ] && command -v pacman; then
    pacman -S --needed --noconfirm openssl
  elif [ "$(whoami)" = "root" ] && command -v apk; then
    apk add openssl-dev openssl-libs-static
  elif [ "$(whoami)" = "root" ] && command -v apt-get; then
    apt-get install --yes libssl-dev
  elif command -v apt-get && command -v sudo; then
    sudo apt-get install --yes libssl-dev
  fi
fi
# okay, in theory, we're ready to compile/run ~/.dotfiles/package-rs now

FORMFACTOR=$("${HOME}/.dotfiles/bin/detect-formfactor.sh")
echo "formfactor = ${FORMFACTOR}"

OWNERSHIP=$("${HOME}/.dotfiles/bin/detect-ownership.sh")
echo "ownership = ${OWNERSHIP}"

echo "rustup = $(command -v rustup)"
rustup default stable
rustup --version

echo "cargo = $(command -v cargo)"
cargo --version

if command -v pacman >/dev/null 2>&1; then
  if [ ! -x "${HOME}/.cargo/bin/aura" ]; then
    cargo install aura-pm --locked
  fi
fi

PACKAGES_RS=""
pushd "${HOME}/.dotfiles/crates/dotfiles-packages-rs"
PACKAGES_RS="$(cargo run -- DRY_RUN=${DRY_RUN} ${FORMFACTOR} ${OWNERSHIP})"
popd

# teardown
if [ "$(whoami)" = "root" ]; then
  # the /root copy of the Rust toolchain is only needed for here,
  # most use of the Rust toolchain should depend on the copy in /home
  rm -rf "${HOME}/.cargo"
fi

if [ "${DRY_RUN}" = "false" ]; then
  echo "applying ~/.dotfiles/config/requirements.toml ..."
  eval "${PACKAGES_RS}"

  # handle package name differences between Debian, Ubuntu, etc
  if command -v apt-get >/dev/null 2>&1; then
    for PKG in firmware-linux firmware-linux-free; do
      apt-cache show "${PKG}" >/dev/null 2>&1 && sudo apt-get install --yes "${PKG}"
    done
  fi
fi
