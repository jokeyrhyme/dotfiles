#! /usr/bin/env bash

set -eu -o pipefail

# shellcheck source=../lib/helpers.sh
. "$(dirname $0)/../lib/helpers.sh"

if ! __dotfiles_is_distro_archlinux; then
  exit 0
fi

if ! command -v aura >/dev/null; then
  exit 0
fi

sudo mkdir -p /etc/makepkg.conf.d

echo "installing favourites with 'aura' ..."

echo "removing unnecessary packages with 'aura' ..."
aura -O --abandon || true

echo "updating packages with 'aura' ..."
aura -A --sysupgrade || true

pkgstats submit

sudo systemctl daemon-reload
