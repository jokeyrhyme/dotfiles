#! /usr/bin/env bash

set -eu -o pipefail

if ! command -v docker >/dev/null; then
  # nothing do do here
  exit 0
fi

if command -v systemctl >/dev/null; then
  if systemctl is-enabled docker.service >/dev/null || systemctl is-enabled docker.socket >/dev/null 2>&1; then
    if ! docker version >/dev/null 2>&1; then
      # we have docker but it doesn't seem to be working, let's fix it

      if networkctl status docker0 >/dev/null 2>&1; then
        sudo networkctl delete docker0
      fi
      if ip link show docker0 >/dev/null 2>&1; then
        sudo ip link delete docker0
      fi
      sudo rm -rf /var/lib/docker/network

      if command -v firewall-cmd >/dev/null && systemctl is-active firewalld.service >/dev/null 2>&1; then
        sudo firewall-cmd --permanent --remove-interface docker0 >/dev/null || true 2>&1
        sudo systemctl restart firewalld.service
      fi

      sudo systemctl restart docker.service
    fi
  fi
fi
