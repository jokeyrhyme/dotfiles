#! /usr/bin/env sh

# rootfs: changes to /usr and /etc
# update: install files into /usr and /etc

set -eu

if [ "$(uname)" != "Linux" ]; then
  # so far, only needed on Linux systems
  exit 0
fi

echo 'copying into /etc/ ...'
sudo cp -rv ~/.dotfiles/etc/* /etc/
sudo chmod -R o-rwx /etc/sudoers.d

echo 'copying into /usr/ ...'
sudo cp -rv ~/.dotfiles/usr/* /usr/
