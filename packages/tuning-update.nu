#! /usr/bin/env nu

let FORMFACTOR = ((~/.dotfiles/bin/detect-formfactor.sh | complete).stdout | str trim)
print $'formfactor = ($FORMFACTOR)'
if ($FORMFACTOR != 'desktop') {
    # we only want this running on interactive/desktop machines for now
    exit 0
}

# don't assume cargo/tuning are in PATH yet
if ('~/.cargo/bin/cargo' | path exists) {
    try { ^~/.cargo/bin/cargo install tuning } catch {}
}
if ('~/.cargo/bin/tuning' | path exists) {
    let OWNERSHIP = ((~/.dotfiles/bin/detect-ownership.sh | complete).stdout | str trim)
    print $'ownership = ($OWNERSHIP)'
    # TODO: ownership in CI should not be "personal"
    try { RUST_BACKTRACE=1 ~/.cargo/bin/tuning --tags $FORMFACTOR --tags $OWNERSHIP --tags untagged } catch {}
}
