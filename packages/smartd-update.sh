#! /usr/bin/env bash

set -eu

if [ "$(uname)" != "Linux" ]; then
  # so far, only compatible with Linux systems
  exit 0
fi

# shellcheck source=../lib/helpers.sh
. "$(dirname $0)/../lib/helpers.sh"

# it's a bit tricky to conditionally have this exclusion list or not,
# so here we always have a list of at least one item to make later code simpler
#
# the kernel reserves the `0` major number, it is used to request a dynamic number,
# so this should be okay to always exclude here by default
EXCLUDE_MAJORS=0

# exclude major numbers for MMC devices: smartmontools don't work on them
# TODO: use mmc-utils or other MMC-compatible healthcheck
# exclude major numbers for mem,zram: smartmontools doesn't work on RAM
while IFS= read -r MAJOR; do
  EXCLUDE_MAJORS=${EXCLUDE_MAJORS},$(echo "${MAJOR}" | tr --delete '[:space:]')
done < <(grep '[[:digit:]]\+ \(mem\|mmc\|zram\)' </proc/devices | sed 's/ \(mem\|mmc\|zram\)//')

# grab info about real disk devices, no partitions, etc
DEVICES_INFO=$(lsblk --exclude="${EXCLUDE_MAJORS}" --json --nodeps --output=name,subsystems,type --paths)

# exclude USB devices: they frequently behave badly when queried for SMART info
# TODO: do not exclude well-behaved USB devices
JQ_QUERY='.blockdevices[] | select(.subsystems | contains(":usb:") | not) | .name'

CONFIG_TEMP=$(mktemp)
for DEVICE in $(echo $DEVICES_INFO | jq --raw-output "${JQ_QUERY}"); do
  echo "${DEVICE} -e aam,128 -s (O/../01/./01:024|L/../../6/01:012|S/../.././01:001)" >>"${CONFIG_TEMP}"
  # -H           -> check for impending failure predicted within 24 hours
  #                 (skipping this for now, might be best used by a healthcheck job)
  # -l error -l xerror -l selftest -l offlinests -l selfteststs
  #              -> report increases in number of errors across selected tests
  #                 (skipping this for now, might be best used by a healthcheck job)
  # -e aam,128   -> set Automatic Acoustic Management to "quietest" setting
  # -e apm,64    -> set Advanced Power Management to allow spindown
  # -e standby,244
  #              -> set spindown to 2 hours and put in IDLE mode
  # -s O/../01/./01:024
  #              -> Offline Immediate Test starting the 1st of each month at 1am,
  #                 staggered so 24 hours between each drive
  # -s L/../../6/01:012
  #              -> Long Self-Test starting every Saturday at 1am,
  #                 staggered so 12 hours between each drive
  # -s S/../.././01:001
  #              -> Short Self-Test starting every day at 1am,
  #                 staggered so 1 hour between each drive
done
# settings to apply to everything else (e.g. USB)
echo "DEVICESCAN -e aam,128" >>"${CONFIG_TEMP}"
__dotfiles_mv_if_diff "${CONFIG_TEMP}" /etc/smartd.conf
