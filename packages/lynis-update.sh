#! /usr/bin/env sh

# configure lynis

set -eu

if [ "$(uname)" != "Linux" ]; then
  # for now, let's assume (managed) macOS meets compliance requirements
  exit 0
fi

# shellcheck source=../lib/helpers.sh
. "$(dirname $0)/../lib/helpers.sh"

# FILE-7524
for FILE in /etc/cron.hourly /etc/cron.daily /etc/cron.weekly /etc/cron.monthly; do
  if [ -d "${FILE}" ]; then
    sudo chmod -R go-rwx "${FILE}"
  fi
done

if command -v lynis >/dev/null && [ -f /etc/lynis/custom.prf ]; then
  FORMFACTOR=$("${HOME}/.dotfiles/bin/detect-formfactor.sh")
  echo "formfactor = ${FORMFACTOR}"

  if [ "${FORMFACTOR}" = "server" ]; then
    CONFIG_TEMP=$(mktemp)
    sudo cat /etc/lynis/custom.prf | sed \
      -e 's/machine-role=personal/machine-role=server/g' \
      >"${CONFIG_TEMP}"
    __dotfiles_mv_if_diff "${CONFIG_TEMP}" /etc/lynis/custom.prf
  fi

  if [ "${FORMFACTOR}" = "desktop" ]; then
    CONFIG_TEMP=$(mktemp)
    sudo cat /etc/lynis/custom.prf | sed \
      -e 's/machine-role=personal/machine-role=workstation/g' \
      -e 's/skip-test=STRG-1840/# skip-test=STRG-1840/g' \
      -e 's/skip-test=USB-1000/# skip-test=USB-1000/g' \
      >"${CONFIG_TEMP}"
    __dotfiles_mv_if_diff "${CONFIG_TEMP}" /etc/lynis/custom.prf

    CONFIG_TEMP=$(mktemp)
    sudo cat /etc/modprobe.d/dotfiles.conf | sed \
      -e 's/# blacklist usb_storage/blacklist usb_storage/g' \
      >"${CONFIG_TEMP}"
    __dotfiles_mv_if_diff "${CONFIG_TEMP}" /etc/modprobe.d/dotfiles.conf
  fi

  if __dotfiles_is_distro_archlinux; then
    CONFIG_TEMP=$(mktemp)
    sudo cat /etc/lynis/custom.prf | sed \
      -e 's/# skip-test=PKGS-7370/skip-test=PKGS-7370/g' \
      >"${CONFIG_TEMP}"
    __dotfiles_mv_if_diff "${CONFIG_TEMP}" /etc/lynis/custom.prf
  fi
fi
