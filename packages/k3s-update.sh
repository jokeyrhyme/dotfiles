#! /usr/bin/env bash

set -eu

if [ "$(uname)" != "Linux" ]; then
  # so far, only needed on Linux systems
  exit 0
fi
if [[ ${HOSTNAME} != "rpi-k3s-"* ]]; then
  exit 0
fi

sudo mkdir -p /etc/sysctl.d
sudo cp -v "${HOME}/.dotfiles/config/k3s/90-dotfiles-kubelet.conf" /etc/sysctl.d/

"${HOME}/.dotfiles/bin/secrets-decrypt.sh"
export K3S_TOKEN=(cat "${HOME}/.dotfiles/config/k3s/token.secret.txt")

# TODO: avoid hardcoding addresses here
CLUSTER_ARG=""
TLS_SAN_ARG=""
if [ ${HOSTNAME} = "rpi-k3s-117" ]; then
  CLUSTER_ARG="--cluster-init"
  TLS_SAN_ARG="--tls-san=192.168.1.117"
elif [ ${HOSTNAME} = "rpi-k3s-119" ]; then
  CLUSTER_ARG="--server=https://192.168.1.117:6443"
  TLS_SAN_ARG="--tls-san=192.168.1.119"
elif [ ${HOSTNAME} = "rpi-k3s-125" ]; then
  CLUSTER_ARG="--server=https://192.168.1.117:6443"
  TLS_SAN_ARG="--tls-san=192.168.1.125"
fi

# --kubelet-arg:
#   - see: https://kubernetes.io/docs/tasks/administer-cluster/sysctl-cluster/#enabling-unsafe-sysctls
#   - see: https://docs.k3s.io/networking/networking-services#service-load-balancer
#   - k3s sets this for us by default for use with the default ServiceLB component,
#     so we lose this when we --disable=servicelb
curl -sfL https://get.k3s.io | sh -s - server "${CLUSTER_ARG}" "${TLS_SAN_ARG}" \
  --disable=servicelb \
  --flannel-backend=wireguard-native \
  --kubelet-arg '--allowed-unsafe-sysctls=net.ipv4.ip_forward,net.ipv6.conf.all.forwarding' \
  --protect-kernel-defaults \
  --secrets-encryption

"${HOME}/.dotfiles/bin/secrets-teardown.sh"
