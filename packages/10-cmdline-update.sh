#! /usr/bin/env sh

set -eu

# shellcheck source=../lib/helpers.sh
. "$(dirname $0)/../lib/helpers.sh"

if ! __dotfiles_is_distro_archlinux; then
  # only tested with Arch Linux
  exit 0
fi

sudo mkdir -p /etc/cmdline.d
sudo mkdir -p /etc/dracut.conf.d
sudo rm -fv /etc/kernel/cmdline
sudo rm -fv /etc/kernel/cmdline-fallback

ROOT_DEV=$(mount | grep ' on / type ' | cut -d ' ' -f 1)
ROOT_UUID=$(lsblk --list --noheadings --output UUID "${ROOT_DEV}")
echo "root=UUID=${ROOT_UUID}" | sudo tee /etc/cmdline.d/dotfiles-root.conf
echo "kernel_cmdline+=' root=UUID=${ROOT_UUID} '" | sudo tee /etc/dracut.conf.d/dotfiles-root.conf

if mount | grep ' on / type btrfs ' >/dev/null 2>&1; then
  echo "filesystems+=' btrfs '" | sudo tee --append /etc/dracut.conf.d/dotfiles-root.conf
fi

# # TODO: support other subvolume names for the root
# if mount | grep ' on / type btrfs ' | grep -o ',subvol=/@)\?$' >/dev/null 2>&1; then
#   echo "rootflags=subvol=/@" | sudo tee /etc/cmdline.d/dotfiles-rootflags.conf
#   echo "kernel_cmdline+=' rootflags=subvol=/@ '" | sudo tee /etc/dracut.conf.d/dotfiles-rootflags.conf
# fi

# if we're using a swap file on btrfs, recreate it
# (solves various issues with `btrfs balance` breaking it, etc)
if mount | grep ' on /swap type btrfs ' >/dev/null 2>&1; then
  if [ -f /swap/swapfile ]; then
    SWAP_KB=$(grep 'MemTotal:.*kB' </proc/meminfo | grep -o '[[:digit:]]\+')
    # || true: this will fail if the swapfile is in use,
    # but in this case we don't actually need to replace it, so it's fine
    sudo rm -fv /swap/swapfile || true
    sudo btrfs filesystem mkswapfile /swap/swapfile --size "${SWAP_KB}k" || true
  fi
fi

if [ -e /dev/mapper/vg0-swap ]; then
  eval "$(sudo blkid --output export /dev/mapper/vg0-swap)"
  if [ "${UUID}" = "unknown" ]; then
    echo "could not determine block UUID of /dev/mapper/vg0-swap"
    exit 1
  fi
  echo "resume=UUID=${UUID}" | sudo tee /etc/cmdline.d/dotfiles-resume.conf
  echo "kernel_cmdline+=' resume=UUID=${UUID} '" | sudo tee /etc/dracut.conf.d/dotfiles-resume.conf

elif sudo btrfs inspect-internal map-swapfile /swap/swapfile >/dev/null 2>&1; then
  OFFSET=$(sudo btrfs inspect-internal map-swapfile --resume-offset /swap/swapfile)

  UUID=unknown
  DEVNAME=$(mount | grep ' on /swap type btrfs ' | cut -d ' ' -f 1)
  eval "$(sudo blkid --output export ${DEVNAME})"
  if [ "${UUID}" = "unknown" ]; then
    echo "could not determine block UUID of /swap"
    exit 1
  fi

  echo "resume=UUID=${UUID} resume_offset=${OFFSET}" | sudo tee /etc/cmdline.d/dotfiles-resume.conf
  echo "kernel_cmdline+=' resume=UUID=${UUID} resume_offset=${OFFSET} '" | sudo tee /etc/dracut.conf.d/dotfiles-resume.conf
fi
