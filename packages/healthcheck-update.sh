#! /usr/bin/env sh

set -eu

if [ "$(uname)" != "Linux" ]; then
  # so far, only compatible with Linux systems
  exit 0
fi

if ! command -v git >/dev/null; then
  # need git to continue
  exit 0
fi

HEALTHCHECK_DIR="${HOME}/GitLab/jokeyrhyme/healthcheck"
if ! git -C "${HEALTHCHECK_DIR}" status >/dev/null 2>&1; then
  mkdir -p "$(dirname ${HEALTHCHECK_DIR})"
  git clone https://gitlab.com/jokeyrhyme/healthcheck.git "${HEALTHCHECK_DIR}"
fi

git -C "${HEALTHCHECK_DIR}" pull
sudo make -C "${HEALTHCHECK_DIR}" install
