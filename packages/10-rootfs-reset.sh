#! /usr/bin/env sh

# rootfs: changes to /usr and /etc
# reset: undo changes made by ./rootfs-update.sh script

# prefer naming files with "dotfiles" in the name,
# so that we can cleanup with wildcards instead of explicit filenames

set -eu

if [ "$(uname)" != "Linux" ]; then
  # so far, only needed on Linux systems
  exit 0
fi

sudo rm -f /etc/cmdline.d/*dotfiles*
sudo rm -f /etc/makepkg.conf.d/*dotfiles*
sudo rm -f /etc/mkinitcpio.conf.d/*dotfiles*
sudo rm -f /etc/pacman.d/hooks/*dotfiles*

sudo rm -f /etc/dracut.conf.d/*dotfiles*
sudo rm -f /etc/modprobe.d/*dotfiles*
sudo rm -f /etc/polkit-1/rules.d/*dotfiles*
sudo rm -f /etc/security/limits.d/*dotfiles*
sudo rm -f /etc/sudoers.d/*dotfiles*
sudo rm -f /etc/sysctl.d/*dotfiles*

sudo rm -f /etc/systemd/*.conf.d/*dotfiles*
sudo rm -f /etc/systemd/network/*dotfiles*
sudo rm -rf /etc/systemd/system/login.conf.d
sudo rm -f /etc/systemd/system/*.d/*dotfiles*
sudo rm -f /etc/systemd/system/*dotfiles*
sudo rm -f /etc/systemd/system/greetd.service.d/override.conf
sudo rm -f /etc/systemd/system/systemd-networkd-wait-online.service.d/override.conf
sudo rm -f /etc/systemd/system/update-and-reboot.service
sudo rm -f /etc/systemd/system/update-and-reboot.timer
sudo rm -f /etc/systemd/user/*.d/*dotfiles*
sudo rm -f /etc/systemd/user-environment-generators/*dotfiles*

sudo rm -f /etc/default/cpupower
sudo rm -f /etc/tlp.d/*dotfiles*

sudo rm -f /usr/local/bin/*dotfiles* /usr/share/wayland-sessions/*dotfiles*
sudo rm -f /usr/share/polkit-1/actions/*dotfiles*

# shell expansions/globs are evaluated prior to passing to `sudo`,
# so we can't use `*dotfiles*` for files that are not visible to the non-root user
sudo rm -f /var/lib/rancher/k3s/server/manifests/dotfiles-traefik.yaml

sudo rm -rf /var/log/sysstat
