#! /usr/bin/env sh

set -eu

# shellcheck source=../lib/helpers.sh
. "$(dirname $0)/../lib/helpers.sh"

# shellcheck source=../lib/systemd.sh
. "$(dirname $0)/../lib/systemd.sh"

if ! __dotfiles_is_distro_debian; then
  exit 0
fi

sudo apt-get autoremove --yes
sudo apt-get update --yes
sudo apt-get upgrade --yes
sudo apt-get autoclean --yes

if ! __dotfiles_is_distro_raspbian; then
  exit 0
fi

sudo systemctl daemon-reload
__dotfiles_systemctl_enable dotfiles-update-and-reboot.timer
