#! /usr/bin/env bash

set -eu

if ! command -v systemctl >/dev/null 2>&1; then
  exit 0
fi

# shellcheck source=../lib/systemd.sh
. "$(dirname $0)/../lib/systemd.sh"

HOSTNAME=$("${HOME}/.dotfiles/bin/detect-hostname.sh")

if [[ ${HOSTNAME} == "rpi-k3s-"* ]] || [ "${HOSTNAME}" = "pihole" ]; then
  # these systems host resolver services,
  # so we don't want systemd-resolved's stub on port 53 here
  sudo systemctl disable --now systemd-resolved.service
  sudo systemctl mask systemd-resolved.service
  if [ "$(stat --format '%F' /etc/resolv.conf)" = "symbolic link" ]; then
    sudo rm -fv /etc/resolv.conf
  fi
  if [ ! -f /etc/resolv.conf ]; then
    # TODO auto-detect rather than hardcode
    echo "nameserver 192.168.1.1" | sudo tee /etc/resolv.conf
  fi

elif __dotfiles_is_systemd_unit systemd-resolved.service; then
  # otherwise, we want to default to systemd-resolved (if available)
  sudo systemctl unmask systemd-resolved.service
  sudo systemctl enable --now systemd-resolved.service
  sudo ln --force --symbolic /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
fi

if systemctl is-active systemd-networkd.service >/dev/null; then
  sudo systemctl restart systemd-networkd.service
fi
if __dotfiles_is_systemd_unit systemd-networkd-wait-online.service; then
  sudo systemctl restart systemd-networkd-wait-online.service
fi
