#! /usr/bin/env bash

set -eu -o pipefail

if [ "$(uname)" != "Linux" ]; then
  # so far, only needed on Linux systems
  exit 0
fi
if ! command -v firewall-cmd >/dev/null 2>&1; then
  exit 0
fi

# shellcheck source=../lib/helpers.sh
. "$(dirname $0)/../lib/helpers.sh"

echo 'copying into /etc/firewalld/ ...'
sudo cp -rv ~/.dotfiles/etc/firewalld/* /etc/firewalld/
sudo systemctl restart firewalld.service # to re-read changed files in /etc/firewalld/

# reset to predefined zones and their defaults,
# delete any custom zones
echo "firewalld: remove default services ..."
for ZONE in $(sudo firewall-cmd --permanent --get-zones); do
  for SERVICE in $(sudo firewall-cmd --permanent --zone=${ZONE} --list-services); do
    sudo firewall-cmd --permanent --zone=${ZONE} --remove-service=${SERVICE} >/dev/null 2>&1 || true
  done
done

if ! __dotfiles_is_distro_archlinux; then
  echo "firewalld: mapping traffic from local sources to internal zone..."

  # assume that only Archlinux has a modern-enough firewalld
  sudo firewall-cmd --permanent --zone=internal --add-source=ipset:dotfiles-local
  sudo firewall-cmd --permanent --zone=internal --add-source=ipset:dotfiles-local6
else
  echo "firewalld: rich rules for traffic to/from local sources ..."

  sudo firewall-cmd --permanent --zone=external --add-rich-rule='rule family="ipv4" source not ipset="dotfiles-local" destination ipset="dotfiles-local" accept'
  sudo firewall-cmd --permanent --zone=external --add-rich-rule='rule family="ipv6" source not ipset="dotfiles-local6" destination ipset="dotfiles-local6" accept'

  sudo firewall-cmd --permanent --zone=internal --add-rich-rule='rule family="ipv4" source ipset="dotfiles-local" destination ipset="dotfiles-local" accept'
  sudo firewall-cmd --permanent --zone=internal --add-rich-rule='rule family="ipv6" source ipset="dotfiles-local6" destination ipset="dotfiles-local6" accept'
fi

echo "firewalld: policy: local-to-world ..."
# trust traffic from (e.g.) docker containers outbound for the internet
# https://firewalld.org/2024/04/strictly-filtering-docker-containers
sudo firewall-cmd --permanent --new-policy local-to-world
sudo firewall-cmd --permanent --policy local-to-world --add-ingress-zone internal
sudo firewall-cmd --permanent --policy local-to-world --add-egress-zone ANY
sudo firewall-cmd --permanent --policy local-to-world --set-target ACCEPT
sudo firewall-cmd --permanent --policy local-to-world --add-masquerade

# --set-default-zone can fail if chain already exists
sudo firewall-cmd --set-default-zone=public >/dev/null 2>&1 || true

echo "firewalld: mapping interfaces to public zone ..."
# TODO: less hardcoding
for INTERFACE in $(ip link | grep --before-context=1 'link/ether' | grep --only-matching '^[[:digit:]]\+: [[:alnum:]]\+: ' | cut --delimiter=: --fields=2); do
  if [ "${INTERFACE}" = "docker0" ]; then
    # leave docker's virtual interface alone, docker assigns it to the "docker" zone automatically
    continue
  fi
  sudo firewall-cmd --permanent --zone=public --change-interface=${INTERFACE} >/dev/null 2>&1
done

echo "firewalld: services for public zone ..."
sudo firewall-cmd --permanent --zone=public --add-service=dhcpv6-client

echo "firewalld: services for internal zone ..."
sudo firewall-cmd --permanent --zone=internal --add-service=cockpit
sudo firewall-cmd --permanent --zone=internal --add-service=samba-client
sudo firewall-cmd --permanent --zone=internal --add-service=ssh
sudo firewall-cmd --permanent --zone=internal --add-service=steam-streaming
sudo firewall-cmd --permanent --zone=internal --add-service=syncthing
sudo firewall-cmd --permanent --zone=internal --add-service=upnp-client
sudo firewall-cmd --permanent --zone=internal --add-service=wireguard

HOSTNAME=$("${HOME}/.dotfiles/bin/detect-hostname.sh")

if [ "${HOSTNAME}" = "helios64" ]; then
  echo "firewalld: services for ${HOSTNAME} ..."
  sudo firewall-cmd --permanent --zone=internal --add-service=samba
fi

if [[ ${HOSTNAME} == "rpi-k3s-"* ]]; then
  echo "firewalld: services for ${HOSTNAME} ..."
  # traffic can only match one source-based zone,
  # so rather than have a cluster-specific source-based zone,
  # we'll just have to reuse the internal zone for now
  # TODO: block local addresses that are not part of the cluster
  sudo firewall-cmd --permanent --zone=internal --add-service=dotfiles-metallb
  sudo firewall-cmd --permanent --zone=internal --add-service=etcd-client
  sudo firewall-cmd --permanent --zone=internal --add-service=etcd-server
  sudo firewall-cmd --permanent --zone=internal --add-service=kube-apiserver
  sudo firewall-cmd --permanent --zone=internal --add-service=kube-nodeport-services
  sudo firewall-cmd --permanent --zone=internal --add-service=kubelet

  # needed for running pi-hole on the k8s/k3s cluster
  sudo firewall-cmd --permanent --zone=public --add-service=dhcp
  sudo firewall-cmd --permanent --zone=public --add-service=dhcpv6
  sudo firewall-cmd --permanent --zone=internal --add-service=dhcp
  sudo firewall-cmd --permanent --zone=internal --add-service=dhcpv6
  sudo firewall-cmd --permanent --zone=internal --add-service=dns
  sudo firewall-cmd --permanent --zone=internal --add-service=http
fi

sudo systemctl restart firewalld.service # to reload the new configuration
