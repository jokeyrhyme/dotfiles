#! /usr/bin/env sh

# install things we want to see (and can acquire) everywhere,
# don't add packages here that are for specific roles or distros

set -eu

# shellcheck source=../lib/helpers.sh
. "$(dirname $0)/../lib/helpers.sh"

# shellcheck source=../lib/systemd.sh
. "$(dirname $0)/../lib/systemd.sh"

if command -v systemctl >/dev/null; then
  echo "configuring systemd for basics ..."
  sudo systemctl daemon-reload
  systemctl is-active acpid.service >/dev/null && sudo systemctl restart acpid.service
  systemctl is-active smartd.service >/dev/null && sudo systemctl restart smartd.service
  __dotfiles_systemctl_enable acpid.service lid-button.service

  # smartd.service on Archlinux, smartmontools.service on Raspberry Pi OS
  for SERVICE in smartd.service smartmontools.service; do
    if [ -e /dev/sda ] || [ -e /dev/nvme0 ]; then
      __dotfiles_systemctl_enable "${SERVICE}"
    else
      __dotfiles_systemctl_disable "${SERVICE}"
    fi
  done

  if lsmod | /usr/bin/grep -i btusb >/dev/null 2>&1; then
    __dotfiles_systemctl_enable bluetooth.service
  fi
  __dotfiles_systemctl_enable avahi-daemon.service clamav-freshclam.service
  __dotfiles_systemctl_enable cockpit.socket pmlogger.service
  __dotfiles_systemctl_enable firewalld.service
  __dotfiles_systemctl_enable upower.service

  # prefer iwd
  if __dotfiles_is_systemd_unit iwd.service; then
    __dotfiles_systemctl_disable wpa_supplicant.service
    # TODO: handle wpa_supplicant@*.service e.g. wpa_supplicant@wlan.service
    __dotfiles_systemctl_enable iwd.service
  fi

  # prefer systemd-networkd
  if __dotfiles_is_systemd_unit systemd-networkd.service; then
    __dotfiles_systemctl_disable NetworkManager.service
    __dotfiles_systemctl_enable systemd-networkd.service
  fi

  sudo systemctl unmask systemd-rfkill.service systemd-rfkill.socket

  # seems weirdly broken everywhere
  # see: https://bugs.launchpad.net/ubuntu/+source/fwupd/+bug/1969976
  # see: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=943343
  __dotfiles_systemctl_disable fwupd-refresh.timer
fi
