#! /usr/bin/env sh

set -eu

if [ "$(uname)" != "Linux" ]; then
  # for now, let's assume (managed) macOS meets security requirements
  exit 0
fi

# shellcheck source=../lib/helpers.sh
. "$(dirname $0)/../lib/helpers.sh"

# see: https://man.archlinux.org/man/login.defs.5
if grep ENCRYPT_METHOD /etc/login.defs >/dev/null; then
  CONFIG_TEMP=$(mktemp)
  sed \
    -e 's/^ENCRYPT_METHOD[[:space:]]\+[[:alnum:]]\+$/ENCRYPT_METHOD SHA512/g' \
    >"${CONFIG_TEMP}" </etc/login.defs
  __dotfiles_mv_if_diff "${CONFIG_TEMP}" /etc/login.defs
else
  echo "ENCRYPT_METHOD SHA512" | sudo tee --append /etc/login.defs
fi

# see: https://man.archlinux.org/man/login.defs.5
if grep SHA_CRYPT_MIN_ROUNDS /etc/login.defs >/dev/null; then
  CONFIG_TEMP=$(mktemp)
  sed \
    -e 's/^SHA_CRYPT_MIN_ROUNDS[[:space:]]\+[[:digit:]]\+$/SHA_CRYPT_MIN_ROUNDS 500000/g' \
    >"${CONFIG_TEMP}" </etc/login.defs
  __dotfiles_mv_if_diff "${CONFIG_TEMP}" /etc/login.defs
else
  # the default is 5000, seems a bit low
  echo "SHA_CRYPT_MIN_ROUNDS 500000" | sudo tee --append /etc/login.defs
fi

# see: https://man.archlinux.org/man/core/pam/pam_unix.8.en
for FILE in /etc/pam.d/chpasswd /etc/pam.d/common-password /etc/pam.d/newusers; do
  if [ -f "${FILE}" ]; then
    CONFIG_TEMP=$(mktemp)
    sed \
      -e 's/^password[[:space:]]\+required[[:space:]]\+pam_unix.so sha512 shadow$/password required pam_unix.so sha512 shadow rounds=500000/g' \
      -e 's/^password[[:space:]]\+\[success=1 default=ignore\][[:space:]]\+pam_unix.so obscure sha512$/password [success=1 default=ignore] pam_unix.so obscure sha512 rounds=500000/g' \
      >"${CONFIG_TEMP}" <"${FILE}"
    __dotfiles_mv_if_diff "${CONFIG_TEMP}" "${FILE}"
  fi
done
