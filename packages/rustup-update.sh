#! /usr/bin/env sh

# make sure `rustup` is available,
# but leave updates/etc to our `tuning` setup

set -eu

if [ ! -x ~/.cargo/bin/rustup ] && command -v curl >/dev/null; then
  curl https://sh.rustup.rs -sSf | sh -s -- --no-modify-path --profile minimal -y
fi

if command -v rustup >/dev/null; then
  # prune unused toolchains
  for TOOLCHAIN in $(rustup toolchain list); do
    if echo "${TOOLCHAIN}" | grep "default" >/dev/null 2>&1; then
      continue
    fi
    if echo "${TOOLCHAIN}" | grep "stable" >/dev/null 2>&1; then
      continue
    fi
    rustup toolchain uninstall "${TOOLCHAIN}"
  done
fi
