#! /usr/bin/env sh

set -eu

if [ "$(uname)" != "Linux" ]; then
  # so far, only needed on Linux systems
  exit 0
fi

# shellcheck source=../lib/helpers.sh
. "$(dirname $0)/../lib/helpers.sh"

for CONFIG_FILE in /etc/ssl/openssl.cnf /etc/ssl/openssl.cnf.dist; do
  if [ -f "${CONFIG_FILE}" ]; then
    # https://askubuntu.com/questions/1409458/openssl-config-cuases-error-in-node-js-crypto-how-should-the-config-be-updated
    CONFIG_TEMP=$(mktemp)
    sed \
      -e 's/^providers = provider_sect/#providers = provider_sect/g' \
      >"${CONFIG_TEMP}" <"${CONFIG_FILE}"
    __dotfiles_mv_if_diff "${CONFIG_TEMP}" "${CONFIG_FILE}"
  fi
done
