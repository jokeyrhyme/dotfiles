#! /usr/bin/env sh

# configure snapper for btrfs / and $HOME,
# and enable scrub and balance schedules
# see: https://github.com/kdave/btrfsmaintenance#tuning-periodic-snapshotting

set -eu

if [ "$(uname)" != "Linux" ]; then
  # so far, only needed on Linux systems
  exit 0
fi

# shellcheck source=../lib/helpers.sh
. "$(dirname $0)/../lib/helpers.sh"

# shellcheck source=../lib/systemd.sh
. "$(dirname $0)/../lib/systemd.sh"

__dotfiles_snapper_ensure_config() { # $1 $2
  if ! sudo snapper -c "${1}" get-config >/dev/null 2>&1; then
    sudo snapper -c "${1}" create-config "${2}"
  fi
}

if ! mount | grep btrfs -c >/dev/null 2>&1; then
  # no btrfs volumes

  __dotfiles_systemctl_disable snapper-cleanup.timer
  __dotfiles_systemctl_disable snapper-timeline.timer
  __dotfiles_systemctl_disable btrfs-balance.timer
  __dotfiles_systemctl_disable btrfs-scrub.timer

  exit 0
fi

__dotfiles_systemctl_enable snapper-cleanup.timer
__dotfiles_systemctl_enable snapper-timeline.timer
__dotfiles_systemctl_enable btrfs-balance.timer
__dotfiles_systemctl_enable btrfs-scrub.timer

if command -v snapper >/dev/null && command -v systemctl >/dev/null; then
  if mount | grep " on / type btrfs " -c >/dev/null 2>&1; then
    __dotfiles_systemctl_enable snapper-boot.timer

    __dotfiles_snapper_ensure_config root /
    CONFIG_TEMP=$(mktemp)
    sudo cat /etc/snapper/configs/root | sed \
      -e 's/TIMELINE_LIMIT_HOURLY="[0-9]\+"/TIMELINE_LIMIT_HOURLY="12"/g' \
      -e 's/TIMELINE_LIMIT_DAILY="[0-9]\+"/TIMELINE_LIMIT_DAILY="5"/g' \
      -e 's/TIMELINE_LIMIT_WEEKLY="[0-9]\+"/TIMELINE_LIMIT_WEEKLY="2"/g' \
      -e 's/TIMELINE_LIMIT_MONTHLY="[0-9]\+"/TIMELINE_LIMIT_MONTHLY="1"/g' \
      -e 's/TIMELINE_LIMIT_YEARLY="[0-9]\+"/TIMELINE_LIMIT_YEARLY="0"/g' \
      >"${CONFIG_TEMP}"
    __dotfiles_mv_if_diff "${CONFIG_TEMP}" /etc/snapper/configs/root
  else
    __dotfiles_systemctl_disable snapper-boot.timer
  fi

  if mount | grep " on ${HOME} type btrfs " -c >/dev/null 2>&1; then
    __dotfiles_snapper_ensure_config "${USER}.homedir" "${HOME}"
    CONFIG_TEMP=$(mktemp)
    sudo cat "/etc/snapper/configs/${USER}.homedir" | sed \
      -e 's/TIMELINE_LIMIT_HOURLY="[0-9]\+"/TIMELINE_LIMIT_HOURLY="12"/g' \
      -e 's/TIMELINE_LIMIT_DAILY="[0-9]\+"/TIMELINE_LIMIT_DAILY="7"/g' \
      -e 's/TIMELINE_LIMIT_WEEKLY="[0-9]\+"/TIMELINE_LIMIT_WEEKLY="1"/g' \
      -e 's/TIMELINE_LIMIT_MONTHLY="[0-9]\+"/TIMELINE_LIMIT_MONTHLY="0"/g' \
      -e 's/TIMELINE_LIMIT_YEARLY="[0-9]\+"/TIMELINE_LIMIT_YEARLY="0"/g' \
      >"${CONFIG_TEMP}"
    __dotfiles_mv_if_diff "${CONFIG_TEMP}" "/etc/snapper/configs/${USER}.homedir"
  fi
fi
