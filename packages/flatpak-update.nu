#! /usr/bin/env nu

if not ((sys host).name | str contains 'Linux') {
    # so far, only needed on Linux systems
    exit 0
}

# assumption: rules are in place to allow user to `flatpak --system` without `sudo`, etc

mkdir ~/.var/app

# prefer `--system` to help prevent accidental/casual/malicious modification of package contents
^flatpak uninstall --all --assumeyes --delete-data --noninteractive --user

# remove `--user` remotes, too, per above preference for `--system`
^flatpak remotes --columns name --user |
    lines |
    each { |LINE| $LINE | str trim } | 
    filter { |LINE| $LINE | is-not-empty } |
    each { |REMOTE| ^flatpak remote-delete --force --user $REMOTE }

^sudo flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
^sudo flatpak remote-add --if-not-exists flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo

# remove all pins, many are historical and no longer needed
# TODO: determine which pins we intend/need
let PINNED = ^flatpak pin |
    lines |
    each { |LINE| $LINE | str trim } | 
    filter { |LINE| $LINE != 'Pinned patterns:' } |
    filter { |LINE| $LINE != 'No pinned patterns' }
if ($PINNED | is-not-empty) {
    # no default policy in Arch Linux for `sudo`-free `flatpak pin` 
    ^sudo flatpak pin --remove $PINNED
}

let FLATPAK_FOUND = (^flatpak list --columns application | lines)
print $"flatpak packages installed: ($FLATPAK_FOUND | length)"

# reset overrides for all apps,
# so that we have config-as-code for our flatpak app permissions
$FLATPAK_FOUND | each { |APP_ID|
    ^flatpak override --reset --system $APP_ID
    ^flatpak override --user --reset $APP_ID
}

[
  "--filesystem=xdg-config/google-chrome:create",
  "--filesystem=xdg-documents",
  "--filesystem=xdg-download",
  "--filesystem=xdg-pictures",
  "--filesystem=xdg-public-share",

  # for `rustup doc`, `cargo doc`, etc
  "--filesystem=xdg-run/doc ",

  # for open-source stuff
  "--filesystem=~/GitHub:create",
  "--filesystem=~/GitLab:create",

  # for PWA
  "--filesystem=~/.local/share/applications:create",
  "--filesystem=~/.local/share/icons:create",

  # for wayland and screenshare
  "--filesystem=xdg-run/pipewire-0",
  "--socket=wayland",
] | each { |OVERRIDE|
    # prefer `--user` so that overrides are per-user
    # (although, we don't have any multi-user machines, anyway)
    ^flatpak override --user $OVERRIDE 'com.google.Chrome'
}

[
  "--env=MOZ_ENABLE_WAYLAND=1",
  "--env=MOZ_WEBRENDER=1",
  "--filesystem=~/.mozilla:create",
  "--filesystem=xdg-documents",
  "--filesystem=xdg-download",
  "--filesystem=xdg-pictures",
  "--filesystem=xdg-public-share",

  # for `rustup doc`, `cargo doc`, etc
  "--filesystem=xdg-run/doc ",

  # for open-source stuff
  "--filesystem=~/GitHub:create",
  "--filesystem=~/GitLab:create",

  # for PWA
  "--filesystem=~/.local/share/applications:create",
  "--filesystem=~/.local/share/icons:create",

  # for wayland and screenshare
  "--filesystem=xdg-run/pipewire-0",
  "--socket=wayland",
]  | each { |OVERRIDE|
    # prefer `--user` so that overrides are per-user
    # (although, we don't have any multi-user machines, anyway)
    ^flatpak override --user $OVERRIDE 'org.mozilla.firefox'
}

# TODO: detect non-flatpak installation and set default accordingly
^xdg-settings set default-web-browser com.google.Chrome.desktop

^flatpak uninstall --delete-data --noninteractive --unused --assumeyes
^flatpak update --assumeyes
