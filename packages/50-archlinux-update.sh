#! /usr/bin/env bash

set -eu -o pipefail

# shellcheck source=../lib/helpers.sh
. "$(dirname $0)/../lib/helpers.sh"

# shellcheck source=../lib/systemd.sh
. "$(dirname $0)/../lib/systemd.sh"

if ! __dotfiles_is_distro_archlinux; then
  exit 0
fi

echo "installing favourites with 'pacman' ..."

# basics
sudo update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /usr/bin/wezterm 10
sudo update-alternatives --set x-terminal-emulator /usr/bin/wezterm

echo 'preparing for Secure Boot ...'
sudo ~/.dotfiles/bin/secure-boot-create-keys.sh

echo "removing unnecessary packages with 'pacman' ..."
# https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Removing_unused_packages_(orphans)
# `pacman` will return a non-zero exit code if there isn't anything to cleanup
if pacman -Q --deps --quiet --unrequired --unrequired; then
  pacman -Q --deps --quiet --unrequired --unrequired | sudo pacman -R --noconfirm --nosave --recursive - || true
fi

echo "updating packages with 'pacman' ..."
sudo pacman -S --refresh --sysupgrade

# `|| true`: ignore errors from analystics
pkgstats submit || true

sudo systemctl daemon-reload
__dotfiles_systemctl_enable clamav-daemon.socket cups.service dictd.service ollama.service
__dotfiles_systemctl_enable lid-button.service
__dotfiles_systemctl_enable paccache.timer reflector.timer
__dotfiles_systemctl_disable speech-dispatcherd.service

if "${HOME}/.dotfiles/bin/detect-nvidia-gpu.sh"; then
  if pacman -Qi nvidia-utils >/dev/null 2>&1; then
    __dotfiles_systemctl_enable nvidia-hibernate.service nvidia-persistenced.service nvidia-powerd.service nvidia-resume.service nvidia-suspend.service
  else
    __dotfiles_systemctl_disable nvidia-hibernate.service nvidia-persistenced.service nvidia-powerd.service nvidia-resume.service nvidia-suspend.service
  fi
fi

# https://pipewire.pages.freedesktop.org/wireplumber/running-wireplumber-daemon.html#replacing-pipewire-media-session
__dotfiles_systemctl_disable_user pipewire-media-session.service
__dotfiles_systemctl_enable_user wireplumber
