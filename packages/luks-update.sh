#! /usr/bin/env sh

set -eu

if [ "$(uname)" != "Linux" ]; then
  # so far, only needed on Linux systems
  exit 0
fi

for CMD in cryptsetup grep jq lsblk sudo; do
  if ! command -v "${CMD}" >/dev/null 2>&1; then
    printf 'warning: command "%s" not found, skipping ...' "${CMD}"
    exit 0
  fi
done

__dotfiles_get_crypt_name() { # $1 = device
  for NAME in $(lsblk --list --noheadings --output NAME "${1}"); do
    # assumes that we don't have an ambiguously-named mapping,
    # e.g. a mapping for "sda" that is named "sdb"
    if [ -e "/dev/mapper/${NAME}" ]; then
      echo "${NAME}"
      break
    fi
  done
}

for NAME in $(lsblk --list --noheadings --output NAME); do
  DEVICE="/dev/${NAME}"
  if [ ! -e "${DEVICE}" ]; then
    # name probably refers to a device mapper name, not a /dev device
    continue
  fi
  if ! sudo cryptsetup isLuks "${DEVICE}"; then
    # device is not a LUKS volume
    continue
  fi

  CRYPT_NAME=$(__dotfiles_get_crypt_name "${DEVICE}")
  if [ -z "${CRYPT_NAME}" ]; then
    # no LUKS mapping
    continue
  fi

  # convention: the LUKS mapping name should match the label on the partition
  sudo cryptsetup config --label "${CRYPT_NAME}" "${DEVICE}"

  OLD_PARAMETERS=""
  LUKS_FLAGS=$(sudo cryptsetup luksDump "${DEVICE}" | grep '^Flags:')
  if echo "${LUKS_FLAGS}" | grep 'allow-discards' >/dev/null; then
    OLD_PARAMETERS="${OLD_PARAMETERS} --allow-discards"
  fi
  if echo "${LUKS_FLAGS}" | grep 'no-read-workqueue' >/dev/null; then
    OLD_PARAMETERS="${OLD_PARAMETERS} --perf-no_read_workqueue"
  fi
  if echo "${LUKS_FLAGS}" | grep 'no-write-workqueue' >/dev/null; then
    OLD_PARAMETERS="${OLD_PARAMETERS} --perf-no_write_workqueue"
  fi

  PARAMETERS=""
  DISCARD_INFO=$(lsblk --discard --json --nodeps --output DISC-GRAN,DISC-MAX "${DEVICE}")
  # see: https://wiki.archlinux.org/title/Solid_state_drive#TRIM
  DISCARD_GRAN=$(echo "${DISCARD_INFO}" | jq --raw-output '.blockdevices[0]["disc-gran"]')
  DISCARD_MAX=$(echo "${DISCARD_INFO}" | jq --raw-output '.blockdevices[0]["disc-max"]')
  if [ "${DISCARD_GRAN}" != "0B" ] && [ "${DISCARD_MAX}" != "0B" ]; then
    PARAMETERS="${PARAMETERS} --allow-discards"
  fi
  if echo "${DEVICE}" | grep "^/dev/nvme" >/dev/null; then
    # see: https://blog.cloudflare.com/speeding-up-linux-disk-encryption/
    PARAMETERS="${PARAMETERS} --perf-no_read_workqueue --perf-no_write_workqueue"
  fi

  printf "LUKS volume: %s '%s'\n  got:  %s\n  want: %s\n" "${DEVICE}" "${CRYPT_NAME}" "${OLD_PARAMETERS}" "${PARAMETERS}"
  if [ "${OLD_PARAMETERS}" != "${PARAMETERS}" ]; then
    sudo cryptsetup refresh ${PARAMETERS} --persistent "${CRYPT_NAME}"
  fi
done
