#! /usr/bin/env bash

# values with escapable characters like spaces need to go here,
# simple single-word values should go in config/environment.d/10-dotfiles.conf

if command -v launchctl >/dev/null 2>&1; then
  # we hardcode /bin/launchctl here,
  # because $PATH briefly lacks /bin
  PATH="$(/bin/launchctl getenv PATH)"
  SSH_AUTH_SOCK="$(/bin/launchctl getenv SSH_AUTH_SOCK)"
  export PATH SSH_AUTH_SOCK
fi
# from: https://github.com/systemd/systemd/issues/7641#issuecomment-355832773
# read environment.d and environment-generator values into session
# (the grep is to skip $'values with whitespace' because they are problematic here)
# shellcheck disable=SC2046
if command -v systemctl >/dev/null 2>&1; then
  export $(systemctl --user show-environment | grep -v "=$'" | xargs)
fi
# TODO: per-session storage/retrieval without `systemctl`

if [ -n "${SSH_AUTH_SOCK}" ]; then
  if echo "${SSH_AUTH_SOCK-}" | grep '^/private/tmp/com.apple.launchd.' >/dev/null; then
    # ignore Apple's `ssh-agent`, because it does not support USB security keys
    unset SSH_AUTH_SOCK
  elif [ ! -w "${SSH_AUTH_SOCK-}" ]; then
    # current SSH socket isn't usable, ditch it
    unset SSH_AUTH_SOCK
  fi
fi
