#! /usr/bin/env sh

# setup environment variables for a macOS login session,
# mapping selected Linux variables over

set -eu

eval "$(~/.dotfiles/etc/systemd/user-environment-generators/90-dotfiles-compiler.conf)"
launchctl setenv CFLAGS "${CFLAGS:-}"
launchctl setenv CPPFLAGS "${CPPFLAGS:-}"
launchctl setenv CXXFLAGS "${CXXFLAGS:-}"
launchctl setenv LDFLAGS "${LDFLAGS:-}"
launchctl setenv CGO_CFLAGS "${CGO_CFLAGS:-}"
launchctl setenv CGO_CPPFLAGS "${CGO_CPPFLAGS:-}"
launchctl setenv CGO_CXXFLAGS "${CGO_CXXFLAGS:-}"
launchctl setenv CGO_LDFLAGS "${CGO_LDFLAGS:-}"
launchctl setenv GOFLAGS "${GOFLAGS:-}"
launchctl setenv GOLDFLAGS "${GOLDFLAGS:-}"
launchctl setenv MAKEFLAGS "${MAKEFLAGS:-}"
launchctl setenv RUSTFLAGS "${RUSTFLAGS:-}"
if [ -n "$CMAKE_C_COMPILER_LAUNCHER" ]; then
  launchctl setenv CMAKE_C_COMPILER_LAUNCHER "${CMAKE_C_COMPILER_LAUNCHER:-}"
fi
if [ -n "$CMAKE_CXX_COMPILER_LAUNCHER" ]; then
  launchctl setenv CMAKE_CXX_COMPILER_LAUNCHER "${CMAKE_CXX_COMPILER_LAUNCHER:-}"
fi
if [ -n "$RUSTC_WRAPPER" ]; then
  launchctl setenv RUSTC_WRAPPER "${RUSTC_WRAPPER:-}"
fi

eval "$(~/.dotfiles/etc/systemd/user-environment-generators/90-dotfiles-local.conf)"
eval "$(~/.dotfiles/etc/systemd/user-environment-generators/95-dotfiles-adr.conf)"
eval "$(~/.dotfiles/etc/systemd/user-environment-generators/95-dotfiles-golang.conf)"
eval "$(~/.dotfiles/etc/systemd/user-environment-generators/95-dotfiles-haskell.conf)"
eval "$(~/.dotfiles/etc/systemd/user-environment-generators/95-dotfiles-ruby.conf)"
if [ -d /opt/homebrew/bin ]; then
  # assume that output from `brew shellenv` never/rarely changes
  launchctl setenv HOMEBREW_PREFIX "/opt/homebrew"
  launchctl setenv HOMEBREW_CELLAR "/opt/homebrew/Cellar"
  launchctl setenv HOMEBREW_REPOSITORY "/opt/homebrew"
fi

eval "$(~/.dotfiles/etc/systemd/user-environment-generators/95-dotfiles-argc.conf)"
launchctl setenv ARGC_COMPLETIONS_PATH "${ARGC_COMPLETIONS_PATH:-}"
launchctl setenv ARGC_COMPLETIONS_ROOT "${ARGC_COMPLETIONS_ROOT:-}"

if [ -d /etc/paths.d ]; then
  for DROPIN in /etc/paths.d/*; do
    while read -r DIR; do
      case "${PATH-}" in
      *"${DIR}":*) ;;
      *"${DIR}") ;;
      *) PATH="${PATH}:${DIR}" ;;
      esac
    done <"${DROPIN}"
  done
fi
launchctl setenv PATH "${PATH}"

MANPATH=/usr/share/man
if [ -d /etc/manpaths.d ]; then
  for DROPIN in /etc/manpaths.d/*; do
    while read -r DIR; do
      case "${MANPATH-}" in
      *"${DIR}":*) ;;
      *"${DIR}") ;;
      *) MANPATH="${MANPATH}:${DIR}" ;;
      esac
    done <"${DROPIN}"
  done
fi
launchctl setenv MANPATH "${MANPATH}"

eval "$(cat ~/.dotfiles/config/environment.d/10-dotfiles.conf)"
launchctl setenv CLICOLOR "${CLICOLOR:-}"
launchctl setenv DIFFPROG "${DIFFPROG:-}"
launchctl setenv LANGUAGE "${LANGUAGE}"
launchctl setenv RIPGREP_CONFIG_PATH "${RIPGREP_CONFIG_PATH:-}"
