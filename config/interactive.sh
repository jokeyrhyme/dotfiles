#! /usr/bin/env sh

. "${HOME}/.dotfiles/lib/devices.sh"

__dotfiles_is_muxer_needed() {
  [ -z "${ZELLIJ:-}" ] && {
    # alacritty has no built-in tabs/panes functionality
    [ -n "${ALACRITTY_SOCKET:-}" ] ||
      # linux virtual console has no built-in tabs/panes functionality
      [ "${TERM:-}" = "linux" ]
  }
}

if __dotfiles_is_muxer_needed && command -v zellij >/dev/null; then
  # ensure zellij
  # TODO: re-attach to the first detached session found
  if command -v systemd-run >/dev/null; then
    # use systemd to allow detached zellij sessions to keep running after logout
    ZELLIJ_UNIT_ID=zellij-$(date +%s.%N)
    exec systemd-run --same-dir --scope --unit "${ZELLIJ_UNIT_ID}" --user zellij
  else
    exec zellij
  fi
fi

# start an SSH agent if we do not have one yet
if [ -z "${SSH_AUTH_SOCK-}" ]; then
  if [ -x /opt/homebrew/bin/ssh-agent ]; then
    eval "$(/opt/homebrew/bin/ssh-agent -s)"
  elif command -v ssh-agent >/dev/null; then
    eval "$(ssh-agent -s)"
  fi
  export SSH_AUTH_SOCK
fi
if [ -n "${SSH_AUTH_SOCK-}" ]; then
  # propagate SSH_AUTH_SOCK to dbus/systemd environment
  if [ -n "${DISPLAY-}" ] && command -v dbus-update-activation-environment >/dev/null; then
    dbus-update-activation-environment SSH_AUTH_SOCK
  fi
  if command -v launchctl >/dev/null; then
    launchctl setenv SSH_AUTH_SOCK "${SSH_AUTH_SOCK}"
  fi
  if command -v systemctl >/dev/null; then
    # make sure we reuse the one SSH agent for the whole user session
    systemctl --user import-environment SSH_AUTH_SOCK
  fi

  # prepare smartcard/yubikey for SSH use
  # see: https://aus.social/@jokeyrhyme/109129472870400757
  if __dotfiles_has_precursor || __dotfiles_has_yubikey; then
    # there's a Yubico device attached, so let's setup
    if ! ssh-add -l | grep --quiet "(ED25519-SK)"; then
      # no FIDO2 key registered with SSH agent yet
      # shellcheck disable=SC2010
      if [ -x "/opt/homebrew/bin/ssh-add" ]; then
        /opt/homebrew/bin/ssh-add -K || true
      else
        ssh-add -K || true
      fi
    fi
    if [ -f /usr/local/lib/libykcs11.dylib ]; then
      if ! ssh-add -l | grep --quiet "(ED25519-SK)"; then
        # no Yubico PKCS11 key registered with SSH agent yet
        if [ -x "/opt/homebrew/bin/ssh-add" ]; then
          /opt/homebrew/bin/ssh-add -s /usr/local/lib/libykcs11.dylib || true
        else
          ssh-add -s /usr/local/lib/libykcs11.dylib || true
        fi
      fi
    fi
  fi
fi

if command -v helix >/dev/null; then
  export EDITOR=helix
elif command -v nvim >/dev/null; then
  export EDITOR=nvim
else
  export EDITOR=vim
fi

# GPG_TTY is needed for GPG passphrase prompts
GPG_TTY="$(tty)"
export GPG_TTY

# https://opensource.com/article/18/5/advanced-use-less-text-file-viewer
export LESS="-C -M -I -j 10 -x 2 -# 4 -R"
export PAGER=less
if command -v bat >/dev/null 2>&1; then
  export MANPAGER="bat --language man --decorations=never"
  export PAGER="bat --decorations=never"
fi
