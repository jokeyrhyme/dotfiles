local wezterm = require("wezterm")

local config = {}
if wezterm.config_builder then
  config = wezterm.config_builder()
end

if wezterm.gui then
  -- from: https://wezfurlong.org/wezterm/config/lua/config/webgpu_preferred_adapter.html
  -- enable WebGPU only when we have an iGPU and a Vulkan driver for it
  config.front_end = "OpenGL"
  local gpus = wezterm.gui.enumerate_gpus()
  for _, gpu in ipairs(gpus) do
    if gpu.backend == "Vulkan" and gpu.device_type == "IntegratedGpu" then
      config.webgpu_preferred_adapter = gpu
      config.front_end = "WebGpu"
      break
    end
  end

  -- this is relatively expensive,
  -- but provides us with the most reliable detection
  local success, _, _ = wezterm.run_child_process({ "infocmp", "wezterm" })
  if success then
    config.term = "wezterm"
  end

  local color_scheme = "rose-pine"
  if wezterm.gui.get_appearance() == "Light" then
    color_scheme = "rose-pine-dawn"
  end

  local colors = wezterm.get_builtin_color_schemes()[color_scheme]
  colors.selection_bg = "#403d52" -- highlight medium
  colors.selection_fg = colors.brights[7] -- rose
  colors.tab_bar = {
    active_tab = {
      bg_color = colors.background,
      fg_color = colors.brights[7], -- rose
      intensity = "Bold",
    },
    background = colors.background,
    inactive_tab = {
      bg_color = colors.background,
      fg_color = colors.foreground,
    },
    inactive_tab_hover = {
      bg_color = colors.background,
      fg_color = colors.brights[3], -- foam
    },
  }
  config.color_scheme = color_scheme
  config.colors = colors
end

local environment_variables = {}
if wezterm.target_triple == "aarch64-apple-darwin" then
  config.font_size = 16.0
  -- on macOS, we're avoiding having to change too many files outside of $HOME,
  -- so we have to change this here, instead of modifying /etc/shells
  environment_variables["SHELL"] = "/opt/homebrew/bin/nu"
end

config.check_for_updates = false
config.enable_kitty_keyboard = true
config.font = wezterm.font_with_fallback({
  "JetBrains Mono",
  "Noto Sans Mono",
  "Libertinus Mono",
  "Noto Color Emoji",
  "Noto Sans Symbols",
  "Noto Sans Symbols2",
  "Symbols Nerd Font",
})
config.hide_tab_bar_if_only_one_tab = false
config.initial_cols = 100
config.initial_rows = 50
config.set_environment_variables = environment_variables
config.show_new_tab_button_in_tab_bar = false
config.status_update_interval = 797 -- a little more-frequent than the default 1000
config.tab_max_width = 32
config.use_fancy_tab_bar = false
config.use_resize_increments = false
config.visual_bell = {
  fade_in_duration_ms = 97,
  fade_out_duration_ms = 97,
}
config.window_decorations = "RESIZE"

return config
