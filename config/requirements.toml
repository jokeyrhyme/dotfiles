[[group]]
description = "requirements for ~/.dotfiles/crates/dotfiles-packages-rs"
package = ["curl", "fakeroot", "gcc", "pkgconf"]

[[group]]
description = "basics"
package = [
  "bash",
  "direnv",
  "git",
  "jq",
  "lsof",
  "man-db",
  "neovim",
  "ripgrep",
  "s-tui",
  "time",
  "uutils-coreutils",
  "zsh",
]

[[group]]
description = "basics [arch_linux]"
package = [
  "arch-audit",
  "archlinux-contrib",
  "aura",
  "pacman-contrib",
  "pkgstats",
  "reflector",
]
when = ["arch_linux"]

[[group]]
description = "basics [arch_linux][btrfs_root]"
package = ["snap-pac"]
when = ["arch_linux", "btrfs_root"]

[[group]]
description = "basics [linux]"
package = [
  "base",
  "base-files",
  "base-passwd",
  "dpkg",
  "dracut",
  "lvm2",           # TODO only on systems using LVM
  "snapper",        # TODO only on systems using btrfs or LVM
  "udisks2-lvm2",   # TODO only on systems using LVM
  "udisks2-zram",
  "zram-generator",
]
when = ["linux"]

[[group]]
description = "basics [btrfs][linux]"
package = ["httm", "btrfs-progs", "udisks2-btrfs"]
when = ["btrfs", "linux"]

[[group]]
description = "basics [desktop]"
package = [
  "alacritty",
  "atuin",
  "argc",
  "bat",
  "bottom",
  "d2",
  "difftastic",
  "duf",
  "dust",
  "fd",
  "flavours",
  "gnuplot",
  "nushell",
  "pueue",
  "repgrep",
  "sd",
  "skim",
  "tealdeer",
  "tuning",
  "wezterm",
  "yq",         # required by `argc`
  "zellij",
]
when = ["desktop"]

[[group]]
description = "basics [desktop][linux]"
package = [
  "dosfstools",
  "exfatprogs",
  "flatpak",
  "fuse2",           # required for AppImage apps
  "greetd",          # handy when not using GDM, and also needed for cosmic-greeter
  "greetd-tuigreet",
  "kmon",
  "linux",
  "linux-lts",
  "webkit2gtk",      # needed for AppImage apps
  "webkit2gtk-4.1",  # needed for AppImage apps
  "xdg-user-dirs",
  "xdg-utils",
]
when = ["desktop", "linux"]

[[group]]
description = "encryption & security"
package = ["age", "gnupg", "lynis", "sequoia-sq", "ssh-sensible"]

[[group]]
description = "encryption & security [linux]"
package = [
  "clamav",
  "cryptsetup",
  "efibootmgr",
  "efitools",
  "firewalld",
  "ipset",
  "nftables",
  "ssh-sensible",
  "sudo",
  "usbguard",
]
when = ["linux"]

[[group]]
description = "encryption & security [desktop]"
package = [
  "bitwarden",
  "libfido2",
  "seahorse",
  "sbctl",
  "sbsigntools",
  "sops",
  "yubico-piv-tool",
  "yubikey-manager",
]
when = ["desktop"]

[[group]]
description = "encryption & security [desktop][work]"
package = ["libxcrypt-compat"]
when = ["desktop", "work"]

[[group]]
description = "developer tools"
package = ["base-devel", "mold", "perf", "sccache"]

[[group]]
description = "developer tools [desktop]"
package = [
  "alex",
  "bash-language-server",
  "cargo-audit",
  "cargo-crev",
  "cargo-flamegraph",
  "cargo-mutants",
  "clang",
  "cmake",
  "cross",
  "delve",
  "efm-langserver",
  "eslint",
  "genact",
  "gimoji",
  "gitoxide",
  "go",
  "gofumpt",
  "gopls",
  "harper",
  "hyperfine",
  "jujutsu",
  "just",
  "lefthook",
  "lld",
  "lldb",
  "llvm",
  "lsp-ai",
  "lua-language-server",
  "luacheck",
  "luarocks",
  "mise",
  "mkcert",
  "npm",
  "oxlint",
  "prettier",
  "python-lsp-server",
  "python-pipx",
  "revive",
  "semgrep",
  "shfmt",
  "srgn",
  "stylua",
  "tokei",
  "typescript",
  "typescript-language-server",
  "typos",
  "vale",
  "valgrind",
  "vscode-langservers-extracted",
  "write-good",
  "yaml-language-server",
  "yarn",
]
when = ["desktop"]

[[group]]
description = "developer tools [desktop][work]"
package = [
  "jdk17-openjdk",
  "jdtls",
  "jetbrains-toolbox",
  "kotlin-language-server",
  "maven",
  "python-virtualenv",
  "terraform",
  "terraform-ls",
]
when = ["desktop", "work"]

[[group]]
description = "devices, firmware, and drivers [linux]"
package = [
  "acpi",
  "acpid",
  "bluez",
  "bolt",
  "firmware-realtek",
  "fwupd",
  "hdparm",
  # linux-headers is needed for DKMS
  "linux-headers",
  "linux-firmware",
  # linux-headers is needed for DKMS
  "linux-lts-headers",
  "parted",
  "powertop",
  "smartmontools",
  "tpm2-tools",
  "upower",
  "usbutils",
  "v4l-utils",
  "wireless-regdb",
]
when = ["linux"]

[[group]]
description = "devices, firmware, and drivers [desktop]"
package = ["ghostscript", "keymapp", "zsa-wally-cli"]
when = ["desktop"]

[[group]]
description = "devices, firmware, and drivers [amd_cpu][linux]"
package = ["amd-ucode"]
when = ["amd_cpu", "linux"]

[[group]]
description = "devices, firmware, and drivers [amd_gpu][linux]"
package = [
  "amdgpu_top",
  # "amdvlk", # disabled for now, very buggy with the wgpu Rust crate
  "rocm-clang-ocl",
  "rocm-hip-libraries",
  "rocm-hip-runtime",
  "vulkan-radeon",
]
when = ["amd_gpu", "linux"]

[[group]]
description = "devices, firmware, and drivers [desktop][linux]"
package = [
  "blueman",
  "brightnessctl",
  "cups",
  "cups-filters",
  "cups-pk-helper",
  "memtest86+-efi",
  "pappl",
  "pavucontrol",
  "pipewire",
  "pipewire-pulse",
  "system-config-printer",
  "vulkan-intel",
  "wireplumber",
]
when = ["desktop", "linux"]

[[group]]
description = "devices, firmware, and drivers [intel_cpu][linux]"
package = ["intel-ucode"]
when = ["intel_cpu", "linux"]

[[group]]
description = "devices, firmware, and drivers [nvidia_gpu][linux]"
package = ["nvidia-open-dkms", "nvidia-utils"]
when = ["nvidia_gpu", "linux"]

[[group]]
description = "devices, firmware, and drivers [raspberry_pi]"
package = [
  "hostapd",
  "linux-headers-arm64",
  "linux-headers-rpi-v8",
  "linux-image-arm64",
  "linux-image-rpi-v8",
  "raspberrypi-bootloader",
  "raspberrypi-kernel",
  "raspberrypi-kernel-headers",
  "raspi-config",
  "raspi-firmware",
  "rpi-eeprom",
  "rpi-update",
  "sense-hat",
]
when = ["raspberry_pi"]

[[group]]
description = "devices, firmware, and drivers [desktop][personal]"
package = [
  "freecad",
  "openscad",
  "precursorupdater",
  "prusa-slicer",
  "rpi-imager",
]
when = ["desktop", "personal"]

[[group]]
description = "docker, kubernetes, virtualisation, etc [desktop]"
package = [
  "docker",
  "docker-buildx",
  "docker-compose",
  "helm",
  "k9s",
  "kdash",
  "kubectl",
  "minikube",
  "popeye",
  "qemu-user-static",
  "qemu-user-static-binfmt",
]
when = ["desktop"]

[[group]]
description = "docker, kubernetes, virtualisation, etc [desktop][linux][personal]"
package = ["bottles"]
when = ["desktop", "linux", "personal"]

[[group]]
description = "docker, kubernetes, virtualisation, etc [desktop][work]"
package = ["packer", "vagrant", "virtualbox", "virtualbox-host-dkms"]
when = ["desktop", "work"]

[[group]]
description = "entertainment, media, etc, [desktop]"
package = [
  "espeak-ng",
  "festival",
  "festival-english",
  "gimp",
  "inkscape",
  "krita",
  "mpv",
  "obs-studio",
  "speech-dispatcher",
  "zathura",
  "zathura-pdf-poppler",
]
when = ["desktop"]


[[group]]
description = "entertainment, media, etc, [desktop][linux]"
package = [
  "gst-plugins-base",
  "gst-plugins-good",
  "helvum",
  "loupe",
  "playerctl",
]
when = ["desktop", "linux"]

[[group]]
description = "entertainment, media, etc, [desktop][personal]"
package = ["czkawka", "musescore"]
when = ["desktop", "personal"]

[[group]]
description = "chat, communication, social [desktop]"
package = ["discord", "signal-desktop", "slack-desktop"]
when = ["desktop"]

[[group]]
description = "chat, communication, social [desktop][personal]"
package = ["element-desktop", "jitsi-meet", "mattermost-desktop", "zulip"]
when = ["desktop", "personal"]

[[group]]
description = "chat, communication, social [desktop][work]"
package = ["zoom"]
when = ["desktop", "work"]

[[group]]
description = "games [desktop]"
package = ["minecraft-launcher"]
when = ["desktop"]

[[group]]
description = "games [desktop][personal]"
package = ["heroic-games-launcher", "steam"]
when = ["desktop", "personal"]

[[group]]
description = "networking, protocols, etc"
package = [
  "avahi",
  "bind",
  "bmon",
  "cyrus-sasl-gssapi",
  "curl",
  "dog",
  "iptraf-ng",
  "nmap",
  "openssh",
  "picocom",
  "wget",
  "wireguard-tools",
  "xh",
]

[[group]]
description = "networking, protocols, etc [desktop]"
package = [
  "bandwhich",
  "firefox",
  "gping",
  "google-chrome",
  "oha",
  "rustscan",
  "sn0int",
  "sniffnet",
  "trippy",
  "wgcf",
]
when = ["desktop"]


[[group]]
description = "networking, protocols, etc [linux]"
package = ["iwd", "systemd-resolvconf"]
when = ["linux"]

[[group]]
description = "networking, protocols, etc [desktop][personal]"
package = ["mabel", "rclone", "syncthing"]
when = ["desktop", "personal"]

[[group]]
description = "networking, protocols, etc [desktop][work]"
package = ["amazon-workspaces", "openconnect", "openldap"]
when = ["desktop", "work"]

[[group]]
description = "networking, protocols, etc [personal]"
package = ["tailscale"]
when = ["personal"]

[[group]]
description = "services [desktop]"
package = ["ollama"]
when = ["desktop"]

[[group]]
description = "services [linux][personal]"
package = [
  "cockpit",
  "cockpit-machines",
  "cockpit-packagekit",
  "cockpit-storaged",
]
when = ["linux", "personal"]

[[group]]
description = "services [linux][personal][server]"
package = ["open-iscsi"]
when = ["linux", "personal", "server"]

[[group]]
description = "graphical desktop environment [desktop]"
package = [
  # fonts
  "inter-font",
  "otf-font-awesome",
  "otf-libertinus",
  "ttf-jetbrains-mono",
  "ttf-nerd-fonts-symbols",
  # for broad unicode coverage
  "noto-fonts",
  "noto-fonts-cjk",
  "noto-fonts-emoji",
  "noto-fonts-extra",
]
when = ["desktop"]

[[group]]
description = "graphical desktop environment [desktop][linux]"
package = [
  "resources",
  "wl-clipboard",
  "xdg-desktop-portal-gtk",

  # Pop!OS COSMIC, and friends
  "cosmic-files",
  "cosmic-player",
  "cosmic-session",
  "cosmic-store",
  "cosmic-terminal",
  "cosmic-text-editor",
  "cosmic-wallpapers",

  # GNOME and friends
  "gnome-keyring",
  "gnome-settings-daemon",
  "gvfs",                  # for cosmic-files
  "gvfs-smb",
]
when = ["desktop", "linux"]

[[group]]
description = "graphical desktop environment [desktop][mac_os]"
package = ["linearmouse", "stats"]
when = ["desktop", "mac_os"]

[[group]]
description = "dictionaries & input methods [desktop]"
package = [
  "dict-gcide",
  "dict-freedict-eng-spa",
  "dict-freedict-spa-eng",
  "dictd",
]
when = ["desktop"]
