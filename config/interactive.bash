#! /usr/bin/env bash

# enable concurrent shell history
# https://opensource.com/article/18/5/bash-tricks
shopt -s histappend

if command -v atuin >/dev/null; then
  eval "$(atuin init bash)"
fi
