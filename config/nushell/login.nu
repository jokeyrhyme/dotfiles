# by convention (?), login shells are once-per-login,
# however, some tools run login shells more frequently than this

# are there greeters that do not run shells as login shells?

# seems like this is a good place for tasks that might not run at all,
# and might also run more frequently than necessary