# cherry-pick the values we store ourselves, otherwise unwanted values like PWD creep in
const KEYS = [
    'CFLAGS',
    'CGO_CFLAGS',
    'CGO_CPPFLAGS',
    'CGO_CXXFLAGS',
    'CGO_LDFLAGS',
    'CMAKE_CXX_COMPILER_LAUNCHER',
    'CMAKE_C_COMPILER_LAUNCHER',
    'CPPFLAGS',
    'CXXFLAGS',
    'DFT_BACKGROUND',
    'DISPLAY',
    'GOFLAGS',
    'GOLDFLAGS',
    'LDFLAGS',
    'MAKEFLAGS',
    'MALLOC_CONF',
    'PATH',
    'RUSTC_WRAPPER',
    'RUSTFLAGS',
    'SSH_AUTH_SOCK',
    'SWAYSOCK',
    'WAYLAND_DISPLAY',
    'XDG_CURRENT_DESKTOP',
    'XDG_SESSION_DESKTOP',
    'XDG_SESSION_TYPE',
]

export def get-variables []: nothing -> record {
    if (which launchctl | is-not-empty) {
       $KEYS |
            each { |KEY| { key: $KEY, value: (^launchctl getenv $KEY | default null) } } |
            filter { |ROW| $ROW.value | is-not-empty } |
            k-v-table-to-record
    } else if (which systemctl | is-not-empty) {
        # read environment.d and environment-generator values into session
        # this requires systemd 250 or newer
        ^systemctl --user show-environment --output json |
            from json |
            select --ignore-errors ...$KEYS |
            without-empty-values
    } else {
        # TODO: per-session storage/retrieval without `launchctl` or `systemctl`
        {}
    }
}

export def k-v-table-to-record []: table -> record {
    $in |
        rotate |
        headers |
        reject 'key' |
        into record
}

export def without-empty-values []: record -> record {
    $in |
        transpose 'key' 'value' |
        filter { |ROW| $ROW.value | is-not-empty } |
        k-v-table-to-record
}
