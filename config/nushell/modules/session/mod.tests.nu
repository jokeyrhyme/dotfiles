use std assert
use .

const FILENAME = path self
print $FILENAME

let IN = [['key', 'value']; ['a', 123], ['b', 456], ['c', 789]]
let WANT = { a: 123, b: 456, c: 789 }
let GOT = $IN | session k-v-table-to-record
assert ($GOT == $WANT)
print 'OK'

let IN = { a: 123, b: null, c: 'def', g: '', h: 456 }
let WANT = { a: 123, c: 'def', h: 456 }
let GOT = $IN | session without-empty-values
assert ($GOT == $WANT)
print 'OK'

# effectively asserts that this does not throw
session get-variables | ignore
print 'OK'
