# see: https://github.com/nushell/nushell/blob/main/crates/nu-utils/src/sample_config/default_env.nu

let PPID = ps | where 'pid' == $nu.pid | first | get 'ppid'
# this is dumb, but apparently on macOS we don't always have a parent
let PARENTS = ps | where 'pid' == $PPID
if ($PARENTS | is-not-empty) {
    let PARENT = $PARENTS | first | get 'name'
    let GREETERS = ['greetd']
    let GREETER_WIPE_VARIABLES = ['DISPLAY', 'SWAY_SOCK', 'WAYLAND_DISPLAY']
    if $PARENT in $GREETERS {
        hide-env --ignore-errors ...$GREETER_WIPE_VARIABLES
         if (which systemctl | is-not-empty) {
            # coming from a greeter, we can assume we've just logged in,
            # which means we do not want to reuse any of these values
            ^systemctl --user unset-environment ...$GREETER_WIPE_VARIABLES
        }   
    }
}

use ./modules/session
session get-variables | load-env

if ('SSH_AUTH_SOCK' in $env) {
    if ($env.SSH_AUTH_SOCK | str starts-with '/private/tmp/com.apple.launchd.') {
        # ignore Apple's `ssh-agent`, because it does not support USB security keys
        hide-env SSH_AUTH_SOCK
    } else if (not ($env.SSH_AUTH_SOCK | path exists) or (ls --long $env.SSH_AUTH_SOCK | where readonly == false | is-empty)) {
        # current SSH socket isn't usable, ditch it
        hide-env SSH_AUTH_SOCK
    }
}

# start an SSH agent if we do not have one yet
if (
    (not ('SSH_AUTH_SOCK' in $env)) or
    (not ($env.SSH_AUTH_SOCK | path exists)) or
    ((ls -la $env.SSH_AUTH_SOCK | first).readonly)
) {
    # current SSH_AUTH_SOCK is unusable, must (re)create
    if ('/opt/homebrew/bin/ssh-agent' | path exists) {
        parse_variables (^/opt/homebrew/bin/ssh-agent -s | complete | get stdout)
    } else if (which ssh-agent | is-not-empty) {
        parse_variables (^ssh-agent -s | complete | get stdout)
    } else {
        {}
    }
} else {
    { SSH_AUTH_SOCK: $env.SSH_AUTH_SOCK }
} | load-env

$env.SHELL = $nu.current-exe
