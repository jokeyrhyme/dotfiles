# see: https://github.com/nushell/nushell/blob/main/crates/nu-utils/src/sample_config/default_config.nu

use ~/.dotfiles/lib/devices.nu [ has_precursor has_yubikey ]
use ~/.dotfiles/lib/posix.nu [ parse_variables ]

def __dotfiles_find_up [filename: string, dir] {
    let candidate = ($dir | path join $filename)
    let parent_dir = ($dir | path dirname)
    if ($candidate | path exists) {
        $candidate
    } else if ($parent_dir != '/') and ($parent_dir | path exists) {
        __dotfiles_find_up $filename $parent_dir
    } else {
        null
    }
}

$env.DOTFILES_CMD_START = null
$env.config = {
    completions: {
        algorithm: 'fuzzy',
    },
    history: {
        file_format: 'sqlite',
        sync_on_enter: false, # prefer concurrent sessions to not share history
    },
    hooks: {
        env_change: {
            PWD: [
                {
                    # https://github.com/direnv/direnv
                    condition: { || which 'direnv' | is-not-empty },
                    code: { || ^direnv export json | from json | default {} | load-env },
                },
                {
                    condition: { |before, after|
                        (which 'mise' | is-not-empty) and (
                            (__dotfiles_find_up '.node-version' $after | is-not-empty) or
                            (__dotfiles_find_up '.nvmrc' $after | is-not-empty) or
                            (__dotfiles_find_up '.tool-versions' $after | is-not-empty)
                        )
                    },
                    code: { || ^mise install --raw },
                },
                # TODO: handle Python virtual environments
            ],
        },
        pre_execution: [
            { $env.DOTFILES_CMD_START = (date now) }
        ],
    },
    keybindings: [],
    shell_integration: {
        # OSC 7
        # alacritty (no): https://github.com/alacritty/alacritty/blob/master/docs/escape_support.md
        # GNOME Console, Terminal (no?): ...
        # wezterm, wezterm+tmux (yes): https://wezfurlong.org/wezterm/shell-integration.html
        # zellij (no): https://github.com/zellij-org/zellij/issues/898
        osc7: ('WEZTERM_PANE' in $env and (not ('ZELLIJ' in $env))), 
        # OSC 133
        # alacritty (no): https://github.com/alacritty/alacritty/issues/5850
        # alacritty: (no): https://github.com/zellij-org/zellij/issues/899
        osc133: ('WEZTERM_PANE' in $env and (not ('ZELLIJ' in $env))), 
    },
    show_banner: false,
    table: {
        mode: 'none',
    },
    use_kitty_protocol: ('WEZTERM_PANE' in $env and (not ('ZELLIJ' in $env))),
}

if $nu.is-interactive {
    def __dotfiles_is_muxer_needed [] {
        (not ('ZELLIJ' in $env)) and (
            match ($env | get --ignore-errors 'TERM' | default 'linux') {
                "alacritty" => true, # alacritty doesn't have built-in tabs
                "linux" => true, # virtual console on linux
                "xterm-ghostty" => true, # we disable ghostty's built-in tabs
                _ => false,
            }
        )
    }

    # start a terminal muxer if we do not have one yet
    if (__dotfiles_is_muxer_needed) and (which 'zellij' | is-not-empty) {
        # TODO: re-attach to the first detached session found
        if (which systemd-run | is-not-empty) {
            # use systemd to allow detached zellij sessions to keep running after logout
            let ZELLIJ_UNIT_ID = 'zellij-' + (date now | format date "%s")
            exec systemd-run --same-dir --scope --unit $ZELLIJ_UNIT_ID --user zellij
        } else {
            exec zellij
        }
    }

    if 'ZELLIJ_SESSION_NAME' in $env {
        if ('ALACRITTY_SOCKET' in $env and 'ALACRITTY_WINDOW_ID' in $env) {
            let ALACRITTY_CONFIG = {
                keyboard: {
                    bindings: [
                        {
                            command: { args: ["--session", $env.ZELLIJ_SESSION_NAME, "action", "go-to-next-tab"], program: (which 'zellij' | first).path },
                            key: "Tab",
                            mods: "Control",
                        },
                        {
                            command: { args: ["--session", $env.ZELLIJ_SESSION_NAME, "action", "go-to-previous-tab"], program: (which 'zellij' | first).path },
                            key: "Tab",
                            mods: "Control | Shift",
                        },
                    ]
                }
            }
            ^alacritty msg config --window-id $env.ALACRITTY_WINDOW_ID ($ALACRITTY_CONFIG | to toml)
        }
        if ($env | get --ignore-errors 'TERM_PROGRAM' | default 'xterm') == 'ghostty' {
            # TODO: wire up Ghostty keybindings to zellij tabs
            # see: https://github.com/ghostty-org/ghostty/discussions/2872
        }
    }


    # start an SSH agent if we do not have one yet
    if ('SSH_AUTH_SOCK' in $env) {
        { SSH_AUTH_SOCK: $env.SSH_AUTH_SOCK }
    } else {
        if ('/opt/homebrew/bin/ssh-agent' | path exists) {
            parse_variables (^/opt/homebrew/bin/ssh-agent -s | complete | get stdout)
        } else if (which ssh-agent | is-not-empty) {
            parse_variables (^ssh-agent -s | complete | get stdout)
        } else {
            {}
        }
    } | load-env
    if ('SSH_AUTH_SOCK' in $env) {
        # propagate SSH_AUTH_SOCK to dbus/systemd environment
        if ('DISPLAY' in $env) and (which dbus-update-activation-environment | is-not-empty) {
            ^dbus-update-activation-environment SSH_AUTH_SOCK
        }
        if (which launchctl | is-not-empty) {
            ^launchctl setenv SSH_AUTH_SOCK $env.SSH_AUTH_SOCK
        }
        if (which systemctl | is-not-empty) {
            ^systemctl --user import-environment SSH_AUTH_SOCK
        }

        # prepare smartcard/yubikey for SSH use
        # see: https://aus.social/@jokeyrhyme/109129472870400757
        if (has_precursor) or (has_yubikey) {
            if (ssh-add -l | lines | find '(ED25519-SK)' | is-empty) {
                # no FIDO2 key registered with SSH agent yet
                if ('/opt/homebrew/bin/ssh-add' | path exists) {
                    try { /opt/homebrew/bin/ssh-add -K } catch {}
                } else {
                    try { ssh-add -K } catch {}
                }
            }
            if ('/usr/local/lib/libykcs11.dylib' | path exists) {
                if (ssh-add -l | lines | find 'PIV Authentication (ECDSA)' | is-empty) {
                    # no Yubico PKCS11 key registered with SSH agent yet
                    if ('/opt/homebrew/bin/ssh-add' | path exists) {
                        try { /opt/homebrew/bin/ssh-add -s /usr/local/lib/libykcs11.dylib } catch {}
                    } else {
                        try { ssh-add -s /usr/local/lib/libykcs11.dylib } catch {}
                    }
                }
            }
        }
    }

    # https://opensource.com/article/18/5/advanced-use-less-text-file-viewer
    $env.LESS = "-C -M -I -j 10 -x 2 -# 4 -R"
    if (which bat | is-not-empty) {
        {
            MANPAGER: "bat --language man --decorations=never",
            PAGER: 'bat --decorations=never',
        }
    } else {
        {
            MANPAGER: 'less',
            PAGER: 'less',
        }
    } | load-env

    $env.EDITOR = if (which hx | is-not-empty) {
        "hx"
    } else if (which nvim | is-not-empty ) {
        "nvim"
    } else {
        "vim"
    }

    # GPG_TTY is needed for GPG passphrase prompts
    $env.GPG_TTY = (^tty)

    def _argc_completer [args: list<string>] {
        ^argc --argc-compgen nushell "" ...$args
            | split row "\n"
            | each { |line| $line | split column "\t" value description }
            | flatten
    }
    let external_completer = {|spans|
        if ($spans | length) < 2 {
            '' # default completer
        } else {
            _argc_completer $spans
        }
    }
    $env.config.completions.external.enable = true
    $env.config.completions.external.completer = $external_completer

    let HOME_DIR_REGEX = $'^('~' | path expand | str replace --all '/' '\/')'
    $env.PROMPT_COMMAND = {
        mut CWD = pwd | str replace --regex $HOME_DIR_REGEX '~' 
        let CWD_PARTS = $CWD | path split
        let CWD_DIRS = $CWD_PARTS | take (($CWD_PARTS | length) - 1) 
        let CWD_TAIL = $CWD_PARTS | last
        $CWD = [
            ...($CWD_DIRS | each { |DIR| $DIR | str substring 0..0 }),
            $CWD_TAIL,
        ] | path join

        let INDICATOR_ANSI = if $env.LAST_EXIT_CODE == 0 { ansi green } else { ansi red }
        let INDICATOR = $'($INDICATOR_ANSI)> (ansi reset)'
        
        let PARTS = [
            (ansi reset),
            $CWD,
            $INDICATOR,
        ]
        $PARTS | str join ' '
    }
    $env.PROMPT_INDICATOR = ''

    $env.PROMPT_COMMAND_RIGHT = {
        let START = $env.DOTFILES_CMD_START
        if $START == null {
            return ''
        }

        let END = (date now)
        let DURATION = $END - $START
        if $DURATION <= 1sec {
            return ''
        }

        $DURATION | format duration sec
    }

    source '~/.cache/atuin/init.nu'
    source '~/.cache/mise/init.nu'
}
