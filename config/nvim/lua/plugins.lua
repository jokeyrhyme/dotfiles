-- see: https://github.com/folke/lazy.nvim
return {
  {
    "NvChad/nvim-colorizer.lua",
    config = function()
      require("colorizer").setup({})
    end,
  },
  {
    "echasnovski/mini.nvim",
    config = function()
      require("mini.basics").setup({
        autocommands = {
          relnum_in_visual_mode = true,
        },
        options = {
          extra_ui = true,
        },
      })
      require("mini.comment").setup({})
      require("mini.completion").setup({
        fallback_action = function()
          -- noop, to disable fallback, non-LSP results aren't useful
          -- https://github.com/echasnovski/mini.nvim/issues/5
        end,
      })
      require("mini.cursorword").setup({})
      require("mini.indentscope").setup({})
      require("mini.pairs").setup({})
      require("mini.statusline").setup({})
      require("mini.surround").setup({})
      require("mini.tabline").setup({})
    end,
    dependencies = { "kyazdani42/nvim-web-devicons" },
  },
  "f-person/git-blame.nvim",
  {
    "folke/which-key.nvim",
    config = function()
      -- https://github.com/folke/which-key.nvim
      require("which-key").setup({})
    end,
  },
  "gpanders/editorconfig.nvim",
  {
    "kyazdani42/nvim-tree.lua",
    config = function()
      -- https://github.com/kyazdani42/nvim-tree.lua
      require("nvim-tree").setup({
        filters = {
          dotfiles = true,
        },
        sync_root_with_cwd = true,
        update_focused_file = {
          enable = true,
        },
      })
      vim.keymap.set("", "<C-\\>", "<cmd>NvimTreeToggle<CR>")
    end,
    dependencies = { "kyazdani42/nvim-web-devicons" },
  },
  {
    "lewis6991/gitsigns.nvim",
    config = function()
      -- https://github.com/lewis6991/gitsigns.nvim
      require("gitsigns").setup()
    end,
    dependencies = { "nvim-lua/plenary.nvim" },
  },
  {
    "nvim-telescope/telescope.nvim",
    config = function()
      require("fuzzyfinder")
    end,
    dependencies = {
      "kyazdani42/nvim-web-devicons",
      "nvim-lua/popup.nvim",
      "nvim-lua/plenary.nvim",
      "nvim-telescope/telescope-ui-select.nvim",
    },
    version = "*",
  },
  {
    "nvim-treesitter/nvim-treesitter",
    build = function()
      pcall(require("nvim-treesitter.install").update({ with_sync = true }))
    end,
    config = function()
      require("filetypes")
    end,
  },
  {
    "rose-pine/neovim",
    config = function()
      vim.opt.background = "dark"
      require("rose-pine").setup({})
      vim.cmd.colorscheme("rose-pine")
    end,
    name = "rose-pine",
  },
  {
    "sindrets/diffview.nvim",
    dependencies = { "kyazdani42/nvim-web-devicons", "nvim-lua/plenary.nvim" },
  },
  {
    "williamboman/mason.nvim",
    config = function()
      require("mason").setup()
    end,
  },
  {
    "windwp/nvim-spectre",
    config = function()
      require("spectre").setup()
      -- nvim-spectre
      vim.keymap.set("", "<leader>S", require("spectre").open, { desc = "spectre: find and replace" })
    end,
    dependencies = { "kyazdani42/nvim-web-devicons" },
  },
}
