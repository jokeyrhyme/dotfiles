-- see: https://github.com/nvim-telescope/telescope.nvim
require("telescope").setup({
  defaults = {
    -- see `string.find` https://www.lua.org/manual/5.3/manual.html#6.4
    file_ignore_patterns = { "%.git/.+" },
    layout_strategy = "vertical",
  },
  extensions = {
    ["ui-select"] = {},
  },
  pickers = {
    find_files = {
      hidden = true,
    },
  },
})
require("telescope").load_extension("ui-select")

local builtin = require("telescope.builtin")

-- see: theprimeagen https://youtu.be/w7i4amO_zaE
vim.keymap.set("n", "<leader>pf", builtin.find_files, { desc = "find_files" })
vim.keymap.set("n", "<leader>ps", builtin.live_grep, { desc = "live_grep" })
vim.keymap.set("n", "<leader>pt", builtin.treesitter, { desc = "treesitter" })
vim.keymap.set("n", "<C-p>", builtin.git_files, { desc = "git_files" })

-- see: tjdevries https://youtu.be/puWgHa7k3SY
vim.keymap.set("n", "<leader>dn", vim.diagnostic.goto_next, { desc = "goto next diagnostic" })
vim.keymap.set("n", "<leader>dp", vim.diagnostic.goto_prev, { desc = "goto prev diagnostic" })
vim.keymap.set("n", "<leader>dl", builtin.diagnostics, { desc = "diagnostics" })
