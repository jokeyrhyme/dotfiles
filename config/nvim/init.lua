-- see: https://github.com/nanotee/nvim-lua-guide

-- see: https://github.com/folke/lazy.nvim
-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
  if vim.v.shell_error ~= 0 then
    vim.api.nvim_echo({
      { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
      { out, "WarningMsg" },
      { "\nPress any key to exit..." },
    }, true, {})
    vim.fn.getchar()
    os.exit(1)
  end
end
vim.opt.rtp:prepend(lazypath)

-- see: https://github.com/ThePrimeagen/init.lua
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")
vim.keymap.set("x", "<leader>p", [["_dP]])
vim.keymap.set("n", "<leader>f", vim.lsp.buf.format, { desc = "lsp: format" })

-- http://karolis.koncevicius.lt/posts/porn_zen_and_vimrc/
-- make Y consistent with C and D.
vim.keymap.set("", "Y", "y$")

-- see: theprimeagen https://youtu.be/w7i4amO_zaE
vim.keymap.set("n", "<leader>vd", vim.diagnostic.open_float, { desc = "diagnostics on this line" })

-- auto-reload files when modified externally
-- https://unix.stackexchange.com/a/383044
vim.o.autoread = true
vim.api.nvim_create_autocmd({ "BufEnter", "CursorHold", "CursorHoldI", "FocusGained" }, {
  command = "if mode() != 'c' | checktime | endif",
  pattern = { "*" },
})

-- disable additional junk files
vim.o.swapfile = false

-- show trailing whitespace
-- https://stackoverflow.com/a/1675752/488373
vim.o.list = true
vim.o.listchars = "tab:  ,extends:>,precedes:<,trail:·"

vim.g.mapleader = " "
vim.g.maplocalleader = "\\"

-- tab stuff
-- https://stackoverflow.com/a/1878983/488373
vim.o.smarttab = true
vim.o.tabstop = 4

vim.opt.guifont = "JetBrains Mono:h12"

-- number of extra lines/columns to keep between cursor and edge of screen
vim.o.scrolloff = 8
vim.o.sidescrolloff = 8

-- start lazy _after_ above settings
require("lazy").setup({
  spec = {
    { import = "plugins" },
  },
  checker = {
    enabled = true,
    frequency = 72 * 3600, -- every 72 hours
  },
  concurrency = 3,
  install = {
    colorscheme = { "rose-pine" },
  },
})
