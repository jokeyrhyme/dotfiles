# new Mozilla Firefox profile / installation

## [General](about:preferences#general)

### Language and Appearance

- Default font: Libertinus Serif 16

- Advanced...

  - Serif: Libertinus Serif

  - Sans-serif: Inter

  - Monospace: JetBrains Mono 14

  - Minimum font size: 12

### Browsing

- Always underline links: enable

## [Search](about:preferences#search)

### Search Bar

- Use the address bar for search and navigation: select

### Default Search Engine

- DuckDuckGo: select

### Search Suggestions

- Provide search suggestions: disable

### Search Shortcuts

- Amazon: remove

- Bing: remove

- eBay: remove

## [Browser Privacy](about:preferences#privacy)

### Enhanced Tracking Protection

- Strict: select

### Website Privacy Preferences

- Tell websites not to sell or share my data: enable

- Send websites a "Do Not Track" request: enable

### Logins and Passwords

- Ask to save logins and passwords for websites: disable

### HTTPS-Only Mode

- Enable HTTP-Only Mode in all windows: select

## [about:config](about:config)

- `browser.fixup.dns_first_for_single_words`: true

  - needed for my employer's internal URL-shortener

- `extensions.pocket.enabled`: false

  - unused feature

- `gfx.webrender.all`: true

- `media.ffmpeg.vaapi.enabled`: true

- `media.ffvpx.enabled`: false

- `privacy.webrtc.legacyGlobalIndicator`: false

  - we rely on OS/desktop/compositor indicators instead

  - see: https://bugzilla.mozilla.org/show_bug.cgi?id=1668358
