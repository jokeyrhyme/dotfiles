# [talos.dev](https://talos.dev)

## new cluster

- you will need to know the IP_ADDRESS values for our nodes, in-advance

- you will need to select an INITIAL_IP_ADDRESS (IP_ADDRESS for our initial node)

- you will need to have chosen a CLUSTER_DOMAIN for the cluster, e.g. "cluster.local"

- we assume that a [pi-hole](https://github.com/pi-hole/pi-hole) provides default DNS resolution on our network

- we assume that the "DNS records" approach is how we will address our cluster: https://www.talos.dev/v0.14/introduction/getting-started/#dns-records

1. create a DNS record in the pi-hole per [./pihole--local-a-record.md](./pihole--local-a-record.md) such that CLUSTER_DOMAIN resolves to one A record for INITIAL_IP_ADDRESS

   - **IMPORTANT**: nodes will use the cluster endpoint for discovery, resulting in split-brain or failed-discovery if a node enrolls itself with a node that is not the initial node

1. if nodes are Raspberry Pis, then prepare the initial primary/control-plane node using [this guide](https://www.talos.dev/v0.14/single-board-computers/rpi_4/)

   - otherwise, start with one of the other "... Platforms" or "Single Board Computers" guides as appropriate: https://www.talos.dev/v0.14/

   - **IMPORTANT**: start with a temporary cluster endpoint based on an IP-address, because the certificates won't yet include CLUSTER_DOMAIN

1. run `talosctl patch machineconfig --patch-file ./talos/cluster.machineconfig.patch.json --nodes INITIAL_IP_ADDRESS --on-reboot`

1. run `talosctl patch machineconfig --patch '[{"op":"add","path":"/cluster/apiServer/certSANs","value":["CLUSTER_DOMAIN","IP_ADDRESSes..."]}]' --nodes INITIAL_IP_ADDRESS --on-reboot`

   - without the IP_ADDRESS for all control plane nodes, [CoreDNS](https://coredns.io/) will have issues

1. run `talosctl patch machineconfig --patch '[{"op":"add","path":"/cluster/clusterName","value":"CLUSTER_DOMAIN"}]' --nodes INITIAL_IP_ADDRESS --on-reboot`

1. run `talosctl patch machineconfig --patch '[{"op":"add","path":"/cluster/controlPlane.endpoint","value":"https://CLUSTER_DOMAIN:6443"}]' --nodes INITIAL_IP_ADDRESS --on-reboot`

1. run `talosctl patch machineconfig --patch '[{"op":"add","path":"/machine/certSANs","value":["IP_ADDRESSes..."]}]' --nodes INITIAL_IP_ADDRESS --on-reboot`

1. run `talosctl reboot --nodes INITIAL_IP_ADDRESS`, to commit/save changes

1. allow time for it to start rebooting (watch with `gping` or `ping`, can take 30-60 seconds to start)

1. allow time for talos' etcd to become available again (watch with `talosctl etcd members --nodes IP_ADDRESS`)

1. install cilium CNI per: https://docs.cilium.io/en/stable/gettingstarted/k8s-install-helm/

1. use `talosctl get machineconfig --nodes IP_ADDRESS --output yaml | tee ~/machineconfig.yml` to retrieve settings to apply to other nodes

1. edit ~/machineconfig.yaml

   1. change `machine.type` to `controlplane`

      - TODO: determine which tokens can and should be per-machine instead of per-cluster

1. for each IP_ADDRESS of the remaining control-plane nodes (ideally 3x including the earlier/first node):

   1. run `talosctl apply-config --file ~/machineconfig.yaml --insecure --nodes IP_ADDRESS`

   1. allow time for it to start rebooting (watch with `gping` or `ping`, can take 30-60 seconds to start)

   1. allow time for talos' etcd to include IP_ADDRESS as a member (watch with `talosctl etcd members --nodes INITIAL_IP_ADDRESS`)

1. edit ~/.talos/config to add the IP_ADDRESS for all your control-plane nodes

1. run `talosctl kubeconfig`

1. run `kubectl get nodes`, you should see all 3x control-plane nodes

1. edit the DNS record in the pi-hole per [./pihole--local-a-record.md](./pihole--local-a-record.md) such that CLUSTER_DOMAIN resolves to A records for each of the IP_ADDRESS of all 3x control-plane nodes

1. run `kubectl get nodes` a few more times, you should see consistent membership (if you sometimes don't see all the nodes, then discovery failed)
