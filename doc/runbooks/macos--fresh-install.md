# macOS: fresh install

## Docker Desktop

- select VirtioFS for file sharing with containers:
  https://www.docker.com/blog/8-top-docker-tips-tricks-for-2024/

## System Settings

### Appearance

- Appearance -> Auto

### Apple ID (section with my name in the label)

- Contact Key Verification

  - Verification in iMessage -> enable

### Battery

- Options...

  - Prevent automatic sleeping on power adapter when the display is off -> enable

### Control Centre

- Control Centre Modules

  - Sound -> Always Show in Menu Bar

### Desktop & Dock

- Automatically hide and show the Dock -> enable

- Hot Corners...

  - top-left -> Mission Control

- Stage Manager -> disable

### Displays

- Night Shift

  - Schedule -> Sunset to Sunrise

### General

- Date & Time

  - 24-hour time -> enable

  - Show 24-hour time on Lock Screen -> enable

- Language & Region

  - Date format -> YYYY-MM-DD

### Internet Accounts

- iCloud (for each iCloud account)

  - Advanced Data Protection -> enable

### Mouse

- Natural scrolling -> disable

### Privacy & Security

- Advanced...

  - Require an administrator password to access system-wide settings -> enable

### Screen Time

- App & Website Activity -> enable

### Sound

- Play sound on startup -> disable

### Touch ID & Password

- Touch ID -> enrol fingerprints

- Use Touch ID to unlock your Mac -> disable

### Trackpad

- Tap to click -> enable

## `/etc/manpaths.d/homebrew`

```
/opt/homebrew/share/man
```

## `/etc/paths.d/homebrew`

```
/opt/homebrew/bin
/opt/homebrew/sbin
/opt/homebrew/opt/openjdk/bin
```
