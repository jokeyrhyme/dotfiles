# new samba share

## prerequisites

- SHARE: identifier/name of the new samba network share

- usernames that will need read-write versus read-only access

- root shell session on the samba host

## outcome

- read-write users will have access to the share both locally and from remote machines

- read-only users will have no direct local access, only from remote machines

## process

1. create read-write and read-only groups for the new share:

   - `groupadd smb-SHARE-rw`; and

   - `groupadd smb-SHARE-ro`

1. ensure desired Linux users exists, e.g:

   - create a samba-only user with no Linux access: `useradd -M -s /sbin/nologin USER`; or

   - create a normal Linux user: `useradd -M -s /usr/bin/sh USER`

1. add user to desired group, NOT to both groups, e.g:

   - `usermod --append --groups smb-SHARE-rw USER`; or

   - `usermod --append --groups smb-SHARE-ro USER`

1. ensure samba users exist for all Linux users where share access is desired:

   - `smbpasswd -a USER`

   - note: generate new passwords for this purpose, do not reuse

1. ensure samba share directory exists: `mkdir -p /srv/samba/SHARE`

1. ensure samba share directory has correct ownership: `chown -R root:smb-SHARE-rw /srv/samba/SHARE`

1. ensure samba share directory has correct permissions: `sudo ~/.dotfiles/bin/chdmod.sh 2770 /srv/samba/SHARE`

   - SGID `2` forces new files to inherit ACLs from their parent directory, not from the user

1. ensure /etc/smb.conf contains:

   ```
   [SHARE]
       path = /srv/samba/SHARE/
       force create mode = 0660
       force directory mode = 2770
       force user = root
       force group = smb-SHARE-rw
       guest ok = no
       inherit acls = yes
       read list = @smb-SHARE-ro
       write list = @smb-SHARE-rw
   ```

1. confirm that /etc/smb.conf is valid: `testparm -s`

1. restart samba service: `systemctl restart smbd.service`

## see also

- https://wiki.samba.org/index.php/Setting_up_Samba_as_a_Standalone_Server

- https://wiki.samba.org/index.php/Setting_up_a_Share_Using_POSIX_ACLs
