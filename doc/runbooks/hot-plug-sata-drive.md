# hot-plug SATA drive

## when?

- adding/removing SATA hard drive without system downtime

## process

- to scan: `echo "0 0 0" >/sys/class/scsi_host/host<n>/scan`

- to remove: `echo x > /sys/bus/scsi/devices/<n>:0:0:0/delete`

## see also

- https://fredericiana.com/2010/06/15/hot-plugging-a-sata-drive-under-linux/
