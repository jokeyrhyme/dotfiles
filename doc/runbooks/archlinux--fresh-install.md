# new archlinux

In general, follow the official guide: https://wiki.archlinux.org/index.php/Installation_guide

We document exceptions or shortcuts below

We'll use `/dev/sdexample`, `/dev/sdexample1`, etc for convenient examples,
substitute in the actual values for your system

TODO: try https://wiki.archlinux.org/title/Archinstall

- 2024-03-02: my outdated copy of `archinstall` did not support LUKS

## "Partition the disks”

1. in `parted`, use `unit s` and percentage values for partition start and end

   - this will result in optimally-aligned partitions

1. 1GiB ESP https://wiki.archlinux.org/index.php/EFI_system_partition

   1. `parted /dev/sdexample` -> `mkpart fat32 efi 0% 1GiB` , `set 1 esp on`

   1. `mkfs.fat -F 32 /dev/sdexample1`

1. LUKS partition for the root filesystem

   1. `parted /dev/sdexample` -> `mkpart ext2 cryptroot 1GiB 100%` ("ext2" is just temporary)

   1. `cryptsetup luksFormat /dev/sdexample2`

   1. https://uapi-group.org/specifications/specs/discoverable_partitions_specification/

      - set GPT partition type to `4f68bce3-e8cd-4db1-96e7-fbcaf984b709` (root partition for amd64)

   - https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate#Hibernation_into_swap_file_on_Btrfs

### LUKS option A: btrfs inside LUKS

1. https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system#Btrfs_subvolumes_with_swap

1. use discoverable GPT partition type, and set the default btrfs subvolume

   - now we do not need `rd.luks...=` or `root=` in our kernel command line

   - now we do not need "root" mapping in /etc/crypttab.initramfs (we set SSD TRIM on the LUKS volume itself)

   - we do still need entry for `/` in /etc/fstab, as there's no were else to set SSD TRIM for btrfs mount options

### LUKS option B: btrfs and swap inside LVM inside LUKS

1. `pvcreate /dev/sdexample2`

1. `vgcreate vg0 /dev/sdexample2`

1. `lvcreate -L 32G vg0 -n swap` (same size as system RAM)

1. `lvcreate -l 100%FREE vg0 -n root` (lower-case "L")

1. (optional) use discoverable GPT partition type, and set the default btrfs subvolume

   - the in-between LVM layer prevents this from working by itself

1. `mkdir -p /mnt/etc/cmdline.d && echo root=/dev/mapper/vg0-root >> /mnt/etc/cmdline.d/root.conf`

1. /etc/crypttab.initramfs

   1. `mkdir -p /mnt/etc`

   1. `cryptsetup luksDump /dev/sdexample2 >> /etc/crypttab.initramfs`

   1. then edit into the correct format, so UUIDs map to "root"

      - e.g. `root UUID=... none luks`

### btrfs subvolumes

- layout btrfs subvolumes https://wiki.archlinux.org/index.php/Snapper#Suggested_filesystem_layout

  - find subvolume ID for "@" and set it as default with `btrfs subvolume set-default`

- for "LUKS option A: btrfs inside LUKS"

  - @swap subvolume, swap file (same size as system RAM), and fstab entry: https://wiki.archlinux.org/title/Btrfs#Swap_file

## "Install essential packages"

1. `pacstrap -K /mnt base btrfs-progs linux linux-firmware lvm2 neovim`

## "Configure the system"

1. `pacman -S neovim networkmanager sudo systemd-resolvconf`

   - the LTS kernel is a good fallback in case of problems

1. `systemctl enable NetworkManager.service systemd-resolved.service`

1. /etc/fstab

   - add `compress=zstd` mount option to BTRFS filesystems

   - add `discard=async` mount option to BTRFS filesystems on TRIM-capable SSDs

   - add `discard` mount option to FAT/MSDOS/VFAT filesystems on TRIM-capable SSDs

1. https://wiki.archlinux.org/index.php/Systemd-boot and https://wiki.archlinux.org/title/Unified_kernel_image#mkinitpcio

   - /etc/mkinitcpio.conf.d/dotfiles.conf: https://gitlab.com/jokeyrhyme/dotfiles/-/blob/main/etc/mkinitcpio.conf.d/dotfiles.conf

   - /etc/mkinitcpio.d/linux.preset: https://gitlab.com/jokeyrhyme/dotfiles/-/blob/main/etc/mkinitcpio.d/linux.preset

## "Post-installation"

1. `ln --force --symbolic /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf`

1. fix up /.snapshots per https://wiki.archlinux.org/title/Snapper#Configuration_of_snapper_and_mount_point

   - disable all snapper timers in systemd first, and delete all existing snapshots

1. `systemctl enable --now systemd-homed.service`

1. create non-root account with `homectl create`

1. `EDITOR=/usr/bin/nvim visudo`, uncomment line to allow `sudo` for users in the "wheel" group

At this point,

we should have a working (yet barebones) system, hooray!
