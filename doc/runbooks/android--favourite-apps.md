# android

## favourite apps

we cannot rely on Google Play's restoration behaviour to be consistent,
so we have this list:

- FUTO Keyboard: [Play Store](https://play.google.com/store/apps/details?id=org.futo.inputmethod.latin.playstore), [F-Droid](https://app.futo.org/fdroid/repo/), [website](https://keyboard.futo.org/)

- Omnivore: [Play Store](https://play.google.com/store/apps/details?id=app.omnivore.omnivore), [GitHub](https://github.com/omnivore-app/omnivore)

- Organics Maps: [Play Store](https://play.google.com/store/apps/details?id=app.organicmaps), [F-Droid](https://f-droid.org/en/packages/app.organicmaps/), [GitHub](https://github.com/organicmaps/organicmaps)

- ProtonMail: [Play Store](https://play.google.com/store/apps/details?id=ch.protonmail.android), [GitHub](https://github.com/ProtonMail/proton-mail-android)

### games

- Unciv: [Play Store](https://play.google.com/store/apps/details?id=com.unciv.app), [F-Droid](https://f-droid.org/en/packages/com.unciv.app/), [GitHub](https://github.com/yairm210/Unciv)

### media

- Myne: [Play Store](https://play.google.com/store/apps/details?id=com.starry.myne), [F-Droid](https://f-droid.org/packages/com.starry.myne/), [GitHub](https://github.com/Pool-Of-Tears/Myne)

- PDF Viewer: [Play Store](https://play.google.com/store/apps/details?id=app.grapheneos.pdfviewer.play), [GitHub](https://github.com/GrapheneOS/PdfViewer)

- Pocket Casts: [Play Store](https://play.google.com/store/apps/details?id=au.com.shiftyjelly.pocketcasts), [GitHub](https://github.com/Automattic/pocket-casts-android)

### security

- AirGuard: [Play Store](https://play.google.com/store/apps/details?id=de.seemoo.at_tracking_detection.release), [F-Droid](https://f-droid.org/packages/de.seemoo.at_tracking_detection), [GitHub](https://github.com/seemoo-lab/AirGuard)

- Bitwarden: [Play Store](https://play.google.com/store/apps/details?id=com.x8bit.bitwarden), [F-Droid](https://mobileapp.bitwarden.com/fdroid/), [GitHub](https://github.com/bitwarden/mobile)

- Bitwarden Authenticator: [Play Store](https://play.google.com/store/apps/details?id=com.bitwarden.authenticator), [GitHub](https://github.com/bitwarden/authenticator-android)

- Proton Pass: [Play Store](https://play.google.com/store/apps/details?id=proton.android.pass), [GitHub](https://github.com/protonpass/android-pass)

- TrackerControl: [Play Store](https://play.google.com/store/apps/details?id=net.kollnig.missioncontrol.play), [F-Droid](https://f-droid.org/app/net.kollnig.missioncontrol.fdroid), [GitHub](https://github.com/TrackerControl/tracker-control-android)

### social

- Element: [Play Store](https://play.google.com/store/apps/details?id=im.vector.app), [F-Droid](https://f-droid.org/app/im.vector.app), [GitHub](https://github.com/vector-im/element-android)

- Jerboa: [Play Store](https://play.google.com/store/apps/details?id=com.jerboa), [F-Droid](https://f-droid.org/packages/com.jerboa), [GitHub](https://github.com/dessalines/jerboa)

- Mastodon: [Play Store](https://play.google.com/store/apps/details?id=org.joinmastodon.android), [GitHub](https://github.com/mastodon/mastodon-android)

- Signal: [Play Store](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms), [GitHub](https://github.com/signalapp/Signal-Android)
