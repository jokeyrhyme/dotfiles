# new [Kobol Helios64](https://wiki.kobol.io/helios64/intro/)

1. download the latest "bullseye" from the helios64 section in the [Armbian archive](https://www.armbian.com/download/)

1. write the image on an SD card, e.g. using [balena etcher](https://www.balena.io/etcher/)

1. boot the device with connected ethernet

1. using GNOME Terminal or other long-known terminal, SSH in (user/pass: root / 1234)

1. set a new root password, and create a non-root user account

1. disconnect and re-connect as new non-root user

1. set hostname

1. review MAC addresses (`ip link`) and reserve IP addresses on network

1. set /etc/hosts for known IP reservations

1. copy authorized_keys to ~/.ssh

1. in `sudo armbian-config`: freeze kernel/firmware updates (there's a real bad bug)

1. in `sudo armbian-config`: disable root for SSH

1. `git clone https://gitlab.com/jokeyrhyme/dotfiles.git ~/.dotfiles`

1. `~/.dotfiles/scripts/update.sh`

1. `sudo reboot`

1. install device in desired location
