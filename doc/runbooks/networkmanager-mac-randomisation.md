# networkmanager

## MAC randomisation

1. `nmcli connection` to list connections

1. find the WiFi connection you wish to modify, by name or UUID

1. run one of the following to set your preference:

`nmcli connection modify NAME_OR_UUID 802-11-wireless.cloned-mac-address random`

or

`nmcli connection modify NAME_OR_UUID 802-11-wireless.cloned-mac-address permanent`
