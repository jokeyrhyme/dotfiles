# yubikey

## new security key

### protect with PIN

1. `ykman fido access change-pin`

1. `ykman fido access change-pin --u2f` (if supported)

1. `ykman piv access change-management-key --algorithm AES256 --generate --touch`
   (or with `--algorithm TDES` if AES is not supported),
   store output in password manager

1. `ykman piv access change-pin` (default is 123456)

1. `ykman piv access change-puk` (default is 12345678)

### SSH

#### SSH ed25519-sk (FIDO2)

1. `ssh-keygen -O resident -t ed25519-sk`,
   no passphrase (we'll rely on the key's PIN protection),
   name file with "\_$SERIAL" on the end (replace "$SERIAL" with the real serial key)

to retrieve the SSH public keys later (or on a new computer):

1. `ssh-add -K` or `ssh-add -L`

#### SSH ecdsa (PIV)

TODO: stop doing this when Bitbucket and other services support ed25519-sk,
see https://jira.atlassian.com/browse/BCLOUD-20297

NOTE: the `-` option for stdout breaks certificate generation,
so specify an output file name even if you do not need it

1. `ykman piv keys generate --algorithm ECCP384 --pin-policy ONCE --touch-policy ALWAYS 9a id.pub`

1. `ykman piv certificates generate --hash-algorithm SHA512 --subject 'CN=SSH-key' 9a id.pub`
   (this might fail on older keys: go back and generate a new key without `--algorithm ECCP384`, then retry)

1. `rm id.pub`

to retrieve the SSH public keys later (or on a new computer):

1. `ssh-keygen -D /usr/lib/opensc-pkcs11.so -e | tee ~/.ssh/id_$TYPE_$SERIAL.pub`

### online services

register the ed25519-sk SSH public keys with:

- https://github.com (both as Authentication and Signing keys)

- https://gitlab.com (without an expiry date)

- https://sr.ht

register the ecdsa (PIV) SSH public keys with:

- https://bitbucket.org

- other services that do not support ...-sk keys

## reset SSH

1. `ykman fido credentials list`,
   continue if there's an "ssh: ... openssh" entry

1. `ykman fido credentials delete openssh`

1. `ykman fido credentials list` again to review

1. `ykman piv reset`

## see also

- https://github.com/drduh/YubiKey-Guide

- https://github.com/vorburger/vorburger.ch-Notes/blob/develop/security/ed25519-sk.md

- https://gist.github.com/Kranzes/be4fffba5da3799ee93134dc68a4c67b

- https://man.archlinux.org/man/core/openssh/ssh-keygen.1.en

- https://man.archlinux.org/man/core/openssh/ssh-add.1.en

- https://docs.yubico.com/software/yubikey/tools/ykman/PIV_Commands.html

- https://github.com/fredxinfan/ykman-piv-ssh

- https://www.fosskers.ca/en/blog/yubikey-ssh
