# [pi-hole](https://pi-hole.net/): local A record

## prerequisites

- able to SSH into your pi-hole machine

- determine the FQDN that you want to define

- determine the IP address(es) that you want to associate with the FQDN

## process

1. SSH into the pi-hole machine

1. create/edit new file: /etc/dnsmasq.d/99-FQDN.conf

1. add as many lines as necessary (1 for each different IP address) e.g. `host-record=FQDN,IP`

1. write the file contents and return to the SSH shell

1. run `pihole-FTL --test` and confirm that the output includes `dnsmasq: syntax check OK.`

1. run `sudo systemctl restart pihole-FTL.service`

1. run `systemctl status pihole-FTL.service` and confirm that the service loaded without errors

1. close the SSH session

1. run `dog FQDN` and confirm that the output includes the expected A record(s)

## see also

- original source for this runbook: https://discourse.pi-hole.net/t/multiple-local-a-records-for-a-single-hostname/48673/7

- https://man.archlinux.org/man/extra/dnsmasq/dnsmasq.8.en
