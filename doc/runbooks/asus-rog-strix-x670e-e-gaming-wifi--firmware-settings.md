# ASUS ROG Strix X670E-E Gaming Wi-Fi

## firmware settings

(switch to Advanced Mode first to see all settings)

### Advanced

#### AMD fTPM Configuration

- Firmware TPM Switch -> enabled

#### AMD PBS

- Thunderbolt/USB4 Support -> enable

#### APM Configuration

- ErP Ready -> Enabled S4+S5

#### NB Configuration

- Integrated Graphics -> disabled

#### Onboard Devices Configuration

- Intel LAN Controller -> disabled
- USB Audio Controller -> disabled
- Wi-Fi Controller -> disabled

#### PCI Subsystem Settings

- SR-IOV Support -> enabled

#### USB Configuration

- Legacy USB Support -> disabled
- XHCI Hand-off -> disabled

### Ai Tweaker

- Ai Overclock Tuner -> EXPO2

### Boot

#### Boot Configuration

- Boot Logo Display -> disabled
- Fast Boot -> disabled
- Setup Mode -> Advanced Mode

#### CSM (Compatibility Support Module)

- Launch CSM -> disabled

#### Secure Boot

- Key Management -> ... perform enrollment ...
- OS Type -> Windows UEFI mode
- Secure Boot Mode -> Custom

### Main

#### Security

- Administrator Password -> ... set ...

### Monitor

#### Fan Speed Monitor

- CPU Fan Speed -> ignored (we're using watercooling instead)

## see also

- https://rog.asus.com/us/motherboards/rog-strix/rog-strix-x670e-e-gaming-wifi-model/
