# new/fresh Raspberry Pi

1. install and launch the Raspberry Pi Imager

   - https://www.raspberrypi.com/software/

   - https://flathub.org/apps/org.raspberrypi.rpi-imager

1. find and select [Raspberry Pi OS Lite](https://www.raspberrypi.org/software/operating-systems/)

   - use 64-bit for newer Raspberry Pis, 32-bit for older

1. OS customisation settings:

   - general -> set username and password

     - `pi` and a randomly-generated string

   - general -> configure wireless LAN

     - set wireless LAN country as appropriate

   - general -> set locale settings

     - set time zone as appropriate

   - services -> enable SSH

     - allow public-key authentication only, and copy in output from `ssh-add -L`

1. mount the ROOTFS partition from the SD card:

   1. `sudoedit $ROOTFS/etc/modules` and add i2c-bcm2708 and i2c-dev (needed for the SenseHAT)

   1. generate a new SSH public/private key-pair with `ssh-keygen -f $ROOTFS/home/pi/.ssh/id_ed25519 -N '' -t ed25519`,
      version the public key file in $REPO/public-keys/

1. boot the device without any keyboard or display, completely headless

   - it is expected that the LEDs on an attached SenseHAT will turn on for 10-20 seconds then turn off

   - allow the device a few minutes just in case there are any first-time processes that have to complete

1. check your DHCP server for the IP address leased to this new device

1. SSH to the new device, using the credentials set back in Raspberry Pi Imager

1. `sudo apt-get update && sudo apt-get --assume-yes dist-upgrade`

1. `sudo apt-get --assume-yes install git`

1. `git clone https://gitlab.com/jokeyrhyme/dotfiles.git ~/.dotfiles`

1. `~/.dotfiles/scripts/update.sh`

1. `systemctl reboot`, wait a few minutes, and reconnect via SSH

1. `~/.dotfiles/scripts/update.sh` again,
   (the script reacts to certain conditions, and those conditions will have changed after the first reboot)

1. `systemctl poweroff`

1. install device in desired location
