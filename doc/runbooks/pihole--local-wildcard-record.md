# [pi-hole](https://pi-hole.net/): local wildcard record

## prerequisites

- able to SSH into your pi-hole machine

- determine the parent FQDN of the wildcard record that you want to define:
  e.g. for `*.foo.local` this is `foo.local`

- determine the IP address that you want to associate with the FQDN and its subdomains

## process

1. SSH into the pi-hole machine

1. create/edit new file: /etc/dnsmasq.d/99-FQDN.conf

1. add this line e.g. `address=/foo.local/192.168.0.10`

1. write the file contents and return to the SSH shell

1. run `pihole-FTL --test` and confirm that the output includes `dnsmasq: syntax check OK.`

1. run `sudo systemctl restart pihole-FTL.service`

1. run `systemctl status pihole-FTL.service` and confirm that the service loaded without errors

1. close the SSH session

1. run `dog example.FQDN` and confirm that the output includes the expected A record

## see also

- https://man.archlinux.org/man/extra/dnsmasq/dnsmasq.8.en
