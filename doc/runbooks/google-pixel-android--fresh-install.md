# new Google Pixel Android

- 2021-11-13: Android 12

## Settings

### Network and internet

- Adaptive connectivity: enable

#### Internet

##### home Wi-Fi network

- Network usage: Treat as unmetered

- Privacy: Use device MAC

##### Network preferences

- Notify for public networks: disable

#### SIMs

- Allow 2G: disable ( see: https://www.eff.org/deeplinks/2022/01/victory-google-releases-disable-2g-feature-new-android-smartphones )

### Connected devices

#### Connection preferences

##### Bluetooth

- Device name: <set>

##### NFC

- Use NFC: enable

- Require device unlock for NFC: enable

##### Nearby Share

- Use Nearby Share: enable

- Device name: <set>

- Device visibility: Contacts

### Apps

#### Assistant

- Continued conversation: enable

- Hold power for your Assistant: enable

### Notifications

- Notification history: enable

- Bubbles: enable

- Sensitive notifications: disable

- Sensitive work profile notifications: disable

- Wireless emergency alerts: enable

- Hide silent notifications in status bar: enable

- Allow notification snoozing: enable

- Notification dot on app icon: enable

- Enhanced notifications: enable

#### Do Not Disturb

##### People

- Calls: Starred contacts and repeat callers

##### Apps

- Add apps: <on-call app>

##### Alarms and other interruptions

- Alarms: enable

- Media sounds: enable

##### Schedules

- Flip to shhh: enable

- Bedtime mode: enable

- Display options for hidden notifications: No sound from notifications

### Battery

#### Battery Saver

- Set a schedule: Based on percentage: 5%

- Turn off when charged: enable

##### Extreme Battery Saver

- When to use: Always use

##### Adaptive preferences

- Adaptive Battery: enable

- Adaptive Charging: enable

### Sound and vibration

- Adaptive Sound: On

- Vibration and haptics: On

- Dial pad tones: disable

- Screen locking sound: disable

- Charging sounds and vibration: disable

- Touch sounds: disable

#### Live Caption

- Live Caption in volume control: enable

#### Now playing

- Identify songs playing nearby: enable

- Notifications: on

- Show search button on the lock screen: enable

#### Media

- Pin media player: enable

- Show media on lock screen: disable

  - otherwise, it's too easy to skip podcast episodes accidentally

- Show media recommendations: disable

  - behavioural advertising is repugnant

### Display

- Adaptive brightness: enable

- Colours: Adaptive

- Smooth display: enable

#### Lock screen

- Privacy: Show sensitive content only when unlocked

- Show wallet: disable

- Show device controls: disable

- Always show time and info: disable

- Wake screen for notifications: enable

##### Now playing

- Identify songs playing nearby: enable

- Show search button on the lock screen: enable

#### Screen timeout

- Screen timeout: 30 seconds

- Screen attention: enable

#### Dark theme

- Schedule: Turns on from sunset to sunrise

#### Night Light

- Schedule: Turns on from sunset to sunrise

#### Auto-rotate screen

- Use auto-rotate: enable

- Enable face detection: enable

### Security

- Screen lock: Pattern

#### Screen lock

- Make pattern visible: disable

- Power button instantly locks: enable

#### Advanced settings

##### App pinning

- Use app pinning: enable

- Ask for unlock pattern before unpinning: enable

### Privacy

- Show passwords: disable

- Notifications on lock screen: Show sensitive content only when unlocked

- Show clipboard access: enable

- When work profile is locked: Hide sensitive work content

#### Google Location history

- Auto-delete: Auto-delete activity older than 3 months

#### Activity controls

- Auto-delete: Auto-delete activity older than 3 months

### Safety & emergency

- Car crash detection: enable

#### Medical information

- Organ donor: yes

### Digital Wellbeing and parental controls

Heads Up: enable, use Physical activity

#### Bedtime mode

- Turn on while charging at bedtime: 21:00 to 04:00

### System

#### Gestures

- Quick Tap: Off

  - otherwise, it's too easy to toggle accidentally

- Quickly open camera: enable

- Flip camera for selfie: enable

- Tap to check phone: enable

- Lift to check phone: enable

- Flip to Shhh: enable

- One-handed mode: disable

  - otherwise, it's too easy to toggle accidentally

- Press and hold power button: Hold for assistant

##### System navigation

- Gesture navigation: select

###### Gesture settings

- Swipe to invoke the assistant: disable

#### Date and time

- Use locale default: disable

- Use 24-hour format: enable

## Settings (on-call)

### Notifications

#### Do Not Disturb

##### Schedules

- Bedtime mode: disable
