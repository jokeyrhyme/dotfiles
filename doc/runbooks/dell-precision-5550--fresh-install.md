# new Dell Precision 5550

## status

- tested with BIOS version 1.9.1

## process

### firmware settings

1. set firmware settings password

1. enable Secure Boot

1. set Secure Boot mode to Audit Mode

1. enable Secure Boot Custom Mode

1. reboot

1. enroll personal keys in Secure Boot

1. set Secure Boot mode to enforce

1. enable Thunderbolt

1. enable Thunderbolt Boot

1. enable SMART Reporting

1. reboot
