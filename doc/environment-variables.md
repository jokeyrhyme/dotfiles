# Environment Variables

## Preferred Methods

### [systemd user-environment-generators](../etc/systemd/user-environment-generators/) (Linux)

- see: `man 7 systemd.environment-generator`

- sequence: runs when the systemd service manager (e.g. `--system` or `--user`) launches,
  or after `systemctl daemon-reload`

- impact: any systemd unit started by that service manager (including transient units started with `systemd-run`) after the generator runs

- benefits:

  - set in one place, no need to duplicate definitions

  - runs first (effectively), broad impact on other processes

  - can edit for new processes without having to log out or reboot (`systemctl daemon-reload`)

    - generators are executable and dynamic, e.g. can use a sha-bang and contain conditions/loops

- caveats:

  - SSH sessions do not get a user service manager instance,
    so do not use these for variables that we want within SSH sessions

  - processes launched by greeter sessions do not start as systemd units by default,
    so consider using a script that starts such processes with `systemd-run`

  - variable values with spaces in them are tricky to parse out of `systemctl show-environment`,
    i.e. in POSIX shell scripts

  - generators are executable and dynamic, e.g. can consume arbitary resources and impact system boot/login time

### [systemd environment.d](../config/environment.d/) (Linux)

- see: `man 5 environment.d`

- almost all the same characteristics as "systemd user-environment-generators" (above)

- benefits:

  - files are static, so no loops or performance glitches

- caveats:

  - files are static, cannot achieve anything more complex than variable interpolation

### [bash](../config/bashrc), [nushell](../config/nushell/), [zsh](../config/zshrc) configuration

- sequence: run when launching a new shell session

- impact: any process (directly or transitively) started from that shell session

- benefits:

  - not limited to systemd units and not limited to Linux

- caveats:

  - have to mirror/rewrite the same work for each shell, can sometimes be non-trivial

## see also

- https://wiki.archlinux.org/title/Environment_variables
