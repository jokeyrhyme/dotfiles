# 4. avoid use of `go install` outside of Go projects

Date: 2024-03-08

## Status

Accepted

## Context

`go install` can be used to install an executable in a per-user directory,
e.g. $HOME/go/bin

However,
even as of Go 1.20,
there is no `go` tooling to list the executables that were installed via `go install`,
to show their versions,
to update them all in bulk,
or to uninstall them

## Decision

We'll only use `go install`
(or the "installergo" job type in `tuning`)
as a last resort,
preferring other installation methods first

## Consequences

We won't be dog-fooding the "installergo" job type in `tuning`,
and we'll be more sensitive to the availability of per-OS / per-platform packaging
