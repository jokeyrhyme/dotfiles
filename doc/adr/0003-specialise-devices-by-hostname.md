# 3. specialise devices by hostname

Date: 2021-09-12

## Status

Accepted

amends [2. couple distribution to device role](0002-couple-distribution-to-device-role.md)

## Context

- within a home network,
  some devices are the only device that performs a particular role
- within a home network,
  some devices are single-purpose
- see: https://gitlab.com/jokeyrhyme/dotfiles/-/issues/53

## Decision

- scripts will detect the hostname of a device
- scripts may execute purpose-specific behaviour based upon the hostname

## Consequences

- these dotfiles are public,
  so these scripts will leak home network hostnames and purposes
