# 2. couple distribution to device role

Date: 2021-09-12

## Status

Accepted

amended by [3. specialise devices by hostname](0003-specialise-devices-by-hostname.md)

## Context

- we have scripts that setup devices into a desired state/role
- some devices are headless,
  some are directly interactive,
  some run services,
  etc
- we need to decide how the scripts recognise the desired state for a device
- see: https://gitlab.com/jokeyrhyme/dotfiles/-/issues/53

## Decision

- scripts will detect the operating system or distribution using `lsb_release`, etc
- scripts will assume that certain OS/distributions imply certain roles

## Consequences

- for now,
  we don't need to pursue other options such as storing settings in a file,
  or using environment variables
- may be awkward later when trying to use similar devices for different purposes
