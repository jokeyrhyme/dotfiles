#! /usr/bin/env nu

let SIGUSR1 = if (sys host).name == 'Darwin' {
    # see: https://developer.apple.com/library/archive/documentation/System/Conceptual/ManPages_iPhoneOS/man3/signal.3.html
    30
} else {
    # see: https://man.archlinux.org/man/signal.7.en
    10
}

def read_portal [] {
    if (which 'busctl' | is-empty) {
        return
    }
    try {
        ^busctl --user call org.freedesktop.portal.Desktop /org/freedesktop/portal/desktop org.freedesktop.portal.Settings ReadOne 'ss' org.freedesktop.appearance color-scheme
        | parse "v {type} {value}"
        # TODO: extract this out into a general D-Bus helper function
        # see: https://dbus.freedesktop.org/doc/dbus-specification.html#id-1.3.8
        # u = uint32
        | update 'value' { |row| if $row.type == 'u' { $row.value | into int } else { $row.value } }
        | first
        | get 'value'
        | match $in {
            # see: https://flatpak.github.io/xdg-desktop-portal/docs/doc-org.freedesktop.portal.Settings.html
            1 => 'dark'
            2 => 'light'
        }
    }
}

def main [
    appearance?: string # "dark" (default) or "light"
] {
    let selected = if ($appearance | is-not-empty) and ($appearance in ['dark', 'light']) {
        $appearance
    } else {
        let portal_setting = read_portal
        if ($portal_setting | is-not-empty) {
            $portal_setting
        } else {
            'dark'
        }
    }

    # TODO: configure neovim

    # set difftastic background
    if (which 'launchctl' | is-not-empty) {
        ^launchctl setenv DFT_BACKGROUND $selected
    } else if (which 'systemctl' | is-not-empty) {
        ^systemctl --user set-environment $'DFT_BACKGROUND=($selected)'
    }

    # set flavours scheme
    let flavours_theme = match $selected {
        "dark" => "rose-pine",
        "light" => "rose-pine-dawn",
    }
    try {
        ^flavours --config ~/.config/flavours/config.toml --directory ~/.cache/flavours --verbose apply $flavours_theme
    } catch { |err|
        print $err
    }

    # set helix scheme
    let helix_theme = match $selected {
        "dark" => "rose_pine",
        "light" => "rose_pine_dawn",
    }
    ^ln -sf $'($env.HOME)/.config/helix/runtime/themes/($helix_theme).toml' ~/.config/helix/runtime/themes/dotfiles.toml

    ps
    | where name == 'helix' or name == 'hx'
    | each { |PROCESS|
        kill --signal $SIGUSR1 $PROCESS.pid | ignore
    } | ignore
}
