#! /usr/bin/env sh

# form factors:
# - unknown
# - desktop: directly attached to monitor+keyboard(+mouse),
#   user session is typically in a graphical/wayland environment
# - server: no input/output peripherals,
#   user session is typically text-only and via remote/SSH

set -eu

FORMFACTOR=unknown

# see: https://www.dmtf.org/standards/smbios
CHASSIS_TYPE="2" # unknown
if [ -f /sys/class/dmi/id/chassis_type ]; then
  CHASSIS_TYPE=$(cat /sys/class/dmi/id/chassis_type)
fi
case "${CHASSIS_TYPE}" in
"3") FORMFACTOR=desktop ;;
"4") FORMFACTOR=desktop ;;  # low profile desktop
"6") FORMFACTOR=desktop ;;  # mini tower
"7") FORMFACTOR=desktop ;;  # tower
"8") FORMFACTOR=desktop ;;  # portable TODO: laptop
"9") FORMFACTOR=desktop ;;  # laptop   TODO: laptop
"10") FORMFACTOR=desktop ;; # notebook TODO: laptop
"11") FORMFACTOR=server ;;  # main server chassis
*) FORMFACTOR=unknown ;;
esac

if [ "${FORMFACTOR}" = "unknown" ] && command -v system_profiler >/dev/null 2>&1; then
  SP=$(system_profiler -detailLevel mini -json SPHardwareDataType || true)
  if echo "$SP" | grep "MacBook" - >/dev/null 2>&1; then
    FORMFACTOR=desktop # TODO: laptop
  fi
fi

HOSTNAME=$("${HOME}/.dotfiles/bin/detect-hostname.sh")

case "${HOSTNAME}" in
pihole) FORMFACTOR=server ;;
printer-bridge-pi) FORMFACTOR=server ;;
raspberrypi) FORMFACTOR=server ;;
rpi-*) FORMFACTOR=server ;;
*) ;;
esac

echo "${FORMFACTOR}"
