#! /usr/bin/env sh

set -eu

# usage: finds USB batteries and their charge_level (0-255)
# output: each stdout line is its own JSON object

for USB in /sys/bus/usb/devices/usb*; do
  for FILE in $(fd --exclude=device --exclude=driver --exclude=firmware_node --exclude=peer --exclude=port --exclude=subsystem --max-depth=10 --prune charge_level "${USB}"*); do
    ID="$(basename ${USB})"
    SYS_PATH="$(dirname ${FILE})"
    LABEL=""
    if [ -r "${SYS_PATH}"/device_type ]; then
      LABEL=$(cat ${SYS_PATH}/device_type)
    fi
    echo "{\"id\":\"${ID}\",\"chargeLevel\":$(cat ${FILE}),\"sysPath\":\"${SYS_PATH}\",\"label\":\"${LABEL}\"}"
  done
done
