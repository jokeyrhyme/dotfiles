#! /usr/bin/env nu

let RECIPIENT_PUBS = (ls ~/.dotfiles/public-keys/*.pub)
if ($RECIPIENT_PUBS | is-empty) {
  error make { msg: 'recipient public SSH keys not found' }
} else {
  $RECIPIENT_PUBS | each { |PUB| print $'found: ($PUB.name)' } | ignore
}

[
  ('~/.dotfiles/config/k3s/token.secret.txt' | path expand),
  ('~/.dotfiles/k8s/gitlab-runner.secret.yaml' | path expand),
  ('~/.dotfiles/k8s/pi-hole.secret.yaml' | path expand),
] | each { |DECRYPTED|
  if not ($DECRYPTED | path exists) {
    error make { msg: $'($DECRYPTED) not found' }
  }
  let ENCRYPTED = $'($DECRYPTED).age'

  let RECIPIENT_ARGS = $RECIPIENT_PUBS | each { |PUB| [ '--recipients-file', $PUB.name ] } | flatten

  rm --force $ENCRYPTED
  ^age --armor --encrypt --output $ENCRYPTED ...$RECIPIENT_ARGS $DECRYPTED
  print $'age: ($DECRYPTED) -> ($ENCRYPTED)'
} | ignore

