#! /usr/bin/env sh

set -eu

for DECRYPTED in "${HOME}/.dotfiles/config/k3s/token.secret.txt" \
  "${HOME}/.dotfiles/k8s/gitlab-runner.secret.yaml" \
  "${HOME}/.dotfiles/k8s/pi-hole.secret.yaml"; do
  if [ -f "${DECRYPTED}" ]; then
    shred --force --remove --zero "${DECRYPTED}"
  fi
done
