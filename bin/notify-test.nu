#! /usr/bin/env nu

# see: https://man.archlinux.org/man/extra/libnotify/notify-send.1.en
# see: https://specifications.freedesktop.org/notification-spec/notification-spec-latest.html

# TODO: figure out how to specify icons that work

notify-send --urgency=low --app-name=networkmanager --category network.connected --icon=network-wireless-connected-symbolic 'My Wi-Fi Network' 'Connected'

# TODO: copy metadata from real Slack notifications
notify-send --urgency=normal --app-name=slack --category im.received 'Kermit the Frog' "It ain't easy being green"

# big message to test notification size
notify-send --urgency=normal --app-name=slack --category im.received 'Lorem ipsum dolor sit amet, consectetur adipiscing elit' "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam aliquam felis a orci gravida, ut eleifend enim mollis. Maecenas convallis tristique ultrices. Nullam cursus turpis in eleifend semper. Ut efficitur pulvinar arcu sed lobortis. Ut quis lorem in lacus pulvinar bibendum vel laoreet dui. Donec efficitur, nisi quis volutpat convallis, quam enim tempor nisl, et porta est ligula nec massa. In hac habitasse platea dictumst. Praesent vel lacus bibendum, gravida nunc et, malesuada sapien. Maecenas et cursus enim. Integer placerat elementum lectus, vel hendrerit neque posuere id."

notify-send --urgency=critical --icon=face-worried 'this is fine' 'everything is on fire'

notify-send --urgency=critical --app-name=linux --icon=software-update-urgent 'Software Update' 'I simply must have the latest model!'

let VOLUME_ID = (notify-send --urgency=normal --app-name volume --expire-time 2000 --hint int:value:50 --print-id 'Volume' --transient | str trim)
[60, 70, 80, 90, 100] | each { |$it|
    notify-send --urgency=normal --app-name volume --expire-time 2000 --hint $'int:value:($it)' --replace-id ($VOLUME_ID) --transient 'Volume'
    sleep 0.5sec
}