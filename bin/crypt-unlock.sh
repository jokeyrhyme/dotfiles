#! /usr/bin/env sh

set -eu

# TODO: find more potential LUKS volumes e.g. NVMe
for DRIVE in /dev/sd*; do
  if ! cryptsetup luksDump ${DRIVE} >/dev/null 2>&1; then
    echo "${DRIVE} is probably not LUKS, skipping"
    continue
  fi

  LABEL=$(cryptsetup luksDump ${DRIVE} | grep Label: | cut -f 2)
  if [ "${LABEL}" = "(no label)" ]; then
    echo "${DRIVE} has no label, skipping"
    continue
  fi

  echo "${DRIVE} -> ${LABEL} ..."
  if [ -e /dev/mapper/"${LABEL}" ]; then
    echo "/dev/mapper/${LABEL} already exists"
  else
    cryptsetup open ${DRIVE} ${LABEL}
  fi
done
