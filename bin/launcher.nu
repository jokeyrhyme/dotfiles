#! /usr/bin/env nu

let cmd = [
  "/usr/bin/env"
  "--unset"
  "ALACRITTY_SOCKET"
  "--unset"
  "ALACRITTY_WINDOW_ID"
  "--unset"
  "WEZTERM_PANE"
  "--unset"
  "WEZTERM_UNIX_SOCKET"
  $"($env.HOME)/.cargo/bin/sklauncher"
  "--inline-info"
  "--preview-window"
  "down:20%"
]

let columns = 32
let font_size = 24
let rows = 12

# prefer alacritty here,
# as it's better suited for a fast-loading launcher
if (which 'alacritty' | is-not-empty) {
  exec alacritty --class launcher --option $'font.size=($font_size)' --option $'window.dimensions.columns=($columns)' --option $'window.dimensions.lines=($rows)' --command ...$cmd
}
if (which 'wezterm' | is-not-empty) {
  exec wezterm --config $'font_size=($font_size)' --config $'initial_cols=($columns)' --config $'initial_rows=($rows)' start --always-new-process --class launcher  --workspace launcher ...$cmd
}
