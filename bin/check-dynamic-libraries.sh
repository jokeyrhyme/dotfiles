#! /usr/bin/env sh

set -eu

for EXE in /usr/local/bin/* ~/.cargo/bin/*; do
  MISSING=$(ldd "${EXE}" 2>&1 | grep 'not found' || true)
  if [ -n "${MISSING}" ]; then
    echo "${EXE}"
    echo "${MISSING}"
  fi
done
