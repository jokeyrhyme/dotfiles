#! /usr/bin/env sh

set -eu

HOSTNAME="unknown"
if [ "${HOSTNAME}" = "unknown" ] && command -v hostnamectl >/dev/null; then
  # ||: fails when systemd is installed but not in use
  HOSTNAME="$(hostnamectl hostname || echo 'unknown')"
fi
if [ "${HOSTNAME}" = "unknown" ] && [ -r /etc/hostname ]; then
  HOSTNAME="$(cat /etc/hostname)"
fi

echo "${HOSTNAME}"
