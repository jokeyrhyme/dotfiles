#! /usr/bin/env sh

# purpose: run `chmod` recursively on directories not files
# usage: path/to/chdmod.sh 770 path/to/parent/directory
# ${1} = mode
# ${2} = path

set -eu

if [ ! -d "${2}" ]; then
  echo "second argument '${2}' is not a directory"
  exit 1
fi

find "${2}" -type d -exec chmod ${1} "{}" ";"
