#! /usr/bin/env sh

# see: https://wiki.samba.org/index.php/Setting_up_a_Share_Using_POSIX_ACLs

set -eu

if [ ! -d /srv/samba ]; then
  # nothing to do
  exit 0
fi

for SHARE in /srv/samba/*; do
  if grep " ${SHARE} " </etc/fstab >/dev/null 2>&1; then
    if mount | grep " ${SHARE} " >/dev/null 2>&1; then
      echo "${SHARE} already mounted"
    else
      sudo mount --verbose "${SHARE}"
    fi
  fi
done
