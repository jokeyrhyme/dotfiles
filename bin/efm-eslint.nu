#! /usr/bin/env -S nu --stdin
 
def main [
  --filepath: path
]: string -> string {
  $in | ^npm exec --yes eslint -- --format json --stdin --stdin-filename $filepath
  | from json
  | first
  | get messages
  | each { |msg| 
    let error_type = match $msg.severity {
      2 => 'e',
      1 => 'w',
      _ => 'i'
    }
    $'($msg.line),($msg.column)-($msg.endLine),($msg.endColumn): ($error_type): ($msg.message)'
  }
  | str join "\n"
}


