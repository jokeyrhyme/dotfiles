#! /usr/bin/env nu

let FILESYSTEMS = (df --exclude-type=devtmpfs --exclude-type=tmpfs --local | detect columns | reject 'on' | each { |FS|
    let USED = ($FS | get 'Use%' | str trim --char '%' | into int)
    $FS | update 'Use%' $USED | insert 'Free%' (100 - $USED)
})

# output the worst temperature
$FILESYSTEMS | sort-by 'Use%' | last | to json