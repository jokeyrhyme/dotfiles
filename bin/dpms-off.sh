#! /usr/bin/sh

set -eu

eval "$(systemctl --user show-environment | grep DISPLAY)"
wlr-randr | grep --extended-regexp '^\S+' | while read -r LABEL; do
  OUTPUT=$(echo "${LABEL}" | cut -d ' ' -f 1)
  wlr-randr --output "${OUTPUT}" --off

  # allow some time to settle,
  # to avoid a wayland/compositor crash
  sleep 3s
done
