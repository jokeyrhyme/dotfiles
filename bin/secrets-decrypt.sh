#! /usr/bin/env sh

set -eu

IDENTITY=~/.ssh/id_ed25519
if [ ! -r "${IDENTITY}" ]; then
  echo "error: ${IDENTITY} not found"
  exit 1
fi

for DECRYPTED in "${HOME}/.dotfiles/config/k3s/token.secret.txt" \
  "${HOME}/.dotfiles/k8s/gitlab-runner.secret.yaml" \
  "${HOME}/.dotfiles/k8s/pi-hole.secret.yaml"; do
  ENCRYPTED="${DECRYPTED}.age"
  if [ ! -r "${ENCRYPTED}" ]; then
    echo "error: ${ENCRYPTED} not found"
    exit 1
  fi

  rm --force "${DECRYPTED}"
  age --decrypt --identity "${IDENTITY}" --output "${DECRYPTED}" "${ENCRYPTED}"
  echo "age: ${ENCRYPTED} -> ${DECRYPTED}"
done
