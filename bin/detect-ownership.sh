#! /usr/bin/env sh

set -eu

HOSTNAME=$("${HOME}/.dotfiles/bin/detect-hostname.sh")

OWNERSHIP=work
case "${HOSTNAME}" in
myhnegon) OWNERSHIP=personal ;;
pihole) OWNERSHIP=personal ;;
printer-bridge-pi) OWNERSHIP=personal ;;
rpi-*) OWNERSHIP=personal ;;
valios) OWNERSHIP=personal ;;
*) ;;
esac

echo "${OWNERSHIP}"
