#! /usr/bin/env sh

# creates secure-boot files if they don't yet exist
# https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface/Secure_Boot#Using_your_own_keys

set -eu

if [ "${USER:-nobody}" != "root" ]; then
  echo "must be root"
  exit 1
fi

PRIVATE_DIR=/var/lib/sbctl/keys
PUBLIC_DIR=/efi/loader/keys/auto

if [ -e "${PRIVATE_DIR}/../GUID" ] && [ -e "${PRIVATE_DIR}/PK/PK.pem" ]; then
  echo "Secure Boot keys already exist"
else
  /usr/bin/rm -rf "${PUBLIC_DIR}" "${PRIVATE_DIR}/../GUID" "${PRIVATE_DIR}"
  /usr/bin/mkdir -p "${PUBLIC_DIR}"
  /usr/bin/sbctl create-keys
fi

# the remaining script below provides systemd-boot with access to {PK,KEK,db}.auth files

__dotfiles_ensure_certificates() { # $1=variable
  if [ -e "${PUBLIC_DIR}/${1}.cer" ]; then
    echo "${1}: DER format certificates already exists"
  else
    /usr/bin/openssl x509 -outform DER -in "${PRIVATE_DIR}/${1}/${1}.pem" -out "${PUBLIC_DIR}/${1}.cer"
  fi

  if [ -e "${PUBLIC_DIR}/${1}.esl" ]; then
    echo "${1}: EFI Signature List format certificates already exists"
  else
    /usr/bin/cert-to-efi-sig-list -g "$(cat ${PRIVATE_DIR}/../GUID)" "${PRIVATE_DIR}/${1}/${1}.pem" "${PUBLIC_DIR}/${1}.esl"
  fi
}

__dotfiles_ensure_certificates "PK"
__dotfiles_ensure_certificates "KEK"
__dotfiles_ensure_certificates "db"

__dotfiles_ensure_authentication_header() { # #1=in-variable $2=out-variable
  if [ -e "${PUBLIC_DIR}/${2}.auth" ]; then
    echo "${1}->${2}: EFI Signature List format certificates with authentication header already exists"
  else
    /usr/bin/sign-efi-sig-list -g "$(cat ${PRIVATE_DIR}/../GUID)" -k "${PRIVATE_DIR}/${1}/${1}.key" -c "${PRIVATE_DIR}/${1}/${1}.pem" "${2}" "${PUBLIC_DIR}/${2}.esl" "${PUBLIC_DIR}/${2}.auth"
  fi
}

__dotfiles_ensure_authentication_header "PK" "PK"
__dotfiles_ensure_authentication_header "PK" "KEK"
__dotfiles_ensure_authentication_header "KEK" "db"
