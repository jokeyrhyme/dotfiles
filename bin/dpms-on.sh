#! /usr/bin/sh

set -eu

eval "$(systemctl --user show-environment | grep DISPLAY)"
wlr-randr | grep --extended-regexp '^\S+' | while read -r LABEL; do
  OUTPUT=$(echo "${LABEL}" | cut -d ' ' -f 1)
  if [ "${LABEL}" = 'eDP-1 "Sharp Corporation 0x14D0 (eDP-1)"' ]; then
    if ! grep -rc closed /proc/acpi/button/lid/*/state >/dev/null; then
      wlr-randr --output "${OUTPUT}" --on
    fi
  else
    wlr-randr --output "${OUTPUT}" --on
  fi
done

# lock screen when coming out of sleep
setsid --fork ~/.dotfiles/bin/lock-screen.sh
