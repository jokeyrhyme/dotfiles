#! /usr/bin/env sh

# purpose: lock screen and properly handle notifications

set -e

~/.dotfiles/bin/notifications-disable.sh

eval "$(systemctl --user show-environment | grep DISPLAY)"
if command -v waylock >/dev/null; then
  # avoid having multiple lock processes
  if ! pgrep waylock >/dev/null; then
    waylock
  fi
elif command -v swaylock >/dev/null; then
  # avoid having multiple lock processes
  if ! pgrep swaylock >/dev/null; then
    swaylock
  fi
fi

~/.dotfiles/bin/notifications-enable.sh
