#! /usr/bin/env nu

use '../lib/btrfs.nu' [ btrfs_balance_status list_btrfs_mounts ]

def main [] {
    let devices = (main status)
    print $devices
}

# ensures that no btrfs balance operations are in-progress
def "main cancel" [] {
    let mounts = (main status)
    $mounts |
        filter { |mount| ($mount.status) == 'running' } |
        each { |mount| 
            ^sudo btrfs balance pause $mount.path
        } | ignore
    
    let mounts = (main status)
    print $mounts
}

# ensures that a btrfs balance operation is in-progress on all mounts
def "main continue" [] {
    let mounts = (main status)

    $mounts |
        filter { |mount| ($mount.status) != 'running' } |
        each { |mount|
            ^sudo btrfs balance start --full-balance --background --enqueue ($mount.path)
        } | ignore

    # TODO: how do we `btrfs balance resume` in the background, seems to block?

    let mounts = (main status)
    print $mounts
}

def "main status" [
    --watch
] {
    loop {
        let mounts = (list_btrfs_mounts | each { |mount| btrfs_balance_status $mount.path })

        if not ($watch) {
            return $mounts
        }
    
        clear
        print $mounts

        sleep 30sec
    }
}
