#! /usr/bin/env nu

use '../lib/btrfs.nu' [ btrfs_scrub_status list_btrfs_devices ]

def main [] {
    let devices = (main status)
    print $devices
}

# ensures that no btrfs scrub operations are in-progress
def "main cancel" [] {
    let devices = (main status)
    let running = $devices | filter { |row| ($row | get 'status') == 'running' }
    $running | each { |row| 
        ^pkexec btrfs scrub cancel $row.device
    } | ignore
    
    let devices = (main status)
    print $devices
}

# ensures that a btrfs scrub operation is in-progress on one or more devices
def "main continue" [] {
    let devices = (main status)

    let running = $devices | filter { |row| ($row | get 'status') == 'running' }
    if ($running | is-not-empty) {
        return
    }
    
    let interrupted = $devices | filter { |row| ($row | get 'status') == 'interrupted' }
    if ($interrupted | is-not-empty) {
        ^pkexec btrfs scrub resume -c3 ($interrupted | shuffle | first).device
        return
    }

    let aborted = $devices | filter { |row| ($row | get 'status') == 'aborted' }
    if ($aborted | is-not-empty) {
        ^pkexec btrfs scrub resume -c3 ($aborted | shuffle | first).device
        return
    }

    let finished = $devices | filter { |row| ($row | get 'status') == 'finished' }
    if ($finished | is-not-empty) {
        ^pkexec btrfs scrub start -c3 ($finished | shuffle | first).device
        return
    }
}

def "main status" [
    --watch
] {
    loop {
        let devices = (
            list_btrfs_devices |
                each { |device| btrfs_scrub_status $device | reject --ignore-errors "duration" "time_left" "uuid" }
        )

        if not ($watch) {
            return $devices
        }

        clear
        print $devices

        sleep 30sec
    }
}
