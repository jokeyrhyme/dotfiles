#! /usr/bin/env sh

set -e

if command -v dconf >/dev/null; then
  dconf write /org/gnome/desktop/notifications/show-banners true >/dev/null 2>&1 || true
fi
if command -v dunstctl >/dev/null; then
  dunstctl set-paused false >/dev/null 2>&1 || true
fi
