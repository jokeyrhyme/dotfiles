#! /usr/bin/nu

# usage: finds device temperatures reported via hwmon
# see also: https://www.kernel.org/doc/html/latest/hwmon/sysfs-interface.html

# assumption: 0C to 90C is a good default preferred temperature rage
let FALLBACK_HIGH_THRESHOLD = 90000 # for when we can't identity the bad threshold
let FALLBACK_LOW_THRESHOLD = 0      # for when we can't identity the bad threshold

# some thresholds are in millivolts instead of millidegrees Celsius,
# so we'll assume that anything outside of -100C < x < 200C is really in millivolts
let TOO_HIGH_TO_BE_CELSIUS = 200000
let TOO_LOW_TO_BE_CELSIUS = -100000

def read_number_file [FILE, FALLBACK] {
    if ($FILE | path exists) {
        try {
            open --raw $FILE | str trim | into int
        } catch {
            # some files result in "No data available (os error 61)"
            $FALLBACK
        }
    } else {
        $FALLBACK
    }
}

def read_celsius_file [FILE, FALLBACK] {
    let NUMBER = (read_number_file $FILE $FALLBACK)
    if $NUMBER > $TOO_HIGH_TO_BE_CELSIUS {
        $FALLBACK_HIGH_THRESHOLD
    } else if $NUMBER < $TOO_LOW_TO_BE_CELSIUS {
        $FALLBACK_LOW_THRESHOLD
    } else {
        $NUMBER
    }
}

def map_value_to_range [VALUE, OLD_MIN, OLD_MAX, NEW_MIN, NEW_MAX] {
  # CC BY-SA 2.5: https://stackoverflow.com/a/5295202/488373
  ($NEW_MIN + (($NEW_MAX - $NEW_MIN) * ($VALUE - $OLD_MIN)) / ($OLD_MAX - $OLD_MIN))
}

def map_value_to_percent [VALUE, OLD_MIN, OLD_MAX] {
  (map_value_to_range $VALUE $OLD_MIN $OLD_MAX 0 100)
}

let INPUTS = (fd --follow --max-depth=2 'temp[[:digit:]]+_input' /sys/class/hwmon/ | lines | each { |INPUT_FILE|
    let TEMP_PREFIX = ($INPUT_FILE | str replace '_input$' '')
    let TEMP = ($TEMP_PREFIX | path basename)

    let LABEL_FILE = $'($TEMP_PREFIX)_label'
    let LABEL = (if ($LABEL_FILE | path exists) {
        open $LABEL_FILE | str trim
    } else {
        ''
    })

    let HWMON_DIR = ($INPUT_FILE | path dirname)
    let NAME_FILE = ($HWMON_DIR | path join 'name')

    # from kernel docs: emergency >= crit >= max
    let EMERGENCY_FILE = $'($TEMP_PREFIX)_emergency'
    let CRIT_FILE = $'($TEMP_PREFIX)_crit'
    let MAX_FILE = $'($TEMP_PREFIX)_max'
    let MIN_FILE = $'($TEMP_PREFIX)_min'

    let EMERGENCY = (read_celsius_file $EMERGENCY_FILE $FALLBACK_HIGH_THRESHOLD)
    let CRIT = (read_celsius_file $CRIT_FILE $FALLBACK_HIGH_THRESHOLD)
    let MAX = (read_celsius_file $MAX_FILE $FALLBACK_HIGH_THRESHOLD)
    let MIN = (read_celsius_file $MIN_FILE $FALLBACK_LOW_THRESHOLD)

    let HWMON = ($HWMON_DIR | path basename)
    let INPUT = (read_number_file $INPUT_FILE 0)

    {
        crit: $CRIT,
        emergency: $EMERGENCY,
        hwmon: $HWMON,
        input: $INPUT,
        input_file: $INPUT_FILE,
        label: $LABEL,
        max: $MAX,
        name: (open $NAME_FILE | str trim),
        percent: (map_value_to_percent $INPUT $MIN $MAX),
        temp: $TEMP,
    }
})

# output the worst temperature
$INPUTS | sort-by 'percent' | last | to json