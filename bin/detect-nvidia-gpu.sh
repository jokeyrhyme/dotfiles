#! /usr/bin/env sh

set -eu

if ! command -v lspci >/dev/null 2>&1; then
  exit 1
fi

DEVICES=$(lspci)
if echo "$DEVICES" | grep " 3D controller: NVIDIA Corporation " >/dev/null 2>&1; then
  exit 0
elif echo "$DEVICES" | grep " VGA compatible controller: NVIDIA Corporation " >/dev/null 2>&1; then
  exit 0
fi

exit 1
