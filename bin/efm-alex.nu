#! /usr/bin/env -S nu --stdin
 
def main [
  --filepath: string
]: string -> string {
  $in | match ($filepath | path parse).extension {
    'htm' | 'html' => (^npm exec --yes alex -- --html --quiet --stdin)
    'mdx' => (^npm exec --yes alex -- --mdx --quiet --stdin)
    'md' => (^npm exec --yes alex -- --quiet --stdin)
    _ => (^npm exec --yes alex -- --quiet --stdin --text)
  } o+e>|
  | lines
  | parse --regex '\s*(?<line>\d+):(?<col>\d+)-(?<end_line>\d+):(?<end_col>\d+)\s\s(?<type>warning|error)\s\s(?<message>.+)\s\s(?<rule>.+)\s\s(?<source>.+)$'
  | each { |msg| 
    let error_type = match $msg.type {
      'error' => 'e',
      'warning' => 'w',
      _ => 'i'
    }
    $'($msg.line),($msg.col)-($msg.end_line),($msg.end_col): ($error_type): ($msg.message)'
  }
  | str join "\n"
}


