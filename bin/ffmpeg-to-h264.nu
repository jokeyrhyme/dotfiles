#! /usr/bin/env nu

# from: https://superuser.com/questions/1380946/how-do-i-convert-10-bit-h-265-hevc-videos-to-h-264-without-quality-loss#1381151

def main [output_dir: path] {
    print $output_dir
    ls | find .avi .m4v .mkv .mp4 | each { |it|
        let INPUT = ($it.name | ansi strip)
        let OUTPUT = ($INPUT | str replace --all '(h|x)265 ?' '' | str replace --all '\.(avi|m4v|mkv|mp4)$' '.h264.mkv' | path dirname --replace $output_dir)
        print $INPUT $OUTPUT
        # -crf 19: one notch worse from "visually-lossless" 17-18
        # -preset slow: given crf, work harder to reduce file size
        ffmpeg -i $INPUT -c:v libx264 -crf 19 -preset slow -vf format=yuv420p -c:a copy -c:s srt $OUTPUT
    }
}