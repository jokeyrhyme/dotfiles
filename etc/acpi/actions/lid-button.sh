#! /usr/bin/env sh

set -e

# see: https://karlcode.owtelse.com/blog/2017/01/09/disabling-usb-ports-on-linux/

# devices:
# 0bda:5510 Realtek Semiconductor Corp. Integrated_Webcam_HD

__dotfiles_lid_closed() {
  for DEVICE in /sys/bus/usb/devices/*/product; do
    PRODUCT=$(cat $DEVICE)
    VENDOR_ID=$(cat "$(dirname $DEVICE)/idVendor")
    USB_PORT=$(basename "$(dirname $DEVICE)")
    if [ "$VENDOR_ID" = "0bda" ] && [ "$PRODUCT" = "Integrated_Webcam_HD" ]; then
      # || true: ignore errors, e.g. the device might already be unbound
      echo "$USB_PORT" | tee /sys/bus/usb/drivers/usb/unbind || true
    fi
  done
}

__dotfiles_lid_opened() {
  for DEVICE in /sys/bus/usb/devices/*/product; do
    PRODUCT=$(cat $DEVICE)
    VENDOR_ID=$(cat "$(dirname $DEVICE)/idVendor")
    USB_PORT=$(basename "$(dirname $DEVICE)")
    if [ "$VENDOR_ID" = "0bda" ] && [ "$PRODUCT" = "Integrated_Webcam_HD" ]; then
      # || true: ignore errors, e.g. the device might already be bound
      echo "$USB_PORT" | tee /sys/bus/usb/drivers/usb/bind || true
    fi
  done
}

case "$3" in
close) __dotfiles_lid_closed ;;
open) __dotfiles_lid_opened ;;
*)
  if [ -d /proc/acpi/button/lid ] && grep -rc closed /proc/acpi/button/lid/*/state >/dev/null; then
    __dotfiles_lid_closed
  else
    __dotfiles_lid_opened
  fi
  ;;
esac
