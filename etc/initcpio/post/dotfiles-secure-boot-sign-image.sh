#! /usr/bin/env bash

# see: https://bentley.link/secureboot/
# see: https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface/Secure_Boot#Signing_the_kernel_with_a_pacman_hook
# see: https://linderud.dev/blog/mkinitcpio-v31-and-uefi-stubs/
# see: `man mkinitcpio` "ABOUT POST HOOKS"

set -eux -o pipefail

UKI="${3}"
/usr/bin/sbctl sign --save "${UKI}"
