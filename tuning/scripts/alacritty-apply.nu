#! /usr/bin/env nu

let CONFIG_PATH = $'($env.HOME)/.config/alacritty/dotfiles-nushell.toml'
touch $CONFIG_PATH

let NU_PATH = (which 'nu' | first | get 'path')

mut CONFIG = {
  env: {
    SHELL: $NU_PATH
  }
  terminal: {
    shell: $NU_PATH
  }
}

$CONFIG | to toml | save --force $CONFIG_PATH

let CONFIG_PATH = $'($env.HOME)/.config/alacritty/dotfiles-font.toml'
touch $CONFIG_PATH

let FONT_SIZE = (if ((sys host).name == 'Darwin') { 16.0 } else { 12.0 })

mut CONFIG = {
  font: {
    size: $FONT_SIZE
  }
}

$CONFIG | to toml | save --force $CONFIG_PATH

