#! /usr/bin/env nu

let OS = match ((sys host).name) {
    'Darwin' => 'apple-darwin',
    _ => 'linux',
}
let MACHINE = match ((uname).machine | str downcase) {
    'arm64' => 'aarch64',
    _ => 'x86_64',
}

mkdir  ~/.local/bin
http get $'https://downloads.haskell.org/~ghcup/($MACHINE)-($OS)-ghcup'
| save --force --progress ~/.local/bin/ghcup

^chmod a+x ~/.local/bin/ghcup
