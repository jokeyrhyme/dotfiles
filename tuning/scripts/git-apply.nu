#! /usr/bin/env nu

# see: https://git-scm.com/docs/git-config

# note: `git config --unset` fails if value not set

try {
    ^git config --global --get user.email
} catch {
    ^git config --global user.email 'jokeyrhyme@jokeyrhy.me'
}

try { ^git config --global --unset-all user.name } catch {}
^git config --global user.name 'Ron Waldon-Howe'

# see: https://docs.github.com/en/authentication/managing-commit-signature-verification/telling-git-about-your-signing-key
try { ^git config --global --unset gpg.program } catch {}
^git config --global gpg.format 'ssh'
try { ^git config --global --unset user.signingKey } catch {}
[
    '~/.ssh/id_ed25519.pub',
    '~/.ssh/id_ed25519_sk.pub', # prefer security-key resident SSH credentials
] | each { |PUB|
    if ($PUB | path exists) {
        ^git config --global user.signingKey ($PUB | path expand)
    }
}

# see: https://docs.github.com/en/authentication/managing-commit-signature-verification/signing-commits
^git config --global commit.gpgsign true

^git config --global push.default 'simple'

if (which 'gpg' | is-not-empty) {
    ^git config --global gpg.program 'gpg'
} else {
    try { ^git config --global --unset gpg.program } catch {}
}

# prevent accidents on case-insensitive file systems
^git config --global core.ignorecase false

# https://github.com/sharkdp/bat
try { ^git config --global --unset pager.log } catch {}
try { ^git config --global --unset pager.reflog } catch {}
try { ^git config --global --unset pager.show } catch {}
if (which 'bat' | is-not-empty) {
    ^git config --global core.pager 'bat'
    ^git config --global pager.diff 'bat --style=plain'
} else {
    try { ^git config --global --unset core.pager } catch {}
    try { ^git config --global --unset pager.diff } catch {}
}

# https://difftastic.wilfred.me.uk/git.html
try { ^git config --global --unset delta.navigate } catch {}
try { ^git config --global --unset delta.syntax-theme } catch {}
try { ^git config --global --unset interactive.diffFilter } catch {}
if (which 'difft' | is-not-empty) {
    ^git config --global diff.external 'difft'
} else {
    try { ^git config --global --unset diff.external } catch {}
}

try { ^git config --global --unset sequence.editor } catch {}

if (which 'git-lfs' | is-not-empty) {
    ^git lfs install
}

^git config --global color.ui 'auto'

# https://github.blog/2022-01-24-highlights-from-git-2-35/
^git config --global diff.algorithm histogram

^git config --global pull.rebase true
^git config --global push.autoSetupRemote true
^git config --global push.default 'simple'
^git config --global protocol.version '2'

# https://about.gitlab.com/2016/12/08/git-tips-and-tricks/
^git config --global fetch.prune true
^git config --global rebase.autosquash true
^git config --global status.branch true
^git config --global status.short true
^git config --global status.submoduleSummary true

# https://youtu.be/Md44rcw13k4
^git config --global rebase.updateRefs true

# http://blog.kfish.org/2010/04/git-lola.html
^git config --global alias.lol 'log --graph --decorate --pretty=oneline --abbrev-commit'
^git config --global alias.lola 'log --graph --decorate --pretty=oneline --abbrev-commit --all'

let GIT_MERGE_FILE_HELP = (^git merge-file --help | complete).stdout
if ($GIT_MERGE_FILE_HELP | str contains '"zdiff3"') {
    # http://psung.blogspot.com.au/2011/02/reducing-merge-headaches-git-meets.html
    # https://github.blog/2022-01-24-highlights-from-git-2-35/
    ^git config --global merge.conflictStyle zdiff3
} else if ($GIT_MERGE_FILE_HELP | str contains '"diff3"') {
    ^git config --global merge.conflictStyle diff3
} else {
  try { ^git config --global --unset merge.conflictStyle } catch {}
}

# https://github.blog/2019-02-24-highlights-from-git-2-21/
^git config --global log.date "auto:human"

# https://sfconservancy.org/news/2020/jun/23/gitbranchname/
# https://git-scm.com/docs/git-config#Documentation/git-config.txt-initdefaultBranch
^git config --global init.defaultBranch "main"

# by convention,
# - we use HTTPS remotes for public repositories
# - we use SSH+GIT remotes for private repositories
# write access for a public HTTPS git remote still requires authentication,
# so we prefer SSH+GIT for that,
# otherwise, we stick with unauthenticated HTTPS for read-only access
# (so we don't need `cargo`'s net.git-fetch-with-cli setting)
[ 'bitbucket.org', 'codeberg.org', 'git.sr.ht', 'github.com', 'gitlab.com', 'gitlab.freedesktop.org', 'gitlab.gnome.org']
| each { |HOST|
    try { ^git config --global --remove-section $'url.git@($HOST):' o+e>| ignore } catch {}
    try { ^git config --global --remove-section $'url."git@($HOST):"' o+e>| ignore } catch {}
    ^git config --global $'url.git@($HOST):.pushInsteadOf' $'https://($HOST)'
}

# trying to grab all submodules automatically
^git config --global fetch.recurseSubmodules true
^git config --global submodule.recurse true

if (which 'ollama' | is-not-empty) {
    ^git config --global gitbutler.aiModelProvider ollama
    ^git config --global gitbutler.aiOllamaModelName qwen2.5:latest
}
