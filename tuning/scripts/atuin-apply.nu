#! /usr/bin/env nu

mkdir ~/.cache/atuin
if (which atuin | is-not-empty) {
    ^atuin init nu | save --force ~/.cache/atuin/init.nu
} else {
    touch ~/.cache/atuin/init.nu
}

