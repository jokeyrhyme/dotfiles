#! /usr/bin/env nu

let COLOR_SCHEME = (^dconf read /org/gnome/desktop/interface/color-scheme)
^dconf reset -f /org/gnome/
^dconf write /org/gnome/desktop/interface/color-scheme $COLOR_SCHEME

rm --force --recursive ~/.cache/babl ~/.cache/gegl-* ~/.cache/geocode-glib ~/.cache/gnome-* ~/.cache/gstreamer-* ~/.cache/libgweather ~/.cache/yelp ~/.cache/zenith
rm --force --recursive ~/.config/evolution ~/.config/goa-* ~/.config/.gsd-* ~/.config/gtk-* ~/.config/nautilus ~/.config/yelp
rm --force --recursive ~/.local/share/epiphany ~/.local/share/evolution ~/.local/share/gegl-* ~/.local/share/gnome-* ~/.local/share/gvfs-* ~/.local/share/nautilus ~/.local/share/webkitgtk ~/.local/share/yelp

^dconf write /org/gnome/desktop/datetime/automatic-timezone true

# we set `XKB_...` over in ./config/environment.d/95-dotfiles-wayland.conf
if ($env.XKB_DEFAULT_OPTIONS? | default '' | is-not-empty) {
    ^dconf write /org/gnome/desktop/input-sources/xkb-options $"['($env.XKB_DEFAULT_OPTIONS?)', 'layout:($env.XKB_DEFAULT_LAYOUT? | default 'us')', 'model:($env.XKB_DEFAULT_MODEL? | default 'pc101')']"
} else {
    ^dconf write /org/gnome/desktop/input-sources/xkb-options $"['layout:($env.XKB_DEFAULT_LAYOUT? | default 'us')', 'model:($env.XKB_DEFAULT_MODEL? | default 'pc101')']"
}

^dconf write /org/gnome/desktop/interface/document-font-name '"Inter 11"'
^dconf write /org/gnome/desktop/interface/font-name '"Inter 11"'
^dconf write /org/gnome/desktop/interface/monospace-font-name '"JetBrains Mono 12"'

^dconf write /org/gnome/desktop/media-handling/autorun-never true

^dconf write /org/gnome/desktop/notifications/show-in-lock-screen false

^dconf write /org/gnome/desktop/peripherals/touchpad/tap-to-click true
^dconf write /org/gnome/desktop/peripherals/touchpad/two-finger-scrolling-enabled true

^dconf write /org/gnome/desktop/privacy/old-files-age 'uint32 14'
^dconf write /org/gnome/desktop/privacy/recent-files-max-age 1
^dconf write /org/gnome/desktop/privacy/remove-old-temp-files true
^dconf write /org/gnome/desktop/privacy/remove-old-trash-files true

^dconf write /org/gnome/desktop/search-providers/disable-external true

# lock after 5 minutes
^dconf write /org/gnome/desktop/screensaver/lock-delay 'uint32 0'
^dconf write /org/gnome/desktop/screensaver/lock-enabled true
^dconf write /org/gnome/desktop/session/idle-delay 'uint32 300'

# enable experimental scaling options in GNOME 45
# see: https://www.omglinux.com/how-to-enable-fractional-scaling-fedora/
^dconf write /org/gnome/mutter/experimental-features "['scale-monitor-framebuffer']"

^dconf write /org/gnome/nautilus/icon-view/default-zoom-level '"small-plus"'
^dconf write /org/gnome/nautilus/list-view/default-zoom-level '"small"'
^dconf write /org/gnome/nautilus/preferences/default-folder-viewer '"list-view"'

^dconf write /org/gnome/settings-daemon/plugins/color/night-light-enabled true
^dconf write /org/gnome/settings-daemon/plugins/power/power-button-action '"interactive"'

^dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/']"
^dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/binding "'<Super>slash'"
^dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/command $"'($env.HOME)/.dotfiles/bin/launcher.nu'"
^dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/name "'Launcher'"

# TODO: instead of hardcoding, set according to what is present on the machine
let OWNERSHIP = ((~/.dotfiles/bin/detect-ownership.sh | complete).stdout | str trim)
if $OWNERSHIP == 'personal' {
    ^dconf write /org/gnome/shell/favorite-apps "['com.google.Chrome.desktop', 'org.gnome.Nautilus.desktop', 'org.wezfurlong.wezterm.desktop', 'com.discordapp.Discord.desktop', 'com.mattermost.Desktop', 'im.riot.Riot.desktop', 'org.gnome.Fractal.desktop', 'org.signal.Signal.desktop', 'com.valvesoftware.Steam.desktop']"
} else {
    ^dconf write /org/gnome/shell/favorite-apps "['com.google.Chrome.desktop', 'org.gnome.Nautilus.desktop', 'org.wezfurlong.wezterm.desktop', 'com.slack.Slack.desktop']"
}

^dconf write /org/gnome/shell/welcome-dialog-last-shown-version "'45.1'"

^dconf write /org/gnome/system/location/enabled true
