#! /usr/bin/env nu

let UNIT_NAME = 'speech-dispatcherd.service'
let USER_UNITS_DIR = $'($env.HOME)/.config/systemd/user'

mkdir $USER_UNITS_DIR

# cleaning up previous approaches/attempts
try { ^systemctl disable --now --user $UNIT_NAME } catch {}
rm --force $'($USER_UNITS_DIR)/speech-dispatcher.service' $'($USER_UNITS_DIR)/($UNIT_NAME)'

open $'/usr/lib/systemd/system/($UNIT_NAME)'
| str replace --all --multiline --regex '^WantedBy=multi-user.target' 'WantedBy=default.target'
| save --force $'($USER_UNITS_DIR)/($UNIT_NAME)'

^systemctl enable --now --user $UNIT_NAME
