#! /usr/bin/env nu

# https://github.com/alacritty/alacritty/issues/3962#issuecomment-862212371

# we're using `find` here, 
# so we don't rely on the exact hashing/sharding mechanism of any given `tic`,
# although it would be so much faster to directly check the exact file paths

mkdir ~/.terminfo
 
let RESULTS = (^find ~/.terminfo -name alacritty)
if ($RESULTS | is-empty) {
    http get https://raw.githubusercontent.com/alacritty/alacritty/master/extra/alacritty.info | ^tic -x -
}

let RESULTS = (^find ~/.terminfo -name wezterm)
if ($RESULTS | is-empty) {
    http get https://raw.githubusercontent.com/wez/wezterm/main/termwiz/data/wezterm.terminfo | ^tic -x -
}

let RESULTS = (^find ~/.terminfo -name xterm-kitty)
if ($RESULTS | is-empty) {
    http get https://raw.githubusercontent.com/kovidgoyal/kitty/master/terminfo/kitty.terminfo | ^tic -x -
}
