#! /usr/bin/env nu

# from: https://github.com/probonopd/go-appimage/tree/master/src/appimaged#readme

try { ^systemctl --user disable --now appimaged.service } catch {}
rm --force ~/.config/systemd/user/appimaged.service
rm --force ~/.local/share/applications/appimagekit*.desktop
rm --force ~/.local/share/applications/appimage*
rm --force ~/Applications/appimaged-*.AppImage

if ('~/.config/systemd/user/default.target.wants/appimagelauncherd.service' | path type) == 'file' {
    rm --force ~/.config/systemd/user/default.target.wants/appimagelauncherd.service
}

let MACHINE = (uname).machine

mkdir ~/Applications
# note: nushell has `from xml` but we intentionally avoid it for simplicity and future-proofing
# (GitHub could change the structure of the HTML here in ways that borks an XML approach)
let ASSET_PATH = (
    http get https://github.com/probonopd/go-appimage/releases/expanded_assets/continuous
    | lines
    | find --regex $'appimaged-.*-($MACHINE).AppImage'
    | first
    | parse --regex '^.*\bhref="(?P<asset_path>[^"]+)".*$'
    | first
    | get asset_path
)
let FILE_NAME = ($ASSET_PATH | path basename)
http get $'https://github.com($ASSET_PATH)' | save --force --progress --raw $'($env.HOME)/Applications/($FILE_NAME)'

^chmod a+x $'($env.HOME)/Applications/($FILE_NAME)'

exec $'($env.HOME)/Applications/($FILE_NAME)'
