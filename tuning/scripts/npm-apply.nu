#! /usr/bin/env nu

^npm config delete init.author.name init.author.email init.author.url

mkdir $'($env.HOME)/.local/node_modules'
^npm config set prefix $'($env.HOME)/.local/node_modules'
