#! /usr/bin/env nu

^chown -R $env.USER ~/.ssh
^chmod -R go-rwx ~/.ssh
mkdir ~/.ssh/config.d ~/.ssh/control

if (which 'ssh-sensible' | is-not-empty) {
    let SENSIBLE_CONFIG = $'($env.HOME)/.ssh/config.d/10-ssh-sensible'
    [
      (^ssh-sensible Ciphers)
      (^ssh-sensible KexAlgorithms)
      (^ssh-sensible MACs)
    ]
    | str join "\n"
    | save --force $SENSIBLE_CONFIG
}
