#! /usr/bin/env nu

use ../../lib/git.nu [ has_git_updates ]

let REMOTE_REPO = 'https://github.com/helix-editor/helix.git'
let CLONE_DIR = $'($env.HOME)/GitHub/helix-editor/helix'
if (has_git_updates $REMOTE_REPO $CLONE_DIR) {
    cd $CLONE_DIR
    ^rustup toolchain install
    ^cargo install --locked --path helix-term
}

if (which 'hx' | is-not-empty) {
    try {
        ^hx --grammar fetch
    } catch {
        # this frequently fails because a grammar remote rebased or something,
        # so delete them all and try again
        let HELIX_RUNTIME = ($env.HELIX_RUNTIME? | default $'($env.HOME)/.config/helix/runtime')
        rm --force --recursive $'($HELIX_RUNTIME)/grammars/sources'
        ^hx --grammar fetch
    }
    ^hx --grammar build
}

