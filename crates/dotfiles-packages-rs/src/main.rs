#![deny(clippy::all, clippy::pedantic, unsafe_code)]

use std::{
    collections::HashMap,
    env::args,
    fs::{read_to_string, write},
    path::PathBuf,
};

mod condition;
mod packages;
mod repository;
mod requirements;

use crate::{
    condition::detect,
    packages::{parse_packages_toml, read_packages_file, PackageSpec, RepositorySpec},
    repository::{Action, LocalIndexer, Repository},
    requirements::{parse_requirements_toml, read_requirements_file, Group},
};

fn main() {
    let detected_conditions = detect();
    eprintln!("detected conditions: {detected_conditions:?}");

    let packages_doc = parse_packages_toml(&read_packages_file());
    let detected_repositories: Vec<Repository> = packages_doc
        .repositories
        .enabled
        .iter()
        .filter(|&x| Repository::exists(x))
        .cloned()
        .collect();
    eprintln!("existing repositories: {detected_repositories:?}");

    let requirements_doc = parse_requirements_toml(&read_requirements_file());
    let groups: Vec<Group> = requirements_doc
        .groups
        .into_iter()
        .filter(|g| g.matches_conditions(&detected_conditions))
        .collect();
    let packages: Vec<String> = groups.into_iter().flat_map(|g| g.packages).collect();

    let (mut assignments, unmatched_packages) =
        calculate_assignments(&detected_repositories, &packages_doc.packages, &packages);
    dbg!(&unmatched_packages);

    let mut files: HashMap<PathBuf, Vec<String>> = HashMap::new();
    let mut script: Vec<String> = vec![];
    for key in &detected_repositories {
        let packages = assignments.entry(key.clone()).or_default();
        packages.sort_unstable();
        eprintln!("{key:?}: {packages:?}");
        let mut pkgr = key.repository();
        pkgr.detect();

        for action in pkgr.plan(packages) {
            match action {
                Action::Command(cmd) => script.push(cmd),
                Action::LineInFile(file_path, line) => {
                    let lines = files.entry(file_path).or_default();
                    lines.push(line);
                    // TODO it might not always be valid to sort these files,
                    // but it makes the later file comparison easier to manage
                    lines.sort_unstable();
                }
            }
        }
    }

    if script.is_empty() {
        eprintln!("no commands needed for system to converge towards requirements.toml");
    } else {
        dbg!(&script);
        println!("{}", script.as_slice().join("\n"));
    }

    let files = files.into_iter().fold(
        HashMap::<PathBuf, String>::new(),
        |mut accum, (file_path, lines)| {
            accum.insert(file_path, lines.join("\n"));
            accum
        },
    );
    let files: HashMap<PathBuf, String> = files
        .into_iter()
        .filter(|(file_path, content)| {
            let existing_content = read_to_string(file_path).unwrap_or_default();
            existing_content.trim() != content.trim()
        })
        .collect();

    if files.is_empty() {
        eprintln!("no files need to be written to converge towards requirements.toml");
    } else if args().any(|a| &a == "DRY_RUN=true") {
        for (file_path, content) in files {
            eprintln!(
                "(dry-run): we would write to {} the following:\n{}",
                file_path.display(),
                content
            );
        }
    } else {
        for (file_path, content) in files {
            eprintln!("writing necessary changes to {} ...", file_path.display());
            write(file_path, content).expect("should write to file");
        }
    }
}

fn calculate_assignments(
    detected_repositories: &[Repository],
    package_specs: &HashMap<String, PackageSpec>,
    packages: &[String],
) -> (HashMap<Repository, Vec<String>>, Vec<String>) {
    let mut indexers = HashMap::<Repository, Box<dyn LocalIndexer>>::new();
    for pkgr in detected_repositories {
        if let Some(mut indexer) = pkgr.indexer() {
            indexer.update();
            indexers.insert(pkgr.clone(), indexer);
        }
    }

    let mut unmatched_packages = vec![];
    let assignments: Vec<(String, Repository)> = packages
        .iter()
        .filter_map(|p| {
            for pkgr in detected_repositories {
                let empty_package_spec: PackageSpec = HashMap::new();
                let default_repository_spec = pkgr.default_spec();
                let package_spec = package_specs.get(p).unwrap_or(&empty_package_spec);
                let repository_spec = package_spec.get(pkgr).unwrap_or(&default_repository_spec);

                let pkg_name = match repository_spec {
                    RepositorySpec::Enabled(false) => {
                        // we've disabled this repository, check the next one
                        continue;
                    }
                    RepositorySpec::Enabled(true) => p.clone(),
                    RepositorySpec::Rename(name) => name.clone(),
                };

                match indexers.get(pkgr) {
                    None => {
                        // no indexer, let's assume it's available (since we enabled it)
                        return Some((pkg_name, pkgr.clone()));
                    }
                    Some(indexer) => {
                        if indexer.contains(&pkg_name) {
                            return Some((pkg_name, pkgr.clone()));
                        }
                    }
                }
            }
            // we've run out of repositories and have not yet found a match
            unmatched_packages.push(p);
            None
        })
        .collect();

    unmatched_packages.sort_unstable();

    (
        assignments
            .into_iter()
            .fold(HashMap::new(), |mut map, (pkg, pkgr)| {
                map.entry(pkgr)
                    .and_modify(|packages: &mut Vec<String>| packages.push(pkg.clone()))
                    .or_insert(vec![pkg.clone()]);
                map
            }),
        unmatched_packages.into_iter().cloned().collect(),
    )
}
