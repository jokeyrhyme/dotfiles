use std::{
    env::{args, consts::OS},
    fs::read_to_string,
    path::Path,
    process::{Command, Stdio},
};

use serde::Deserialize;

#[derive(Clone, Debug, Deserialize, Eq, Hash, Ord, PartialEq, PartialOrd)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Condition {
    AmdCpu,
    AmdGpu,
    ArchLinux,
    Btrfs,
    BtrfsRoot,
    Desktop,
    IntelCpu,
    Linux,
    MacOs,
    NvidiaGpu,
    Personal,
    RaspberryPi,
    Server,
    Work,
}

pub(crate) fn detect() -> Vec<Condition> {
    let mut detected = vec![];
    detected.extend(filesystem_conditions());
    detected.extend(formfactor_conditions());
    detected.extend(hardware_conditions());
    detected.extend(ownership_conditions());
    detected.extend(os_conditions());
    detected
}

fn filesystem_conditions() -> Vec<Condition> {
    let mut detected = vec![];
    let mounts = read_to_string(Path::new("/proc/self/mounts")).unwrap_or_else(|_| String::new());
    if mounts.contains(" btrfs ") {
        detected.push(Condition::Btrfs);
        if mounts.contains(" / btrfs ") {
            detected.push(Condition::BtrfsRoot);
        }
    }
    detected
}

fn formfactor_conditions() -> Vec<Condition> {
    if args().any(|a| &a == "server") {
        vec![Condition::Server]
    } else {
        // default to desktop
        vec![Condition::Desktop]
    }
}

fn hardware_conditions() -> Vec<Condition> {
    let mut detected = vec![];
    let cpuinfo = read_to_string(Path::new("/proc/cpuinfo")).unwrap_or_else(|_| String::new());
    if cpuinfo.contains("AuthenticAMD") {
        detected.push(Condition::AmdCpu);
    }
    if cpuinfo.contains("GenuineIntel") {
        detected.push(Condition::IntelCpu);
    }

    let cpuinfo = read_to_string(Path::new("/sys/firmware/devicetree/base/model"))
        .unwrap_or_else(|_| String::new());
    if cpuinfo.contains("Raspberry Pi") {
        detected.push(Condition::RaspberryPi);
    }

    if let Ok(output) = Command::new("lspci").stdin(Stdio::null()).output() {
        if output.status.success() {
            let stdout = String::from_utf8_lossy(&output.stdout);
            if stdout.contains(" VGA compatible controller: Advanced Micro Devices, Inc. ") {
                detected.push(Condition::AmdGpu);
            }
            if stdout.contains(" 3D controller: NVIDIA Corporation ")
                || stdout.contains(" VGA compatible controller: NVIDIA Corporation ")
            {
                detected.push(Condition::NvidiaGpu);
            }
        }
    }

    detected
}

fn ownership_conditions() -> Vec<Condition> {
    if args().any(|a| &a == "work") {
        vec![Condition::Work]
    } else {
        // default to personal
        vec![Condition::Personal]
    }
}

fn os_conditions() -> Vec<Condition> {
    let mut detected = vec![];

    if OS == "linux" {
        detected.push(Condition::Linux);
        let os_release =
            read_to_string(Path::new("/etc/os-release")).unwrap_or_else(|_| String::new());
        if os_release.contains("\nID=arch\n") {
            detected.push(Condition::ArchLinux);
        }
    }

    if OS == "macos" {
        detected.push(Condition::MacOs);
    }

    detected
}
