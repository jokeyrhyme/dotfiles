use std::{
    collections::HashMap,
    env::{current_dir, var},
    fs::read_to_string,
    path::Path,
};

use serde::Deserialize;

use crate::repository::Repository;

/// mapping of package manager/repository to spec
pub(crate) type PackageSpec = HashMap<Repository, RepositorySpec>;

#[derive(Clone, Debug, Default, Deserialize, Eq, PartialEq)]
pub(crate) struct PackagesDoc {
    pub repositories: RepositoriesSpec,
    /// key = package identifier
    #[serde(rename = "package")]
    pub packages: HashMap<String, PackageSpec>,
}

#[derive(Clone, Debug, Default, Deserialize, Eq, Hash, PartialEq)]
pub(crate) struct RepositoriesSpec {
    /// allow-list of repositories, and sorted in order of most-preferred to least-preferred
    pub enabled: Vec<Repository>,
}

#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq)]
#[serde(rename_all = "kebab-case", untagged)]
pub(crate) enum RepositorySpec {
    Enabled(bool),
    /// repository lists this package by a different name
    Rename(String),
}

pub(crate) fn parse_packages_toml(text: &str) -> PackagesDoc {
    toml::from_str(text).expect("should parse packages.toml")
}

pub(crate) fn read_packages_file() -> String {
    let home_dir = var("HOME").expect("HOME environment variable should be set");
    let mut file_path = Path::new(&home_dir)
        .join(".dotfiles")
        .join("config")
        .join("packages.toml");
    if option_env!("CI").is_some() && !file_path.exists() {
        file_path = current_dir()
            .expect("current working directory should be valid")
            .join("..")
            .join("..")
            .join("config")
            .join("packages.toml");
    }

    eprintln!("reading: {}", file_path.display());
    read_to_string(file_path)
        .expect("packages.toml should exist, permit reads, and have UTF-8 contents")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn packages_toml_parse_ok() {
        let text = read_packages_file();
        parse_packages_toml(&text);
    }
}
