use std::{
    fs::read_to_string,
    path::Path,
    process::{Command, Stdio},
};

use crate::packages::RepositorySpec;

use super::{Action, Installer, InstallerInfo, LocalIndexer};

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub(crate) struct Pacman {
    groups: Vec<String>,
    index: Vec<String>,
    present_explicit: Vec<String>,
    present_deps: Vec<String>,
}

impl LocalIndexer for Pacman {
    fn contains(&self, package: &str) -> bool {
        self.index.binary_search(&String::from(package)).is_ok()
    }

    fn update(&mut self) {
        // TODO is there value in splitting our Pacman into PacmanCore and PacmanExtra?
        self.index = ["core", "extra"]
            .iter()
            .flat_map(|repo| {
                let output = Command::new("pacman")
                    .arg("-S")
                    .arg("--list")
                    .arg(repo)
                    .stdin(Stdio::null())
                    .output()
                    .expect("should execute `pacman -S --list ...`");
                if output.status.success() {
                    // e.g. "repo package-name unknown-version"
                    String::from_utf8_lossy(&output.stdout)
                        .lines()
                        .filter(|line| line.starts_with(&format!("{repo} ")))
                        .map(|line| {
                            let mut parts = line.split_ascii_whitespace();
                            parts
                                .next()
                                .expect("`pacman ...` output should look like 'repo package...'");
                            parts.next().expect(
                                "`pacman ...` output should look like 'repo package version'",
                            )
                        })
                        .map(String::from)
                        .collect::<Vec<String>>()
                } else {
                    Vec::<String>::new()
                }
            })
            .collect();

        let output = Command::new("pacman")
            .arg("-S")
            .arg("--groups")
            .stdin(Stdio::null())
            .output()
            .expect("should execute `pacman -S --groups ...`");
        if output.status.success() {
            self.index.extend(
                String::from_utf8_lossy(&output.stdout)
                    .lines()
                    .map(String::from),
            );
        }

        self.index.sort_unstable();
    }
}

impl Installer for Pacman {
    fn plan(&self, add: &[String]) -> Vec<Action> {
        let add_from_groups: Vec<_> = add
            .iter()
            .filter(|n| self.groups.contains(n))
            .flat_map(|n| expand_group(n))
            .collect();

        let mut actions = vec![];

        // find packages that need to be marked as dependencies instead of as explicitly installed
        let need_asdeps: Vec<_> = self
            .present_explicit
            .iter()
            .filter(|n| !add.contains(n) && !add_from_groups.contains(n))
            .map(String::as_str)
            .collect();
        if !need_asdeps.is_empty() {
            actions.push(Action::Command(format!(
                "sudo pacman -D --asdeps {}",
                need_asdeps.join(" "),
            )));
        }

        // find packages that need to be explicitly installed
        let need_explicit: Vec<_> = add
            .iter()
            .filter(|n| {
                !((self.groups.contains(n)
                    && expand_group(n)
                        .iter()
                        .all(|n| self.present_explicit.contains(n)))
                    || self.present_explicit.contains(n))
            })
            .map(String::as_str)
            .collect();
        if !need_explicit.is_empty() {
            actions.push(Action::Command(format!(
                "sudo pacman -S --asexplicit {}",
                need_explicit.join(" "),
            )));
        }

        actions
    }

    fn detect(&mut self) {
        let output = Command::new("pacman")
            .arg("-S")
            .arg("--group")
            .arg("--quiet")
            .stdin(Stdio::null())
            .output()
            .expect("should execute `pacman -S --group --quiet`");
        if output.status.success() {
            self.groups = String::from_utf8_lossy(&output.stdout)
                .lines()
                .map(str::trim)
                .map(String::from)
                .collect();
        }

        let output = Command::new("pacman")
            .arg("-Q")
            .arg("--explicit")
            .arg("--native")
            .arg("--quiet")
            .stdin(Stdio::null())
            .output()
            .expect("should execute `pacman -Q --explicit --native --quiet`");
        if output.status.success() {
            self.present_explicit = String::from_utf8_lossy(&output.stdout)
                .lines()
                .map(str::trim)
                .map(String::from)
                .collect();
        }

        let output = Command::new("pacman")
            .arg("-Q")
            .arg("--deps")
            .arg("--native")
            .arg("--quiet")
            .stdin(Stdio::null())
            .output()
            .expect("should execute `pacman -Q --deps --native --quiet`");
        if output.status.success() {
            self.present_deps = String::from_utf8_lossy(&output.stdout)
                .lines()
                .map(str::trim)
                .map(String::from)
                .collect();
        }
    }
}

impl InstallerInfo for Pacman {
    fn default_spec() -> RepositorySpec {
        // pacman has a local package index
        RepositorySpec::Enabled(true)
    }

    fn exists() -> bool {
        let os_release =
            read_to_string(Path::new("/etc/os-release")).unwrap_or_else(|_| String::new());
        os_release.contains("\nID=arch\n") && which::which("pacman").is_ok()
    }
}

fn expand_group(name: &str) -> Vec<String> {
    let output = Command::new("pacman")
        .arg("-S")
        .arg("--group")
        .arg("--quiet")
        .arg(name)
        .stdin(Stdio::null())
        .output()
        .expect("should execute `pacman -S --group --quiet`");
    if output.status.success() {
        String::from_utf8_lossy(&output.stdout)
            .lines()
            .map(str::trim)
            .map(String::from)
            .collect()
    } else {
        vec![]
    }
}
