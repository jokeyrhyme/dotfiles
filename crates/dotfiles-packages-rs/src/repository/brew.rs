use std::process::{Command, Stdio};

use crate::packages::RepositorySpec;

use super::{Action, Installer, InstallerInfo, LocalIndexer};

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub(crate) struct Brew {
    index: Vec<String>,
}

impl LocalIndexer for Brew {
    fn contains(&self, package: &str) -> bool {
        self.index.binary_search(&String::from(package)).is_ok()
    }

    fn update(&mut self) {
        let output = Command::new("brew")
            .args(["search", "--formula", "''"])
            .stdin(Stdio::null())
            .output()
            .expect("should execute `brew search --formula ''`");
        if output.status.success() {
            // e.g. "aur package-name unknown-version"
            self.index = String::from_utf8_lossy(&output.stdout)
                .lines()
                .map(String::from)
                .collect();
        }
        self.index.sort_unstable();
    }
}

impl Installer for Brew {
    fn detect(&mut self) {
        // noop: no need to do anything here,
        // `brew bundle --force cleanup` will do everything we need
    }

    fn plan(&self, add: &[String]) -> Vec<Action> {
        let home_dir = dirs::home_dir().expect("should locate user's HOME");
        let file_path = home_dir.join(".dotfiles").join("Brewfile");
        add.iter()
            .map(|pkg| Action::LineInFile(file_path.clone(), format!(r#"brew "{pkg}""#)))
            .collect()
    }
}

impl InstallerInfo for Brew {
    fn default_spec() -> RepositorySpec {
        // homebrew has a local package index
        RepositorySpec::Enabled(true)
    }

    fn exists() -> bool {
        // homebrew supports macOS and Linux
        which::which("brew").is_ok()
    }
}
