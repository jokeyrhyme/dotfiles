use crate::packages::RepositorySpec;

use super::{flathub::Flathub, InstallerInfo};

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub(crate) struct FlathubBeta;

impl InstallerInfo for FlathubBeta {
    fn default_spec() -> RepositorySpec {
        Flathub::default_spec()
    }

    fn exists() -> bool {
        Flathub::exists()
    }
}
