use std::{
    collections::HashMap,
    process::{Command, Stdio},
};

use serde::Deserialize;

use crate::packages::RepositorySpec;

use super::{Action, Installer, InstallerInfo};

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub(crate) struct Pipx {
    present: Vec<String>,
}

impl Installer for Pipx {
    fn detect(&mut self) {
        let output = Command::new("pipx")
            .args(["list", "--json"])
            .stdin(Stdio::null())
            .output()
            .expect("should execute `pipx list --json`");
        if output.status.success() {
            let pipx_list: PipxList = serde_json::from_slice(&output.stdout)
                .expect("should parse JSON from `pipx list --json`");
            self.present = pipx_list
                .venvs
                .into_values()
                .map(|venv| venv.metadata.main_package.package)
                .filter(|pkg| !pkg.ends_with("-vpn"))
                // TODO support packages with custom remotes
                .collect();
        }
    }

    fn plan(&self, add: &[String]) -> Vec<Action> {
        let mut actions = vec![];

        // find packages that need to be uninstalled
        let unexpected_apps: Vec<_> = self
            .present
            .iter()
            .filter(|n| !add.contains(n))
            .cloned()
            .collect();
        for pkg in unexpected_apps {
            actions.push(Action::Command(format!("pipx uninstall {pkg}")));
        }

        // find packages that need to be installed
        let need_install: Vec<_> = add
            .iter()
            .filter(|n| !self.present.contains(n))
            .map(String::as_str)
            .collect();
        if !need_install.is_empty() {
            actions.push(Action::Command(format!(
                "pipx install {}",
                need_install.join(" "),
            )));
        }

        actions
    }
}

impl InstallerInfo for Pipx {
    fn default_spec() -> RepositorySpec {
        // TODO determine if there's a local package index for `pip` or `pipx`
        RepositorySpec::Enabled(false)
    }

    fn exists() -> bool {
        which::which("pipx").is_ok()
    }
}

#[derive(Deserialize)]
struct PipxList {
    venvs: HashMap<String, Venv>,
}

#[derive(Deserialize)]
struct Venv {
    metadata: VenvMetadata,
}

#[derive(Deserialize)]
struct VenvMainPackage {
    package: String,
}

#[derive(Deserialize)]
struct VenvMetadata {
    main_package: VenvMainPackage,
}
