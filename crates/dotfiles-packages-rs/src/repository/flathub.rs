use std::process::{Command, Stdio};

use crate::packages::RepositorySpec;

use super::{Action, Installer, InstallerInfo};

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub(crate) struct Flathub {
    present_apps: Vec<String>,
    remotes: Vec<String>,
}

impl Installer for Flathub {
    fn detect(&mut self) {
        if let Ok(output) = Command::new("flatpak")
            .args(["list", "--app", "--columns", "application", "--system"])
            .stdin(Stdio::null())
            .output()
        {
            if output.status.success() {
                let stdout = String::from_utf8_lossy(&output.stdout);
                self.present_apps = stdout
                    .lines()
                    .map(str::trim)
                    .filter(|line| !line.is_empty())
                    .map(String::from)
                    .collect();
            }
        }

        // assumption: names consistently map to expected repositories across all systems
        if let Ok(output) = Command::new("flatpak")
            .args(["remote-list", "--columns", "name", "--system"])
            .stdin(Stdio::null())
            .output()
        {
            if output.status.success() {
                let stdout = String::from_utf8_lossy(&output.stdout);
                self.remotes = stdout
                    .lines()
                    .map(str::trim)
                    .filter(|line| !line.is_empty())
                    .map(String::from)
                    .collect();
            }
        }
    }

    fn plan(&self, add: &[String]) -> Vec<Action> {
        let mut actions = vec![];

        // find packages that need to be uninstalled
        let unexpected_apps: Vec<_> = self
            .present_apps
            .iter()
            .filter(|n| !add.contains(n))
            .cloned()
            .collect();
        if !unexpected_apps.is_empty() {
            actions.push(Action::Command(format!(
                "flatpak uninstall --assumeyes --delete-data --force-remove --system {}",
                unexpected_apps.join(" "),
            )));
        }

        // find packages that need to be installed
        let need_install: Vec<_> = add
            .iter()
            .filter(|n| !self.present_apps.contains(n))
            .map(String::as_str)
            .collect();
        if !need_install.is_empty() {
            // TODO: support installation of runtime extension (these aren't `--app`)
            // TODO: support specification of remotes that are not "flathub"
            actions.push(Action::Command(format!(
                "flatpak install --app --assumeyes --system flathub {}",
                need_install.join(" "),
            )));
        }

        actions
    }
}

impl InstallerInfo for Flathub {
    fn default_spec() -> RepositorySpec {
        // TODO determine if there's a local package index for `flatpak`
        RepositorySpec::Enabled(false)
    }

    fn exists() -> bool {
        which::which("flatpak").is_ok()
    }
}
