use std::process::{Command, Stdio};

use chumsky::{prelude::*, text::newline};

use crate::packages::RepositorySpec;

use super::{Action, Installer, InstallerInfo};

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub(crate) struct Cargo {
    present: Vec<InstalledCrate>,
}

impl Installer for Cargo {
    fn detect(&mut self) {
        self.present = cargo_install_list_parse(&cargo_install_list())
            .into_iter()
            .filter(|ic| ic.custom_remote.is_none())
            .collect();
        // TODO support packages with custom remotes
    }

    fn plan(&self, add: &[String]) -> Vec<Action> {
        let mut actions = vec![];

        // find packages that need to be uninstalled
        let unexpected_apps: Vec<String> = self
            .present
            .iter()
            .filter_map(|n| {
                if add.contains(&n.name) {
                    None
                } else {
                    Some(n.name.clone())
                }
            })
            .collect();
        if !unexpected_apps.is_empty() {
            actions.push(Action::Command(format!(
                "cargo uninstall {}",
                unexpected_apps.join(" "),
            )));
        }

        // find packages that need to be installed
        let need_install: Vec<_> = add
            .iter()
            .filter(|n| !self.present.iter().any(|ic| &&ic.name == n))
            .map(String::as_str)
            .collect();
        if !need_install.is_empty() {
            actions.push(Action::Command(format!(
                "cargo install --locked {}",
                need_install.join(" "),
            )));
        }

        actions
    }
}

impl InstallerInfo for Cargo {
    fn default_spec() -> RepositorySpec {
        // TODO determine if there's a local package index for `cargo`
        // maybe https://doc.rust-lang.org/cargo/reference/registry-index.html ?
        RepositorySpec::Enabled(false)
    }

    fn exists() -> bool {
        which::which("cargo").is_ok()
    }
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
struct InstalledCrate {
    bins: Vec<String>,
    custom_remote: Option<String>,
    name: String,
    version: String,
}

fn cargo_install_list() -> String {
    let output = Command::new("cargo")
        .args(["install", "--list"])
        .stdin(Stdio::null())
        .output()
        .expect("should execute `cargo install --list`");
    assert!(
        output.status.success(),
        "`cargo install --list` should succeed"
    );
    String::from_utf8_lossy(&output.stdout).into_owned()
}

#[derive(Clone, Debug, Eq, PartialEq)]
enum Expr {
    Bin(String),
    CustomRemote(String),
    Name(String),
    Version(String),
}

fn cargo_install_list_parse(input: &str) -> Vec<InstalledCrate> {
    let bin =
        filter::<_, _, Simple<char>>(|c| c == &'-' || c == &'_' || char::is_ascii_alphanumeric(c))
            .repeated()
            .at_least(1)
            .collect::<String>()
            .map(Expr::Bin);
    let custom_remote = filter::<_, _, Simple<char>>(|c: &char| {
        c != &'(' && c != &')' && c.is_ascii() && !c.is_ascii_whitespace()
    })
    .repeated()
    .at_least(1)
    .collect::<String>()
    .map(Expr::CustomRemote)
    .delimited_by(just('('), just(')'));
    let name =
        filter::<_, _, Simple<char>>(|c| c == &'-' || c == &'_' || char::is_ascii_alphanumeric(c))
            .repeated()
            .at_least(1)
            .collect::<String>()
            .map(Expr::Name);
    let version = just::<_, _, Simple<char>>('v')
        .chain(
            filter::<_, _, Simple<char>>(|c: &char| {
                c == &'.' || c == &'-' || c.is_ascii_alphanumeric()
            })
            .repeated()
            .at_least(1),
        )
        .collect::<String>()
        .map(Expr::Version);

    let crate_line = name
        .then_ignore(just(' '))
        .then(version)
        .then(just(' ').ignored().then(custom_remote).or_not())
        .then_ignore(just(':'))
        .then(newline());
    let bin_line = just(' ').repeated().exactly(4).ignored().then(bin);
    let bins_list = bin_line.separated_by(newline());

    let krate = crate_line.then(bins_list);

    let krates = krate.separated_by(newline()).then_ignore(end());
    krates
        .parse(input.trim())
        .expect("should parse `cargo install --list` output")
        .into_iter()
        .filter_map(|ast| match ast {
            ((((Expr::Name(n), Expr::Version(v)), custom_remote_tuple), ()), bin_tuples) => {
                Some(InstalledCrate {
                    bins: bin_tuples
                        .into_iter()
                        .filter_map(|bin_tuple| match bin_tuple {
                            ((), Expr::Bin(b)) => Some(b),
                            _ => None,
                        })
                        .collect(),
                    custom_remote: if let Some(((), Expr::CustomRemote(custom_remote))) =
                        custom_remote_tuple
                    {
                        Some(custom_remote)
                    } else {
                        None
                    },
                    name: n,
                    version: v,
                })
            }
            _ => None,
        })
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn cargo_install_list_parse_custom_remote_ok() {
        let input = "
paru v2.0.3 (https://github.com/Morganamilo/paru.git#5355012a):
    paru
";
        let want = vec![InstalledCrate {
            bins: vec![String::from("paru")],
            custom_remote: Some(String::from(
                "https://github.com/Morganamilo/paru.git#5355012a",
            )),
            name: String::from("paru"),
            version: String::from("v2.0.3"),
        }];
        let got = cargo_install_list_parse(input);
        assert_eq!(got, want);
    }

    #[test]
    fn cargo_install_list_parse_simple_ok() {
        let input = "
cross v0.2.5:
    cross
";
        let want = vec![InstalledCrate {
            bins: vec![String::from("cross")],
            custom_remote: None,
            name: String::from("cross"),
            version: String::from("v0.2.5"),
        }];
        let got = cargo_install_list_parse(input);
        assert_eq!(got, want);
    }

    #[test]
    fn cargo_install_list_parse_simple_with_bins_ok() {
        let input = "
cross v0.2.5:
    cross
    cross-util
";
        let want = vec![InstalledCrate {
            bins: vec![String::from("cross"), String::from("cross-util")],
            custom_remote: None,
            name: String::from("cross"),
            version: String::from("v0.2.5"),
        }];
        let got = cargo_install_list_parse(input);
        assert_eq!(got, want);
    }

    #[test]
    fn cargo_install_list_parse_multiple_crates_ok() {
        let input = "
cross v0.2.5:
    cross
    cross-util
helix-term v24.7.0 (/home/user/GitHub/helix-editor/helix/helix-term):
    hx
paru v2.0.3 (https://github.com/Morganamilo/paru.git#5355012a):
    paru
";
        let want = vec![
            InstalledCrate {
                bins: vec![String::from("cross"), String::from("cross-util")],
                custom_remote: None,
                name: String::from("cross"),
                version: String::from("v0.2.5"),
            },
            InstalledCrate {
                bins: vec![String::from("hx")],
                custom_remote: Some(String::from(
                    "/home/user/GitHub/helix-editor/helix/helix-term",
                )),
                name: String::from("helix-term"),
                version: String::from("v24.7.0"),
            },
            InstalledCrate {
                bins: vec![String::from("paru")],
                custom_remote: Some(String::from(
                    "https://github.com/Morganamilo/paru.git#5355012a",
                )),
                name: String::from("paru"),
                version: String::from("v2.0.3"),
            },
        ];
        let got = cargo_install_list_parse(input);
        assert_eq!(got, want);
    }
}
