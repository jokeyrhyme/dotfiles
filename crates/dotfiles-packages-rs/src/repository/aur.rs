use std::{
    fs::read_to_string,
    path::Path,
    process::{Command, Stdio},
};

use crate::packages::RepositorySpec;

use super::{Action, Installer, InstallerInfo, LocalIndexer};

// we're assuming that every "foreign" package came from AUR

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub(crate) struct Aur {
    index: Vec<String>,
    present_deps: Vec<String>,
    present_explicit: Vec<String>,
}

impl LocalIndexer for Aur {
    fn contains(&self, package: &str) -> bool {
        self.index.binary_search(&String::from(package)).is_ok()
    }

    fn update(&mut self) {
        let body = reqwest::blocking::get("https://aur.archlinux.org/packages.gz")
            .expect("should connect to aur.archlinux.org")
            .text()
            .expect("response body should be valid UTF-8");
        self.index = body.split_ascii_whitespace().map(String::from).collect();
        self.index.sort_unstable();
    }
}

impl Installer for Aur {
    fn plan(&self, add: &[String]) -> Vec<Action> {
        let mut actions = vec![];

        // find packages that need to be marked as dependencies instead of as explicitly installed
        let need_asdeps: Vec<_> = self
            .present_explicit
            .iter()
            .filter(|n| !add.contains(n))
            .map(String::as_str)
            .collect();
        if !need_asdeps.is_empty() {
            actions.push(Action::Command(format!(
                "aura -D --asdeps {}",
                need_asdeps.join(" "),
            )));
        }

        // find packages that need to be explicitly installed
        let need_explicit: Vec<_> = add
            .iter()
            .filter(|n| !self.present_explicit.contains(n))
            .map(String::as_str)
            .collect();
        if !need_explicit.is_empty() {
            actions.push(Action::Command(format!(
                // `|| true`: we don't want AUR package issues to block the rest of the work
                "aura -A {} || true",
                need_explicit.join(" "),
            )));
        }

        actions
    }

    fn detect(&mut self) {
        let output = Command::new("aura")
            .arg("-Q")
            .arg("--deps")
            .arg("--foreign")
            .arg("--quiet")
            .stdin(Stdio::null())
            .output()
            .expect("should execute `aura -Q --deps --foreign --quiet`");
        if output.status.success() {
            self.present_deps = String::from_utf8_lossy(&output.stdout)
                .lines()
                .map(str::trim)
                .map(String::from)
                .collect();
        }

        let output = Command::new("aura")
            .arg("-Q")
            .arg("--explicit")
            .arg("--foreign")
            .arg("--quiet")
            .stdin(Stdio::null())
            .output()
            .expect("should execute `aura -Q --explicit --foreign --quiet`");
        if output.status.success() {
            self.present_explicit = String::from_utf8_lossy(&output.stdout)
                .lines()
                .map(str::trim)
                .map(String::from)
                .collect();
        }
    }
}

impl InstallerInfo for Aur {
    fn default_spec() -> RepositorySpec {
        // aura/AUR has a local package index
        RepositorySpec::Enabled(true)
    }

    fn exists() -> bool {
        let os_release =
            read_to_string(Path::new("/etc/os-release")).unwrap_or_else(|_| String::new());
        os_release.contains("\nID=arch\n") && which::which("aura").is_ok()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn aura_found_in_aur_packages_index() {
        let mut aur = Aur::default();
        aur.update();
        assert!(aur.contains("aura"));
        // https://aur.archlinux.org/packages/aura
    }
}
