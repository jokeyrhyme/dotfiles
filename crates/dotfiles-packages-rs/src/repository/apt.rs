use std::{
    fs::read_to_string,
    path::Path,
    process::{Command, Stdio},
};

use crate::packages::RepositorySpec;

use super::{Action, Installer, InstallerInfo, LocalIndexer};

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub(crate) struct Apt {
    index: Vec<String>,
    present_auto: Vec<String>,
    present_manual: Vec<String>,
}

impl LocalIndexer for Apt {
    fn contains(&self, package: &str) -> bool {
        self.index.binary_search(&String::from(package)).is_ok()
    }

    fn update(&mut self) {
        // TODO find a way to ensure that `sudo apt-get update` has run first
        let output = Command::new("apt-cache")
            // `apt-cache dump` also outputs repository information,
            // which might be useful later
            .arg("dumpavail")
            .stdin(Stdio::null())
            .output()
            .expect("should execute `apt-cache dumpavail`");
        if output.status.success() {
            // e.g. "aur package-name unknown-version"
            self.index = String::from_utf8_lossy(&output.stdout)
                .lines()
                .filter(|line| line.starts_with("Package: "))
                .map(|line| {
                    let mut parts = line.split_ascii_whitespace();
                    parts
                        .next()
                        .expect("`apt-cache dumpavail` output should look like 'Package: ...'");
                    parts
                        .next()
                        .expect("`apt-cache dumpavail` output should look like 'Package: foo'")
                })
                .map(String::from)
                .collect();
        }
        self.index.sort_unstable();
    }
}

impl Installer for Apt {
    fn plan(&self, add: &[String]) -> Vec<Action> {
        let mut actions = vec![];

        // find packages that need to be marked as dependencies instead of explicitly installed
        let need_auto: Vec<_> = self
            .present_manual
            .iter()
            .filter(|n| !add.contains(n))
            .map(String::as_str)
            .collect();
        if !need_auto.is_empty() {
            actions.push(Action::Command(format!(
                "sudo apt-mark auto {}",
                need_auto.join(" "),
            )));
        }

        // find installed packages that need to be promoted from auto to manual
        let need_manual: Vec<_> = self
            .present_auto
            .iter()
            .filter(|n| add.contains(n))
            .map(String::as_str)
            .collect();
        if !need_manual.is_empty() {
            actions.push(Action::Command(format!(
                "sudo apt-mark manual {}",
                need_manual.join(" "),
            )));
        }

        // find packages that need to be installed
        let need_install: Vec<_> = add
            .iter()
            .filter(|n| !self.present_manual.contains(n) && !self.present_auto.contains(n))
            .map(String::as_str)
            .collect();
        if !need_install.is_empty() {
            actions.push(Action::Command(format!(
                "sudo apt-get install --yes {}",
                need_install.join(" "),
            )));
        }

        actions
    }

    fn detect(&mut self) {
        let output = Command::new("apt-mark")
            .arg("showauto")
            .stdin(Stdio::null())
            .output()
            .expect("should execute `apt-mark showauto`");
        if output.status.success() {
            self.present_auto = String::from_utf8_lossy(&output.stdout)
                .lines()
                .map(str::trim)
                .map(String::from)
                .collect();
        }

        let output = Command::new("apt-mark")
            .arg("showmanual")
            .stdin(Stdio::null())
            .output()
            .expect("should execute `apt-mark showmanual`");
        if output.status.success() {
            self.present_manual = String::from_utf8_lossy(&output.stdout)
                .lines()
                .map(str::trim)
                .map(String::from)
                .collect();
        }
    }
}

impl InstallerInfo for Apt {
    fn default_spec() -> RepositorySpec {
        // apt has a local package index
        RepositorySpec::Enabled(true)
    }

    fn exists() -> bool {
        let os_release =
            read_to_string(Path::new("/etc/os-release")).unwrap_or_else(|_| String::new());
        (os_release.contains("\nID=debian\n") || os_release.contains("\nID_LIKE=debian\n"))
            && which::which("apt").is_ok()
            && which::which("apt-cache").is_ok()
    }
}
