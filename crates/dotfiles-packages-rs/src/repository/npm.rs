use std::{
    collections::HashMap,
    process::{Command, Stdio},
};

use serde::Deserialize;

use crate::packages::RepositorySpec;

use super::{Action, Installer, InstallerInfo};

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub(crate) struct Npm {
    present: Vec<String>,
}

impl Installer for Npm {
    fn detect(&mut self) {
        let output = Command::new("npm")
            .args(["--global", "ls", "--depth=0", "--json"])
            .stdin(Stdio::null())
            .output()
            .expect("should execute `npm --global ls --depth=0 --json`");
        if output.status.success() {
            self.present = serde_json::from_slice::<NpmLs>(&output.stdout)
                .expect("should parse JSON from `npm ls ...`")
                .dependencies
                .into_keys()
                .collect();
        }
    }

    fn plan(&self, add: &[String]) -> Vec<Action> {
        let mut actions = vec![];

        // find packages that need to be uninstalled
        let unexpected_apps: Vec<_> = self
            .present
            .iter()
            .filter(|n| !add.contains(n))
            .cloned()
            .collect();
        if !unexpected_apps.is_empty() {
            actions.push(Action::Command(format!(
                "npm --global uninstall {}",
                unexpected_apps.join(" "),
            )));
        }

        // find packages that need to be installed
        let need_install: Vec<_> = add
            .iter()
            .filter(|n| !self.present.contains(n))
            .map(String::as_str)
            .collect();
        if !need_install.is_empty() {
            actions.push(Action::Command(format!(
                "npm --global install {}",
                need_install.join(" "),
            )));
        }

        actions
    }
}

impl InstallerInfo for Npm {
    fn default_spec() -> RepositorySpec {
        // TODO determine if there's a local package index for `npm`
        RepositorySpec::Enabled(false)
    }

    fn exists() -> bool {
        which::which("npm").is_ok()
    }
}

#[derive(Debug, Deserialize)]
struct Dependency {
    #[allow(dead_code)]
    #[serde(default)]
    version: String,
}

#[derive(Debug, Deserialize)]
struct NpmLs {
    #[serde(default)]
    dependencies: HashMap<String, Dependency>,
}
