use std::{
    env::consts::OS,
    process::{Command, Stdio},
};

use crate::packages::RepositorySpec;

use super::{brew::Brew, Action, Installer, InstallerInfo, LocalIndexer};

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub(crate) struct BrewCask {
    index: Vec<String>,
}

impl LocalIndexer for BrewCask {
    fn contains(&self, package: &str) -> bool {
        self.index.binary_search(&String::from(package)).is_ok()
    }

    fn update(&mut self) {
        let output = Command::new("brew")
            .args(["search", "--cask", "''"])
            .stdin(Stdio::null())
            .output()
            .expect("should execute `brew search --cask''`");
        if output.status.success() {
            // e.g. "aur package-name unknown-version"
            self.index = String::from_utf8_lossy(&output.stdout)
                .lines()
                .map(String::from)
                .collect();
        }
        self.index.sort_unstable();
    }
}

impl Installer for BrewCask {
    fn detect(&mut self) {
        // noop: no need to do anything here,
        // `brew bundle --force cleanup` will do everything we need
    }

    fn plan(&self, add: &[String]) -> Vec<Action> {
        let home_dir = dirs::home_dir().expect("should locate user's HOME");
        let file_path = home_dir.join(".dotfiles").join("Brewfile");
        add.iter()
            .map(|pkg| Action::LineInFile(file_path.clone(), format!(r#"cask "{pkg}""#)))
            .collect()
    }
}

impl InstallerInfo for BrewCask {
    fn default_spec() -> RepositorySpec {
        Brew::default_spec()
    }

    fn exists() -> bool {
        // homebrew casks only support macOS
        OS == "macos" && Brew::exists()
    }
}
