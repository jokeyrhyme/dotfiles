use std::{
    env::{current_dir, var},
    fs::read_to_string,
    path::Path,
};

use serde::Deserialize;

use crate::condition::Condition;

#[derive(Clone, Debug, Default, Deserialize, Eq, Hash, PartialEq)]
pub(crate) struct Group {
    pub description: Option<String>,
    #[serde(rename = "package")]
    pub packages: Vec<String>,
    #[serde(default)]
    pub when: Vec<Condition>,
}
impl Group {
    pub fn matches_conditions(&self, conditions: &[Condition]) -> bool {
        self.when.is_empty() || self.when.iter().all(|w| conditions.contains(w))
    }
}

#[derive(Clone, Debug, Default, Deserialize, Eq, Hash, PartialEq)]
pub(crate) struct RequirementsDoc {
    #[serde(rename = "group")]
    pub groups: Vec<Group>,
}

pub(crate) fn parse_requirements_toml(text: &str) -> RequirementsDoc {
    toml::from_str(text).expect("should parse requirements.toml")
}

pub(crate) fn read_requirements_file() -> String {
    let home_dir = var("HOME").expect("HOME environment variable should be set");
    let mut file_path = Path::new(&home_dir)
        .join(".dotfiles")
        .join("config")
        .join("requirements.toml");
    if option_env!("CI").is_some() && !file_path.exists() {
        file_path = current_dir()
            .expect("current working directory should be valid")
            .join("..")
            .join("..")
            .join("config")
            .join("requirements.toml");
    }

    eprintln!("reading: {}", file_path.display());
    read_to_string(file_path)
        .expect("requirements.toml should exist, permit reads, and have UTF-8 contents")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn group_matches_conditions_ok() {
        assert!(Group {
            when: vec![],
            ..Default::default()
        }
        .matches_conditions(&[Condition::Desktop]));

        assert!(Group {
            when: vec![Condition::Desktop],
            ..Default::default()
        }
        .matches_conditions(&[Condition::Desktop]));

        assert!(!Group {
            when: vec![Condition::IntelCpu],
            ..Default::default()
        }
        .matches_conditions(&[Condition::Desktop]));

        assert!(!Group {
            when: vec![Condition::Desktop, Condition::IntelCpu],
            ..Default::default()
        }
        .matches_conditions(&[Condition::Desktop]));

        assert!(Group {
            when: vec![Condition::Desktop, Condition::IntelCpu],
            ..Default::default()
        }
        .matches_conditions(&[Condition::Desktop, Condition::IntelCpu, Condition::AmdGpu]));
    }

    #[test]
    fn requirements_toml_parse_ok() {
        let text = read_requirements_file();
        parse_requirements_toml(&text);
    }
}
