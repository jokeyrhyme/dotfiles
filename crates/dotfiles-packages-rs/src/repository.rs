pub(crate) mod apt;
pub(crate) mod aur;
pub(crate) mod brew;
pub(crate) mod brew_cask;
pub(crate) mod cargo;
pub(crate) mod flathub;
pub(crate) mod flathub_beta;
pub(crate) mod npm;
pub(crate) mod pacman;
pub(crate) mod pipx;

use std::path::PathBuf;

use serde::Deserialize;

use crate::packages::RepositorySpec;

/// installers should read system state, but MUST not action any changes themselves
pub(crate) trait Installer {
    /// build internal representation of current system state
    fn detect(&mut self);

    /// compare inputs to current system state and generate convergence plan
    fn plan(&self, add: &[String]) -> Vec<Action>;

    // reinstall all packages whilst preserving their direct/indirect state
    // TODO fn repair(&self);
}

pub(crate) trait InstallerInfo {
    /// used when the package entry in packages.toml does not explicitly mention this repository
    // TODO can we make this automatic depending on if LocalIndexer is implemented?
    // TODO what other criteria could we use here? trust/security?
    fn default_spec() -> RepositorySpec;

    /// the current system seems to have this repository
    // doesn't really use `self`, but needed for object-safety
    fn exists() -> bool;
}

pub(crate) trait LocalIndexer {
    /// we can find the given package name within the index (call `update()` first)
    fn contains(&self, package: &str) -> bool;

    /// retrieve or otherwise prepare an index of available packages via an expensive one-off operation
    fn update(&mut self);
}

// TODO properly separate installer and repository concepts for clarity
/// we conflate the concepts of installer and non-default configurations/repositories
#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Repository {
    Apt,
    Aur,
    Brew,
    BrewCask,
    Cargo,
    Flathub,
    FlathubBeta,
    Npm,
    Pacman,
    Pipx,
}
// TODO impl LocalIndex for Apt, Aur, Brew, BrewCask, Pacman
// LocalIndex: (after updating catalog) inexpensive to search a repository for a package
// !LocalIndex: expensive/slow to search a repository for a package
impl Repository {
    pub(crate) fn default_spec(&self) -> RepositorySpec {
        match self {
            Repository::Apt => apt::Apt::default_spec(),
            Repository::Aur => aur::Aur::default_spec(),
            Repository::Brew => brew::Brew::default_spec(),
            Repository::BrewCask => brew_cask::BrewCask::default_spec(),
            Repository::Cargo => cargo::Cargo::default_spec(),
            Repository::Flathub => flathub::Flathub::default_spec(),
            Repository::FlathubBeta => flathub_beta::FlathubBeta::default_spec(),
            Repository::Npm => npm::Npm::default_spec(),
            Repository::Pacman => pacman::Pacman::default_spec(),
            Repository::Pipx => pipx::Pipx::default_spec(),
        }
    }

    pub(crate) fn exists(&self) -> bool {
        match self {
            Repository::Apt => apt::Apt::exists(),
            Repository::Aur => aur::Aur::exists(),
            Repository::Brew => brew::Brew::exists(),
            Repository::BrewCask => brew_cask::BrewCask::exists(),
            Repository::Cargo => cargo::Cargo::exists(),
            Repository::Flathub => flathub::Flathub::exists(),
            Repository::FlathubBeta => flathub_beta::FlathubBeta::exists(),
            Repository::Npm => npm::Npm::exists(),
            Repository::Pacman => pacman::Pacman::exists(),
            Repository::Pipx => pipx::Pipx::exists(),
        }
    }

    pub(crate) fn indexer(&self) -> Option<Box<dyn LocalIndexer>> {
        // TODO add more indexers as we implement them
        match self {
            Repository::Apt => Some(Box::<apt::Apt>::default()),
            Repository::Aur => Some(Box::<aur::Aur>::default()),
            Repository::Brew => Some(Box::<brew::Brew>::default()),
            Repository::BrewCask => Some(Box::<brew_cask::BrewCask>::default()),
            Repository::Pacman => Some(Box::<pacman::Pacman>::default()),
            Repository::Cargo
            | Repository::Flathub
            | Repository::FlathubBeta
            | Repository::Npm
            | Repository::Pipx => None,
        }
    }

    pub(crate) fn repository(&self) -> Box<dyn Installer> {
        match self {
            Repository::Apt => Box::<apt::Apt>::default(),
            Repository::Aur => Box::<aur::Aur>::default(),
            Repository::Brew => Box::<brew::Brew>::default(),
            Repository::BrewCask => Box::<brew_cask::BrewCask>::default(),
            Repository::Cargo => Box::<cargo::Cargo>::default(),
            Repository::Flathub => Box::<flathub::Flathub>::default(),
            Repository::FlathubBeta => todo!(),
            Repository::Npm => Box::<npm::Npm>::default(),
            Repository::Pacman => Box::<pacman::Pacman>::default(),
            Repository::Pipx => Box::<pipx::Pipx>::default(),
        }
    }
}

pub(crate) enum Action {
    Command(String),
    LineInFile(PathBuf, String),
}
