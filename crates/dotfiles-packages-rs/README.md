# dotfiles-packages-rs

## what?

- consumes a list of installer/repository settings and mappings
  (e.g. "foo" is called "foo-tool" in `apt-get`) in a [TOML](https://toml.io) file
- consumes a list of groups of required packages,
  with conditions sensitive to current system context/state,
  also in TOML
- produces commands to evaluate in your shell, with the following goals:
  - match each package with the most-preferred installer/repository
  - install every package listed as though the user explicitly installed them
  - find any installed package that is not listed,
    marking them as though the user did not explicitly install them
    (i.e. installed only as a dependency of another package)

## why?

- declarative, kinda' like [Nix](https://nixos.org/),
  but allows me to keep using [Rust](https://www.rust-lang.org/) and [Arch Linux](https://archlinux.org/)
- it was desirable to achieve this using tools that are preinstalled by default across various Linux distributions,
  but it's not that difficult to install [`rustup`](https://rustup.rs/),
  and we get a much better developer experience this way
