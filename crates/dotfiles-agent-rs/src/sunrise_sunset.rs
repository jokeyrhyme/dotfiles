use std::process::Stdio;

use anyhow::Result;
use chrono::{DateTime, Days, Local};
use serde::Deserialize;
use tokio::{process::Command, sync::broadcast::Receiver};
use tracing::{error, info, instrument};
use winit::event_loop::EventLoopProxy;

use crate::{ColorScheme, Location, UserEvent};

#[derive(Clone, Deserialize, Debug, PartialEq)]
struct Results {
    sunrise: DateTime<Local>,
    sunset: DateTime<Local>,
}

#[derive(Clone, Deserialize, Debug, PartialEq)]
struct Response {
    results: Results,
}

#[derive(Clone, Deserialize, Debug, PartialEq)]
enum SolarEvent {
    Sunrise,
    Sunset,
}

#[instrument(skip_all)]
pub(crate) async fn sunrise_sunset_task(
    tx: EventLoopProxy<UserEvent>,
    mut rx: Receiver<UserEvent>,
) -> Result<()> {
    let mut location: Option<Location> = None;
    let mut resumed: bool = false;
    let mut timezone: Option<String> = None;

    loop {
        if let (Some(loc), Some(tz), true) = (&location, &timezone, resumed) {
            match lookup_next_solar_event(loc, tz).await {
                Ok((_, se)) => {
                    info!(solar_event = ?se, "determined next solar event");
                    match se {
                        SolarEvent::Sunrise => {
                            if let Err(err) =
                                tx.send_event(UserEvent::ColorScheme(ColorScheme::Dark))
                            {
                                error!(err = ?err, "should send to event loop");
                            }
                            let _ = set_gtk_color_scheme(&ColorScheme::Dark).await;
                        }
                        SolarEvent::Sunset => {
                            if let Err(err) =
                                tx.send_event(UserEvent::ColorScheme(ColorScheme::Light))
                            {
                                error!(err = ?err, "should send to event loop");
                            }
                            let _ = set_gtk_color_scheme(&ColorScheme::Light).await;
                        }
                    }
                }
                Err(err) => {
                    error!(err = ?err, "should lookup solar events");
                }
            }
        }
        if let Ok(event) = rx.recv().await {
            match event {
                UserEvent::ColorScheme(_) => { /* no-op */ }
                UserEvent::Location(loc) => {
                    location = Some(loc);
                }
                UserEvent::Timezone(tz) => {
                    timezone = Some(tz);
                }
                UserEvent::WindowResumed => {
                    resumed = true;
                }
                UserEvent::WindowExiting => {
                    info!("shutting down task ...");
                    break;
                }
            }
        }
    }
    Ok(())
}

#[instrument]
async fn lookup_next_solar_event(
    location: &Location,
    timezone: &str,
) -> Result<(DateTime<Local>, SolarEvent)> {
    info!("fetching ...");
    let client = reqwest::Client::new();
    let now = Local::now();

    let dates = &[
        now,
        now.checked_add_days(Days::new(1))
            .expect("should determine tomorrow's date"),
    ];

    let mut responses = vec![];
    for date in dates {
        responses.push(
            client
                .get(format!(
            "https://api.sunrise-sunset.org/json?date={}&formatted=0&lat={}&lng={}&tzid={timezone}",
            date.format("%Y-%m-%d"), location.latitude, location.longitude
        ))
                .send()
                .await?
                .json::<Response>()
                .await?,
        );
    }

    let solar_events: Vec<_> = responses
        .into_iter()
        .flat_map(|res| {
            vec![
                (res.results.sunrise, SolarEvent::Sunrise),
                (res.results.sunset, SolarEvent::Sunset),
            ]
        })
        .collect();

    let now = Local::now();
    Ok(solar_events
        .iter()
        .find(|(dt, _)| dt > &now)
        .expect("should find next solar event")
        .clone())
}

#[instrument]
async fn set_gtk_color_scheme(color_scheme: &ColorScheme) -> Result<()> {
    match Command::new("dconf")
        .args([
            "write",
            "/org/gnome/desktop/interface/color-scheme",
            &format!(
                "'{}'",
                match color_scheme {
                    ColorScheme::Dark => "prefer-dark",
                    ColorScheme::Light => "prefer-light",
                }
            ),
        ])
        .stdin(Stdio::null())
        .status()
        .await
    {
        Ok(status) => {
            if !status.success() {
                error!(
                    exit_code = status.code(),
                    "dconf should exit with exit code 0"
                );
            }
        }
        Err(err) => error!(
            err = ?err,
            "dconf should start",
        ),
    }
    Ok(())
}
