use anyhow::Result;
use futures_util::stream::StreamExt;
use tokio::sync::broadcast::{Receiver, Sender};
use tracing::{error, info, instrument};
use zbus::Connection;
use zbus_systemd::timedate1::TimedatedProxy;

use crate::UserEvent;

#[instrument(skip_all)]
pub(crate) async fn timezone_task(
    tx: Sender<UserEvent>,
    mut rx: Receiver<UserEvent>,
) -> Result<()> {
    let bus = Connection::system().await?;
    let proxy = TimedatedProxy::new(&bus).await?;

    let mut timezone_changed_signal = proxy.receive_timezone_changed().await;
    loop {
        tokio::select! {
            Some(property) = timezone_changed_signal.next()=> {
                match property.get().await {
                    Ok(timezone) => {
                        if let Err(err) = tx.send(UserEvent::Timezone(timezone)) {
                            error!(err = ?err, "should send to channel");
                        }
                    }
                    Err(err) => error!(err = ?err, "should read from D-Bus signal"),
                }
            }
            Ok(event) = rx.recv() => {
                match event {
                    UserEvent::WindowResumed => {
                        if let Err(err) = tx.send(UserEvent::Timezone(proxy.timezone().await?)) {
                            error!(err = ?err, "should send to channel");
                        }
                    }
                    UserEvent::WindowExiting => {
                        info!("shutting down task ...");
                        break;
                    }
                    _ => { /* no-op */ }
                }
            }
        }
    }
    Ok(())
}
