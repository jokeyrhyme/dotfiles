use anyhow::Result;
use ashpd::{
    desktop::{
        location::{Accuracy, LocationProxy},
        settings::Settings,
    },
    WindowIdentifier,
};
use futures_util::stream::StreamExt;
use tokio::sync::broadcast::{Receiver, Sender};
use tracing::{error, info, instrument};
use winit::event_loop::EventLoopProxy;

use crate::{ColorScheme, Location, UserEvent};

#[instrument(skip_all)]
pub(crate) async fn color_scheme_task(
    tx: EventLoopProxy<UserEvent>,
    mut rx: Receiver<UserEvent>,
) -> Result<()> {
    let proxy = Settings::new().await?;

    let mut color_scheme_changed_signal = proxy.receive_color_scheme_changed().await?;
    loop {
        tokio::select! {
            Some(color_scheme) = color_scheme_changed_signal.next() => {
                info!(color_scheme = ?color_scheme, "color scheme updated");
                if let Err(err) = tx.send_event(UserEvent::ColorScheme(ColorScheme::from(color_scheme))) {
                    error!(err = ?err, "should send to event loop");
                }
            }
            Ok(event) = rx.recv() => {
                match event {
                    UserEvent::WindowResumed => {
                        match proxy.color_scheme().await {
                            Ok(color_scheme) => {
                                if let Err(err) = tx.send_event(UserEvent::ColorScheme(ColorScheme::from(color_scheme))) {
                                    error!(err = ?err, "should send to event loop");
                                }
                            },
                            Err(err) => error!(err = ?err, "should send to event loop"),
                        }
                    }
                    UserEvent::WindowExiting => {
                        info!("shutting down task ...");
                        break;
                    }
                    _ => { /* no-op */ }
                }
            }
        }
    }
    Ok(())
}

#[instrument(skip_all)]
pub(crate) async fn location_task(
    tx: Sender<UserEvent>,
    mut rx: Receiver<UserEvent>,
) -> Result<()> {
    let proxy = LocationProxy::new().await?;
    let session = proxy
        .create_session(Some(100 * 1000), Some(60), Some(Accuracy::City))
        .await?;
    proxy.start(&session, &WindowIdentifier::default()).await?;

    let mut location_updated_signal = proxy.receive_location_updated().await?;
    loop {
        tokio::select! {
            Some(location) = location_updated_signal.next() => {
                info!(location =?location, "location updated");
                if let Err(err) = tx.send(UserEvent::Location(Location::from(location))) {
                    error!(err = ?err, "should send to channel");
                }
            }
            Ok(event) = rx.recv() => {
                match event {
                    UserEvent::WindowResumed => {
                        if let Err(err) = tx.send(UserEvent::Location(Location::default())) {
                            error!(err = ?err, "should send to channel");
                        }
                    }
                    UserEvent::WindowExiting => {
                        info!("shutting down task ...");
                        break;
                    }
                    _ => { /* no-op */ }
                }
            }
        }
    }
    Ok(())
}
