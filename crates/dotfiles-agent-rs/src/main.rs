#![deny(clippy::all, clippy::pedantic, unsafe_code)]

//! design:
//! - on Linux:
//!   - [x] use xdg-desktop-portal Location
//!     - [x] fallback to hard-coded coordinates for Sydney
//!     - [x] react to changes in location
//!   - [x] use systemd-timezoned D-Bus API to query timezone
//!     - [x] react to changes in timezone
//!   - [x] use [Sunrise Sunset API](https://sunrise-sunset.org/api) to query sunrise/sunset times
//!     - [ ] fallback to hard-coded 07:00 sunrise and 17:00 sunset
//!     - [ ] wait until future sunrise/sunset and trigger color scheme change
//!   - [x] call `dconf write /org/gnome/desktop/interface/color-scheme "'prefer-light'"` after sunrise
//!   - [x] call `dconf write /org/gnome/desktop/interface/color-scheme "'prefer-dark'"` after sunset
//!   - [x] query xdg-desktop-portal Settings for current theme
//!   - [x] react to xdg-desktop-portal Settings events for theme changes
//! - on macOS:
//!   - assume system is configured to switch to dark mode at sunset, light mode at sunrise
//!   - assume system internally uses location and timezone data to achieve this
//!   - [x] query current theme
//!   - [x] react to system-provided window events for theme changes

use std::{process::Stdio, sync::LazyLock};

use anyhow::Result;
use tokio::{
    process::Command,
    sync::Mutex,
    task::{self},
};
use tracing::{error, info};
#[cfg(target_os = "macos")]
use winit::platform::macos::{ActivationPolicy, EventLoopBuilderExtMacOS};
use winit::{
    application::ApplicationHandler,
    event::WindowEvent,
    event_loop::{ActiveEventLoop, EventLoop},
    window::{Window, WindowButtons, WindowId, WindowLevel},
};

#[cfg(target_os = "linux")]
mod portal;
#[cfg(target_os = "linux")]
mod sunrise_sunset;
#[cfg(target_os = "linux")]
mod systemd;

static SERIALIZE_COMMANDS: LazyLock<Mutex<()>> = LazyLock::new(|| Mutex::new(()));

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt().init();

    let mut builder = EventLoop::<UserEvent>::with_user_event();
    // as a background agent, we don't want to appear in the macOS Dock
    #[cfg(target_os = "macos")]
    builder.with_activation_policy(ActivationPolicy::Accessory);
    let event_loop = builder.build().expect("should connect to windowing system");

    let (tx, _rx) = tokio::sync::broadcast::channel::<UserEvent>(8);

    #[cfg(target_os = "linux")]
    let tasks = {
        let mut tasks: tokio::task::JoinSet<Result<()>> = tokio::task::JoinSet::new();

        tasks.spawn(portal::color_scheme_task(
            event_loop.create_proxy(),
            tx.subscribe(),
        ));
        tasks.spawn(portal::location_task(tx.clone(), tx.subscribe()));
        tasks.spawn(sunrise_sunset::sunrise_sunset_task(
            event_loop.create_proxy(),
            tx.subscribe(),
        ));
        tasks.spawn(systemd::timezone_task(tx.clone(), tx.subscribe()));

        tasks
    };

    // we seem to be starting too soon on macOS,
    // and I haven't yet figured out if there's a reliable event to wait for,
    // so crudely wait a fixed period of time :S
    // this may also help avoid unnecessary churn on Linux
    tokio::time::sleep(std::time::Duration::from_secs(17)).await;

    let mut app = Application { tx, window: None };
    event_loop
        .run_app(&mut app)
        .expect("should connect to windowing system");

    #[cfg(target_os = "linux")]
    let _ = tasks.join_all().await;

    Ok(())
}

async fn handle_color_scheme_changed(color_scheme: ColorScheme) {
    let _ = SERIALIZE_COMMANDS.lock().await;

    info!(color_scheme = ?color_scheme, "handle_color_scheme_changed");
    let home_dir = dirs::home_dir().expect("should find $HOME");
    let handler_script = home_dir
        .join(".dotfiles")
        .join("bin")
        .join("handle-appearance-change.nu");
    match Command::new(&handler_script)
        .arg(String::from(&color_scheme))
        .stdin(Stdio::null())
        .status()
        .await
    {
        Ok(status) => {
            if !status.success() {
                error!(
                    exit_code = status.code(),
                    handler_script = %handler_script.display(),
                    "script should exit with exit code 0"
                );
            }
        }
        Err(err) => error!(
            err = ?err,
            handler_script = %handler_script.display(),
            "script should start",
        ),
    }
}

#[derive(Debug)]
struct Application {
    tx: tokio::sync::broadcast::Sender<UserEvent>,
    window: Option<Window>,
}
impl ApplicationHandler<UserEvent> for Application {
    fn exiting(&mut self, _event_loop: &ActiveEventLoop) {
        let _ = self.tx.send(UserEvent::WindowExiting);
    }

    fn resumed(&mut self, event_loop: &ActiveEventLoop) {
        info!("resumed");
        if let Some(theme) = event_loop.system_theme() {
            task::spawn(handle_color_scheme_changed(ColorScheme::from(theme)));
        }

        if self.window.is_none() {
            // creating one hidden window so we receive WindowEvents
            let window_attributes = Window::default_attributes()
                .with_decorations(false)
                .with_enabled_buttons(WindowButtons::empty())
                .with_resizable(false)
                .with_visible(false)
                .with_window_level(WindowLevel::AlwaysOnBottom);
            self.window = Some(
                event_loop
                    .create_window(window_attributes)
                    .expect("should create window"),
            );
        }

        if let Err(err) = self.tx.send(UserEvent::WindowResumed) {
            error!(err = ?err, "should broadcast on channel");
        }
    }

    #[cfg(target_os = "linux")]
    fn user_event(&mut self, _event_loop: &ActiveEventLoop, event: UserEvent) {
        if let UserEvent::ColorScheme(theme) = event {
            task::spawn(handle_color_scheme_changed(theme));
        };
    }

    fn window_event(
        &mut self,
        _event_loop: &ActiveEventLoop,
        _window_id: WindowId,
        event: WindowEvent,
    ) {
        if let WindowEvent::ThemeChanged(theme) = event {
            info!(theme = ?theme, "theme changed");
            task::spawn(handle_color_scheme_changed(ColorScheme::from(theme)));
        };
    }
}

#[derive(Clone, Copy, Debug, Default, PartialEq)]
enum ColorScheme {
    /// default to Dark, because Dark at noon is less painful than Light at night
    #[default]
    Dark,
    Light,
}
impl std::fmt::Display for ColorScheme {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", String::from(self))
    }
}
impl From<ashpd::desktop::settings::ColorScheme> for ColorScheme {
    fn from(value: ashpd::desktop::settings::ColorScheme) -> Self {
        match value {
            ashpd::desktop::settings::ColorScheme::PreferLight => Self::Light,
            _ => Self::Dark,
        }
    }
}
impl From<winit::window::Theme> for ColorScheme {
    fn from(value: winit::window::Theme) -> Self {
        match value {
            winit::window::Theme::Light => Self::Light,
            winit::window::Theme::Dark => Self::Dark,
        }
    }
}
impl From<&ColorScheme> for String {
    fn from(value: &ColorScheme) -> Self {
        Self::from(match value {
            ColorScheme::Dark => "dark",
            ColorScheme::Light => "light",
        })
    }
}

#[derive(Clone, Debug, PartialEq)]
struct Location {
    latitude: f64,
    longitude: f64,
}
impl Default for Location {
    fn default() -> Self {
        //  hardcode Australia/Sydney, https://xkcd.com/2170/
        Self {
            latitude: -33.8,
            longitude: 151.2,
        }
    }
}
impl From<ashpd::desktop::location::Location> for Location {
    fn from(value: ashpd::desktop::location::Location) -> Self {
        Self {
            latitude: value.latitude(),
            longitude: value.longitude(),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
enum UserEvent {
    #[cfg(target_os = "linux")]
    ColorScheme(ColorScheme),
    #[cfg(target_os = "linux")]
    Location(Location),
    #[cfg(target_os = "linux")]
    Timezone(String),
    WindowExiting,
    WindowResumed,
}
