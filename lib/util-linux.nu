export def lsblk [] {
    ^lsblk --all --json --list --output-all | from json | get 'blockdevices'
}
