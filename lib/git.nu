export def git_branch [LOCAL_DIR: string] {
    ^git -C $LOCAL_DIR rev-parse --abbrev-ref HEAD
}

export def git_update [REMOTE_REPO: string, LOCAL_DIR: string] {
    try {
        ^git -C $LOCAL_DIR status
        # ensure we have the latest
        ^git -C $LOCAL_DIR restore .
        ^git -C $LOCAL_DIR pull
        ^git -C $LOCAL_DIR submodule update --init --force --recursive
    } catch { |err|
        # ensure we have a git repository
        mkdir ($LOCAL_DIR | path dirname)
        rm --force --recursive $LOCAL_DIR
        ^git -C ($LOCAL_DIR | path dirname) clone --recurse-submodules $REMOTE_REPO
    }
}

export def has_git_updates [REMOTE_REPO: string, LOCAL_DIR: string] {
    print $'($REMOTE_REPO) -> ($LOCAL_DIR)'
    mut OLD_COMMIT = 'absent'
    try {
        ^git -C $LOCAL_DIR status
        $OLD_COMMIT = (^git -C $LOCAL_DIR rev-parse HEAD)
        # ensure we have the latest
        ^git -C $LOCAL_DIR restore .
        ^git -C $LOCAL_DIR pull
        ^git -C $LOCAL_DIR submodule update --init --force --recursive
    } catch { |err|
        # ensure we have a git repository
        mkdir ($LOCAL_DIR | path dirname)
        rm --force --recursive $LOCAL_DIR
        ^git -C ($LOCAL_DIR | path dirname) clone --recurse-submodules $REMOTE_REPO
    }
    let NEW_COMMIT = (^git -C $LOCAL_DIR rev-parse HEAD)

    if ($OLD_COMMIT == $NEW_COMMIT) {
        print $'($LOCAL_DIR): already on latest commit: ($NEW_COMMIT)'
        false
    } else {
        print $'($LOCAL_DIR): ($OLD_COMMIT) -> ($NEW_COMMIT)'
        true
    }
}
