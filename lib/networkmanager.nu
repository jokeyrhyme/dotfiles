export def nm_connection [] {
    ^nmcli --color no --fields NAME,TYPE,DEVICE,ACTIVE,STATE connection | detect columns
}

export def nm_device_list [] {
    ^nmcli --color no --fields DEVICE,STATE,TYPE device | detect columns
}

export def nm_device_show [NAME: string] {
    ^nmcli --color no device show $NAME |
        lines |
        each { |LINE|
            let COLON = ($LINE | str index-of ':')
            let KEY = ($LINE | str substring 0..$COLON | str trim)
            let VALUE = ($LINE | str substring ($COLON + 1).. | str trim)
            { KEY: $KEY, VALUE: $VALUE }
        }
    # TODO: return a single record, not a table of key/value records
}

export def nm_device_wifi_list [] {
    let STDOUT = (nmcli --colors no --fields ACTIVE,DEVICE,SSID,SIGNAL device wifi list --rescan no | complete).stdout
    if ($STDOUT | is-empty) {
       []     
    } else {
        $STDOUT | detect columns
    }
}

