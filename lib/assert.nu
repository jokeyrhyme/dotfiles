use std assert

export def assert_type [EXPECTED: string] {
    let ACTUAL = ($in | describe)
    assert equal $ACTUAL $EXPECTED
}
