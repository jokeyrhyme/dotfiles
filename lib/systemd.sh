#! /usr/bin/env sh

__dotfiles_is_systemd_unit() { # $1 = e.g. name.service
  systemctl list-unit-files "${1}" >/dev/null 2>&1
}

__dotfiles_is_systemd_user_unit() { # $1 = e.g. name.service
  # use `status` command to check if unit exists
  systemctl --user status "${1}" >/dev/null 2>&1
}

__dotfiles_systemctl_disable() { # $@ = space-separated list of units to enable
  for UNIT in "$@"; do
    if __dotfiles_is_systemd_unit "${UNIT}"; then
      sudo systemctl disable "${UNIT}"
    fi
  done
}

__dotfiles_systemctl_disable_user() { # $@ = space-separated list of units to enable
  for UNIT in "$@"; do
    if __dotfiles_is_systemd_user_unit "${UNIT}"; then
      systemctl --user disable "${UNIT}"
    fi
  done
}

__dotfiles_systemctl_enable() { # $@ = space-separated list of units to enable
  for UNIT in "$@"; do
    if __dotfiles_is_systemd_unit "${UNIT}"; then
      sudo systemctl enable "${UNIT}"
    fi
  done
}

__dotfiles_systemctl_enable_user() { # $@ = space-separated list of units to enable
  for UNIT in "$@"; do
    if __dotfiles_is_systemd_user_unit "${UNIT}"; then
      systemctl --user enable "${UNIT}"
    fi
  done
}
