export def parse_variables [INPUT: string] {
    let PARSED = ( $INPUT | str trim | parse --regex '(?P<name>\w+)=(?P<value>[^\n;]+)(\n|;)?' )
    if ($PARSED | is-empty) {
        {}
    } else {
        $PARSED | transpose --as-record --header-row --ignore-titles | first
    }
}
