#! /usr/bin/env sh

__dotfiles_has_precursor() {
  if command -v lsusb >/dev/null 2>&1; then
    lsusb -d 1209: >/dev/null 2>&1
  else
    false
  fi
}

__dotfiles_has_yubikey() {
  if command -v lsusb >/dev/null 2>&1; then
    lsusb -d 1050: >/dev/null 2>&1
  elif command -v ioreg >/dev/null 2>&1; then
    ioreg -p IOUSB -l -w 0 | grep '"YubiKey OTP+FIDO+CCID"' >/dev/null 2>&1
  else
    false
  fi
}
