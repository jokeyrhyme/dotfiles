export def delete_if_not_modified_since  [FILE: string, DATE] {
    if (ls ($FILE | path dirname) --long | where name == $FILE | where modified < $DATE | is-not-empty) {
        do --ignore-errors { rm $FILE }
    }
}
