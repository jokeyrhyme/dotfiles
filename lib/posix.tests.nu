use std assert
use ./assert.nu assert_type
use ./posix.nu parse_variables

const FILENAME = path self
print $FILENAME

# TODO adopt nupm package structure for conventional testing:
# https://www.nushell.sh/book/testing.html#nupm-package

# export def parse_variables_with_empty_input [] {
    let INPUT = ""
    let GOT = (parse_variables $INPUT)
    $GOT | assert_type 'record'
    print 'OK'
# }

# export def parse_variables_with_stdout_from_gnome-keyring-daemon [] {
    let INPUT = "GNOME_KEYRING_CONTROL=/run/user/60222/keyring\nSSH_AUTH_SOCK=/run/user/60222/keyring/ssh\n"
    let GOT = (parse_variables $INPUT)
    assert equal $GOT.GNOME_KEYRING_CONTROL '/run/user/60222/keyring'
    assert equal $GOT.SSH_AUTH_SOCK '/run/user/60222/keyring/ssh'
    print 'OK'
# }

# export def parse_variables_with_stdout_from_ssh-agent [] {
    # ssh-agent
    let INPUT = "SSH_AUTH_SOCK=/tmp/ssh-XXXXXXmp5osY/agent.54886; export SSH_AUTH_SOCK;\nSSH_AGENT_PID=54888; export SSH_AGENT_PID;\necho Agent pid 54888;\n"
    let GOT = (parse_variables $INPUT)
    assert equal $GOT.SSH_AGENT_PID '54888'
    assert equal $GOT.SSH_AUTH_SOCK '/tmp/ssh-XXXXXXmp5osY/agent.54886'
    print 'OK'
# }
