export def has_precursor [] {
    if (which lsusb | is-not-empty) {
        (^lsusb -d 1209: | complete).exit_code == 0
    } else {
        false
    }
}

export def has_yubikey [] {
    if (which lsusb | is-not-empty) {
        (^lsusb -d 1050: | complete).exit_code == 0
    } else if (which ioreg | is-not-empty) {
        (^ioreg -p IOUSB -l -w 0 | str contains '"YubiKey OTP+FIDO+CCID"')
    } else {
        false
    }
}
