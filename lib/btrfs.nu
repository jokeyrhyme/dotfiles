use './util-linux.nu' [ lsblk ]

export def btrfs_balance_status [path: string] {
    let default = {
        chunks_done: 0,
        chunks_total: 0,
        path: $path,
        percent: 100,
        status: 'finished',
    }
    let parsed = (
        ^sudo btrfs balance status $path |
        collect |
        parse --regex 'Balance on '\S+' is (?P<status>\w+)\n(?P<chunks_done>\d+) out of about (?P<chunks_total>\d+) chunks balanced'
    )
    if ($parsed | is-empty) {
        $default
    } else {
        $default |
            merge ($parsed | first) |
            into int 'chunks_done' |
            into int 'chunks_total' |
            update percent { |row| (100 * $row.chunks_done / $row.chunks_total) | math round --precision 2 }
    }
}

def maybe_string_to_int [VALUE: string] {
    if ($VALUE != '-') { $VALUE | into int } else { $VALUE }
}

export def btrfs_filesystem_usage [MOUNT: string] {
    ^sudo btrfs filesystem usage --raw -T $MOUNT | 
        parse --regex '(?<device>/dev/[\w/]+)\s+(?<data>[\d\-]+)\s+(?<metadata>[\d\-]+)\s+(?<system>[\d\-]+)\s+(?<unallocated>[\d\-]+)\s+(?<total>[\d\-]+)\s+(?<slack>[\d\-]+)' |
        update data { |it| maybe_string_to_int $it.data } |
        update metadata { |it| maybe_string_to_int $it.metadata } |
        update system { |it| maybe_string_to_int $it.system } |
        update unallocated { |it| maybe_string_to_int $it.unallocated } |
        update total { |it| maybe_string_to_int $it.total } |
        update slack { |it| maybe_string_to_int $it.slack }
}

def is_device_removing [DEVICE: record] {
    (
        ($DEVICE.data != '-') and
        ($DEVICE.unallocated != '-') and
        ($DEVICE.data + $DEVICE.unallocated == 0)
    )
}

export def btrfs_remove_percent [MOUNT: string] {
    let DEVICES = (btrfs_filesystem_usage $MOUNT)
    let KEEPERS = ($DEVICES | where { |it| not (is_device_removing $it) })
    let REMOVALS = ($DEVICES | where { |it| is_device_removing $it })
    if ($REMOVALS | is-empty) {
        100
    } else {
        let DATA = ($KEEPERS | first).data
        100 * ($DATA - ($REMOVALS | first).data) / $DATA
    }
}

export def btrfs_scrub_percent [MOUNT: string] {
    let DETAILS = (^pkexec btrfs scrub status $MOUNT | lines | split column ': ')
    let SCRUBBED_ROW = ($DETAILS | where 'column1' =~ 'Bytes scrubbed')
    let TOTAL_ROW = ($DETAILS | where 'column1' =~ 'Total to scrub')
    if (($SCRUBBED_ROW | is-empty) or ($TOTAL_ROW | is-empty)) {
        100
    } else {
        let SCRUBBED = ($SCRUBBED_ROW | get 'column2' | str trim | split column ' ' | first | get 'column1')
        let TOTAL = ($TOTAL_ROW | first | get 'column2' | str trim)
        100 * ($SCRUBBED | into filesize) / ($TOTAL | into filesize)
    }
}

export def btrfs_scrub_status [device: string] {
    let default = {
        bytes_scrubbed: '0.0GiB  (0.0%)',
        device: $device,
        duration: '0:00:00',
        eta: '1970-01-01',
        scrub_resumed: '1970-01-01',
        scrub_started: '1970-01-01',
        status: 'finished',
        time_left: '0:00:00',
        total_to_scrub: '1B', # TODO: use actual device size here
    }
    let stdout = (^pkexec btrfs scrub status $device | complete).stdout
    let parsed = if ($stdout | str contains 'no stats available') {
        {}
    } else {
        (
            $stdout | 
                from ssv --minimum-spaces 3 --noheaders |
                update column0 { |row| $row.column0 | str trim --char ':' | str snake-case } |
                transpose --as-record --header-row 
        )
    }
    $default |
        merge $parsed |
        into filesize 'total_to_scrub' |
        update bytes_scrubbed { |row| $row.bytes_scrubbed | split row ' ' | first | into filesize } |
        update duration { |row| $row.duration | str replace --regex '(\d+):(\d+):(\d+)' '${1}hr ${2}min ${3}sec' | into duration } |
        update time_left { |row| $row.time_left | str replace --regex '(\d+):(\d+):(\d+)' '${1}hr ${2}min ${3}sec' | into duration } |
        insert percent { |row| $row.bytes_scrubbed / $row.total_to_scrub * 100 | math round --precision 2 } |
        reject --ignore-errors "eta" "scrub_resumed" "scrub_started" "rate" |
        rename --column { bytes_scrubbed: "bytes_done" } |
        rename --column { total_to_scrub: "bytes_total" }
}

# find all unique btrfs devices,
# but deduplicate where a device has multiple mount points
export def list_btrfs_mounts [] {
    ^mount --types btrfs | 
        lines |
        each { |it| $it | parse --regex '^(?P<device>\S+) on (?P<path>\S+) type btrfs' } |
        flatten |
        uniq-by 'device'
}

export def list_btrfs_devices [] {
    lsblk | where fstype == 'btrfs' | get 'path'
}
