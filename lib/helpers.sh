#! /usr/bin/env sh

__dotfiles_is_distro_archlinux() {
  [ -r /etc/os-release ] && grep "ID=arch" </etc/os-release >/dev/null 2>&1
}

__dotfiles_is_distro_debian() {
  # old 32-bit raspbian: [ -r /etc/os-release ] && grep "ID=raspbian" </etc/os-release >/dev/null 2>&1
  [ -r /etc/os-release ] && grep "ID=debian" </etc/os-release >/dev/null 2>&1 && command -v apt-get >/dev/null 2>&1
}

__dotfiles_is_distro_raspbian() {
  # old 32-bit raspbian: [ -r /etc/os-release ] && grep "ID=raspbian" </etc/os-release >/dev/null 2>&1
  __dotfiles_is_distro_debian && command -v raspi-config >/dev/null 2>&1
}

__dotfiles_mv_if_diff() { # $1=source $2=destination
  if [ ! -e "${2}" ]; then
    # destination does not yet exist
    sudo mv "${1}" "${2}"
  elif ! diff --brief "${2}" "${1}" >/dev/null 2>&1; then
    # attempt to display file differences
    if command -v difft >/dev/null; then
      # `|| true`: ignore any error exit codes from difftastic
      difft "${2}" "${1}" || true
    else
      # `|| true`: ignore any error exit codes from diff
      diff "${2}" "${1}" || true
    fi

    sudo mv -v "${1}" "${2}"
  else
    # source and destination are equal
    sudo rm -f "${1}"
  fi
}
