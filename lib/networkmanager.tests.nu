use std 
use ./assert.nu assert_type
use ./networkmanager.nu [ nm_device_list, nm_device_show ]

const FILENAME = path self
print $FILENAME

# export def nm_device_list_includes_loopback [] {
    if (which nmcli | is-empty) {
      print 'SKIP'
      return
    }
    let GOT = (nm_device_list)
    $GOT | assert_type 'table<DEVICE: string, STATE: string, TYPE: string>'
    assert equal ($GOT | where DEVICE == 'lo' | length) 1
    print 'OK'
# }

# export def nm_device_show_includes_loopback [] {
    if (which nmcli | is-empty) {
      print 'SKIP'
      return
    }
    let GOT = (nm_device_show 'lo')
    $GOT | assert_type 'table<KEY: string, VALUE: string>'
    assert not equal ($GOT | where KEY == 'GENERAL.DEVICE' | first | select 'VALUE') 'lo'
    print 'OK'
# }

