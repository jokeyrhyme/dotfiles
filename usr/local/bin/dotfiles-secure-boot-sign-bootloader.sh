#! /usr/bin/env sh

# https://bentley.link/secureboot/
# https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface/Secure_Boot#Signing_the_kernel_with_a_pacman_hook

set -eux

/usr/bin/sbctl sign --save /efi/EFI/BOOT/BOOTX64.EFI
/usr/bin/sbctl sign --save /efi/EFI/systemd/systemd-bootx64.efi
