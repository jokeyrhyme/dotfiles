#! /usr/bin/env nu

if not ('/etc/locale.gen.pacnew' | path exists) {
    mv /etc/locale.gen /etc/locale.gen.pacnew
}

# write new config with English and Spanish enabled
open --raw /etc/locale.gen.pacnew | str replace --all '#en_' 'en_' | str replace --all '#es_' 'es_' | save --force /etc/locale.gen

rm /etc/locale.gen.pacnew

locale-gen
