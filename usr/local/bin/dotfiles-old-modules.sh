#! /usr/bin/env sh

set -eu

for MODULES_DIR in /lib/modules/*; do
  if [ ! -f "${MODULES_DIR}/vmlinuz" ]; then
    /usr/bin/rm -rf "${MODULES_DIR}"
  fi
done
