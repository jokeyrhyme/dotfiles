#! /usr/bin/env sh

# https://bentley.link/secureboot/
# https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface/Secure_Boot#Signing_the_kernel_with_a_pacman_hook

set -eux

EFI_EXE=/usr/lib/fwupd/efi/fwupdx64.efi
SHIM_EXE=/boot/EFI/arch/shimx64.efi

/usr/bin/mkdir -p "$(dirname ${SHIM_EXE})"
/usr/bin/sbctl sign --output "${EFI_EXE}.signed" --save "${EFI_EXE}"
/usr/bin/cp -v "${EFI_EXE}.signed" "${SHIM_EXE}"
