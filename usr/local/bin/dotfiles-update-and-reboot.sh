#! /usr/bin/env sh

# assumptions: executed as a user with password-less `sudo` access

set -eu

~/.dotfiles/scripts/update.sh

sudo systemctl reboot
