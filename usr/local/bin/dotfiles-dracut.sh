#! /usr/bin/env sh

set -eu

UKI_DIR=/efi/EFI/Linux

for EXE in "${UKI_DIR}"/archlinux-dracut-*; do
  VERSION=$(echo $EXE | sed 's/^.\+\/archlinux\-dracut\-//' | sed 's/\.efi$//')
  if [ ! -f /lib/modules/${VERSION}/vmlinuz ]; then
    # || true: ignore error where file is not in the DB
    /usr/bin/sbctl remove-file "${EXE}" || true
    /usr/bin/rm -fv "${EXE}"
  fi
done

while read -r LINE; do
  if echo $LINE | grep 'lib/modules/.\+/vmlinuz' >/dev/null 2>&1; then
    MODULES_DIR=$(dirname "${LINE}");
    VERSION=$(basename "${MODULES_DIR}")
    EXE="${UKI_DIR}/archlinux-dracut-${VERSION}.efi"
    /usr/bin/dracut --force --uefi "${EXE}" "${VERSION}"
    /usr/bin/sbctl sign --save "${EXE}"
  fi
done

