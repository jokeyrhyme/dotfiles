#! /usr/bin/env sh

# configure archlinux `makepkg`,
# mostly used during installation of AUR packages

set -eu

if [ -f /etc/makepkg.conf.pacnew ]; then
  /usr/bin/mv --verbose /etc/makepkg.conf.pacnew /etc/makepkg.conf
fi

if [ -f /etc/makepkg.conf ]; then
  # why change BUILDDIR?
  # otherwise the default is to use /tmp which is tmpfs which is RAM, fills up real fast
  # see also: etc/makepkg.conf.d/dotfiles.conf
  BUILDDIR=/usr/src/makepkg
  /usr/bin/mkdir -p "${BUILDDIR}"
  /usr/bin/chown root:wheel "${BUILDDIR}"
  /usr/bin/chmod g+rwx "${BUILDDIR}"
fi
