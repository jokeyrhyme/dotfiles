#! /usr/bin/env sh

set -eu

# shellcheck source=../lib/helpers.sh
. "$(dirname $0)/../lib/helpers.sh"

# setup
# change /usr to read-write
if command -v systemd-sysext >/dev/null; then
  sudo systemd-sysext unmerge
fi

for RESET_SH in "$(dirname $0)"/../packages/*-reset.*; do
  printf "\n%s ...\n" "$(basename ${RESET_SH})"
  # shellcheck disable=SC1090
  "${RESET_SH}" || true
done
for UPDATE_SH in "$(dirname $0)"/../packages/*-update.*; do
  printf "\n%s ...\n" "$(basename ${UPDATE_SH})"
  # shellcheck disable=SC1090
  "${UPDATE_SH}" || true
done

# teardown
"${HOME}/.dotfiles/bin/secrets-teardown.sh"
# maybe change /usr to read-only
if command -v systemd-sysext >/dev/null; then
  sudo systemd-sysext merge
fi
