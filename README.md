# dotfiles ![Status](https://img.shields.io/badge/status-actively--developed-brightgreen) ![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/jokeyrhyme/dotfiles?branch=main)

## Status

This project is best paired with [`tuning`](https://gitlab.com/jokeyrhyme/tuning).

In the long-term plan, this project will contain preferences, and we'll remove most scripts in favour of `tuning`.

## Installation / Updating

1.  clone/pull this repository into a hidden directory

    ```sh
    $ git clone https://github.com/jokeyrhyme/dotfiles.git ~/.dotfiles

    OR

    $ cd ~/.dotfiles && git pull
    ```

2.  run the following

    ```sh
    $ ~/.dotfiles/scripts/update.sh
    ```

## Note: `sudo`

I avoid `sudo` usage where possible. I need it for:

- system package installation (e.g. `pacman`, `apt`, `yum`)
- changes made in /usr and /etc

## Acknowledgements / Attribution

- much gratitude goes to [sunrise-sunset.org](https://sunrise-sunset.org/) for their handy API
