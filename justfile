fmt:
  #! /usr/bin/env sh
  set -eu
  if command -v cargo >/dev/null; then
    for JUSTFILE in ./crates/*/justfile ; do
      just --justfile "${JUSTFILE}" fmt
    done
  fi
  if command -v prettier >/dev/null; then prettier --list-different --write .; fi
  if command -v shfmt >/dev/null; then
    shfmt -w -s -i 2 ./**/*.bash ./**/*.sh ./**/*.zsh
    shfmt -w -s -i 2 ./config/user-acpid/*
    shfmt -w -s -i 2 ./etc/systemd/user-environment-generators/*
  fi
  if command -v stylua >/dev/null; then stylua .; fi
  if command -v taplo >/dev/null; then taplo format; fi

test:
  #! /usr/bin/env sh
  set -eu
  if command -v alex >/dev/null; then alex ./*.md; fi
  if command -v bash >/dev/null; then bash -n ./**/*.bash ./**/*.sh; fi
  if command -v cargo >/dev/null; then
    for JUSTFILE in ./crates/*/justfile ; do
      just --justfile "${JUSTFILE}" test
    done
  fi
  if command -v luacheck >/dev/null; then luacheck .; fi
  # TODO: adopt nupm and run our nushell tests
  if command -v nu >/dev/null; then just test-nu; fi
  if command -v prettier >/dev/null; then prettier --check .; fi
  if command -v shellcheck >/dev/null; then
    shellcheck ./**/*.bash ./**/*.sh
    shellcheck ./config/*bash* ./config/profile ./config/user-acpid/*
    shellcheck ./etc/systemd/user-environment-generators/*
  fi
  if command -v stylua >/dev/null; then stylua --check .; fi
  if command -v systemd-analyze >/dev/null; then systemd-analyze verify --recursive-errors=no ./etc/systemd/system/*.{service,timer}; fi
  if command -v taplo >/dev/null; then taplo format --check; fi
  if command -v taplo >/dev/null; then taplo lint; fi
  if command -v write-good >/dev/null; then write-good ./*.md; fi
  if command -v zsh >/dev/null; then zsh -n ./**/*.sh ./**/*.zsh; fi
  for GENERATOR in ./etc/systemd/user-environment-generators/*.conf ; do
    eval "$($GENERATOR)" >/dev/null
    echo "${GENERATOR} OK"
  done

test-nu:
  #! /usr/bin/env nu
  glob ./**/*.tests.nu |
      each { |$FILE| nu $FILE }
